const FtpDeploy = require("ftp-deploy");
const archiver = require("archiver");
const version = require("./package.json").version;
const Credentials = require("./credentials");
const creds = new Credentials();
const ftpDeploy = new FtpDeploy();
//const date = new Date().toLocaleDateString();
const uploadProject = {
    user: creds.user,
    // Password optional, prompted if none given
    password: creds.password,
    host: creds.server,
    port: 21,
    localRoot: __dirname + "/dist/golowinskiy-client",
    remoteRoot: "",
    include: ["*", "**/*", `dist/golowinskiy-client-${version}.zip`],
    exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    deleteRemote: true,
    forcePasv: false,
    sftp: false
};

const uploadProjectDuzzle = {
    user: creds.dlogin,
    // Password optional, prompted if none given
    password: creds.dpassword,
    host: creds.dserver,
    port: 21,
    localRoot: __dirname + "/dist/golowinskiy-client",
    remoteRoot: "./public_html",
    include: ["*", "**/*"],
    exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    deleteRemote: true,
    forcePasv: false,
    sftp: false
};

const uploadBuildVersion = {
    user: creds.dlogin,
    password: creds.dpassword,
    host: creds.dserver,
    port: 21,
    localRoot: __dirname + "/dist/",
    remoteRoot: "./backups",
    include: [`golowinskiy-client-${version}.zip`],
    exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    deleteRemote: false,
    forcePasv: true,
    sftp: false
};

function zipDirectory(sourceDir, outPath) {
    const fs = require("fs");
    const archive = archiver("zip", { zlib: { level: 9 } });
    const stream = fs.createWriteStream(outPath);

    return new Promise((resolve, reject) => {
        archive
            .directory(sourceDir, false)
            .on("error", (err) => reject(err))
            .pipe(stream);

        stream.on("close", () => resolve());
        archive.finalize();
    });
}

const loading = (function () {
    var h = ["|", "/", "-", "\\"];
    var i = 0;

    return setInterval(() => {
        i = i > 3 ? 0 : i;
        console.clear();
        console.log("deploying golowinskiy: ", h[i]);
        i++;
    }, 300);
})();

zipDirectory("./dist/golowinskiy-client", `./dist/golowinskiy-client-${version}.zip`)
    .then(loading)
    .then(() => ftpDeploy.deploy(uploadProject))
    .then(() => ftpDeploy.deploy(uploadProjectDuzzle))
    .then((r) => {
        console.log(`golowinskiy-client v${version} deployed successfully!`);
        return r;
    })
    .then(() => ftpDeploy.deploy(uploadBuildVersion))
    .then((r) => {
        clearInterval(loading);
        console.clear();
        console.log(`golowinskiy client v${version} has just deployed successfully!`);
        return r;
    })
    .then(() => ftpDeploy.deploy(uploadBuildVersion))
    .then((r) => {
        console.log(`${creds.web}/golowinskiy-client-${version}.zip`);
        return r;
    })
    .catch((err) => console.log(err));
