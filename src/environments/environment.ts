export const environment = {
  production: false,
  api:  'http://golowinskiy-api.bostil.ru/api/',  //'http://golowinskiy-api.bostil.ru/api/',
  images: 'http://golowinskiy-api.bostil.ru',
  idPortal: '19139',
  domain:   'localhost', //   'golowinskiy.bostil.ru'
  version: require('../../package.json').version + '-dev',
};

