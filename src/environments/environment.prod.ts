export const environment = {
  production: true,
  api: 'http://gol.hramushina.ru/api/',
  images: 'http://gol.hramushina.ru',
  idPortal: '19139',
  domain: 'prod',
  version: require('../../package.json').version,
};