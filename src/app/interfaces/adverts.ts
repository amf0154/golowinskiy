export interface MoveAdverts {
    prc_IDs: Array<number>;
    id: number;
    appCode: number;
    cid: number | string;
}
export interface MoveAdvertsResp {
    result: boolean;
}

export interface GalleryItem {
    appCode: string;
    createdAt: string;
    creater_ID: string;
    cust_ID: string;
    gdate: string;                                                                                     
    id: string;
    idCategories: Array<string>;
    idcrumbs: string;
    image_Base: string;
    image_Site: string;
    img: any;
    isHid: boolean;
    isShowBasket: string;
    mediaLink: string;
    nameCategories: Array<string>
    parent_id: string;
    prc_Br: string;
    prc_ID: number;
    suplier: string;
    tName: string;
    txtcrumbs: string;
}

