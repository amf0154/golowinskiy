export interface CopiedLink {
    Prc_ID: number;
    Appcode: string;
    MediaFileName: string;
    MediaOrder: string
}
