export interface UserInfo {
    accessToken?: string;
    address: string;
    e_mail: string;
    fio: string;
    isEditing: boolean;
    isMoveAdvert: boolean;
    isRepeat: boolean;
    isSendRepeat: boolean;
    phone: string;
    skype: string;
    whatsapp: string;
    isSlideshowAdvert: boolean;
    isSlideshowExtraPict: boolean;
    isSearchPageVariant: any;
    isNoStartAudio: boolean;
    isStudy: boolean;
    isStudyAll: boolean;
    isRussianDoll: boolean;
    isHomePageShow?: boolean;
    showPause?: number;
    isShowMenuRepeat: boolean;
}
