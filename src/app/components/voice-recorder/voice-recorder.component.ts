import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Howl} from 'howler'
import { AudioRecordingService } from '../shared/services/audio-recording.service';


@Component({
  selector: 'app-voice-recorder',
  templateUrl: './voice-recorder.component.html',
  styleUrls: ['./voice-recorder.component.scss']
})
export class VoiceRecorderComponent implements OnInit {
  public blobUrl: Blob = null;
  public vocal_playing: boolean = false;
  public isRecording: boolean = false;
  public currentAudio: any = null;
  public showRecorder: boolean = true;
  @Output() getRecord = new EventEmitter<Blob>();

  constructor(private audioRecordingService: AudioRecordingService,) { }

  ngOnInit() {
    this.audioRecordingService.getRecordedBlob().subscribe((data) => {
      this.blobUrl = data.blob;
      this.getRecord.emit(data.blob)
      const way = URL.createObjectURL(data.blob);
      this.currentAudio = new Howl({
        src: [way],
        html5: true,
        ext: ['ogg']
      });
    });

  }

  timer: number = 0;
  startSec: any = null;
  startCountSec() {
    this.timer++;
  }

  stopSec = () => clearInterval(this.startSec);

  startRec() {
    this.timer = 0;
    this.startSec = setInterval(() => this.startCountSec(), 1000);
    this.audioRecordingService.startRecording();
    this.isRecording = true;
  }

  stopRec() {
    this.stopSec();
    this.isRecording = false;
    this.audioRecordingService.stopRecording();
    this.isRecording = false;
  }

  async playRec() {
    this.vocal_playing = true;
    this.reductButton(this.timer);
    this.currentAudio.play();
  }

  reductingProcess;
  reductButton = (time) => this.reductingProcess = setTimeout(() => this.vocal_playing = false, Number(time + '000'));

  async pauseRec() {
    this.currentAudio.pause();
    this.currentAudio.currentTime = 0;
    this.vocal_playing = false;
    clearTimeout(this.reductingProcess);
  }

  clearRecordedData() {
    this.blobUrl = null;
    this.getRecord.emit(null);
  }


}
