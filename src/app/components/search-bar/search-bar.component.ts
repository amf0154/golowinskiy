import { Component, ElementRef, OnInit,EventEmitter, Output, Renderer2, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { SearchDataService } from 'src/app/search-data.service';
import { MainService } from '../shared/services/main.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnDestroy {

  @ViewChild('searcher',{static: true }) searcher: ElementRef;
  @ViewChild('menu',{static: true }) menu: ElementRef;
  @Output() status = new EventEmitter();
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public local: DictionaryService,
    public mainService: MainService,
    public searchData: SearchDataService,
    private renderer: Renderer2,
  ) { 
    this.unlistener = this.renderer.listen('window', 'click',(e:Event)=>{
      if(this.isSearchOpen && e.target !== this.menu.nativeElement && e.target !== this.searcher.nativeElement){
        this.$isSearchOpen()
      }
    });
    router.events.subscribe((currRoute) => {
      if(currRoute instanceof NavigationEnd && currRoute.url.includes('search')) {
        this.$isSearchOpen(true);
        this.keyword = localStorage.getItem("searchKey");
      }    
    });

  }
  private unlistener: () => void;
  initialValue:string = 'Поиск'
  keyword = '';
  loading: boolean = false;
  data = [];
  loadingImage = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
  selectEvent(item) {
    this.router.navigate([`/categories/null/products/${item.id}`]);
  }

  isSearchOpen = false;

  customFilter = (data) => data;

  onChangeSearch(val: string) {
    this.mainService.search(this.keyword).subscribe(
      res=> {
        this.data = res.map(obj=> {return {
          ...Object.assign(obj),
          id: obj.prc_ID,
          name: obj.tName
        }})
      }
    );
  }

  $isSearchOpen(status: any = false){
      this.isSearchOpen = status;
      this.status.emit(status);
      this.menu.nativeElement.style.width = status ? '160px' : '0px';
      if(status)
        setTimeout(()=> this.menu.nativeElement.focus(), 500);

  }

  $isSearchOpenMobile(){
    if(this.keyword.length == 0){
    this.isSearchOpen  = !this.isSearchOpen;
    this.status.emit(this.isSearchOpen);
    this.menu.nativeElement.style.width = this.isSearchOpen ? '160px' : '0px';
  }else{
    this.openResults()
  }
  }

  get isKeywordVal(){
    return this.keyword.trim().length !==0;
  }

  openResults(){
    if(this.isSearchOpen && this.keyword.trim().length !== 0){
      this.loading = true;
      localStorage.setItem("searchKey",this.keyword);
      this.mainService.search(this.keyword).subscribe(
        res=> {
          this.loading = false;
          this.data = res.map(obj=> {return {
            ...Object.assign(obj),
            id: obj.prc_ID,
            name: obj.tName
          }});
        this.searchData.searchedData = this.data;
        this.router.navigate([`/`]).then(()=>{
          this.router.navigate([`/search`])
        });
        }
      );
    }
  }
  
  onFocused(e){
    // do something when input is focused
  }

  ngOnDestroy() {
    this.unlistener();
  }

}

