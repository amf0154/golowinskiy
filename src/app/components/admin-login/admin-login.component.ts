import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@components/shared/services/auth.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';
import { NotifyService } from '@components/shared/services/notify.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent{
  public loginForm: FormGroup;
  public submitted: boolean = false;
  public dataLoaded = false;
  public response: string = "";
  constructor(
    public dialogRef: MatDialogRef<AdminLoginComponent>,
    public fb: FormBuilder,
    public dictionary: DictionaryService,
    private notify: NotifyService,
    private auth: AuthService,
    private mainService: MainService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  private emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value.trim().length === 0) {
      return { emptySpaces: 'Введите корректное название' };
    }
    return null;
  }

  getData(){
    this.loginForm = this.fb.group({
      userName : [
        "", 
        [
          Validators.required, 
          this.emptySpacesValidator,
        ]
      ],
      password : [
        "", 
        [
          Validators.required, 
          this.emptySpacesValidator,
        ]
      ]
    })
  }


  get f() {
    return this.loginForm.controls;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  login() {
    this.submitted = true;
    if(!this.loginForm.valid){
      return false
    }else{
//      this.spinner.display(true);
      const {userName, password} = this.loginForm.value;
      this.mainService.authAdmin(userName, password).subscribe((data)=>{
        if(data.role === 'superadmin'){
          sessionStorage.setItem("superadmin",userName+':'+password);
          sessionStorage.setItem("token",data.accessToken)
          this.notify.showNotify("Вы успешно авторизованы!",'');
          this.auth.setSuperAdminStatus(true);
          this.dialogRef.close(true);
        }else{
          if(data.txt)
          this.response = data.txt;
          setTimeout(()=>this.response = "",3000)
        }
//        this.spinner.display(false)
      }, () => {
        this.response = "Ошибка входа, попробуйте позже!"
        setTimeout(()=>this.response = "",3000)
//        this.spinner.display(false);
      });
    }
  }

}
