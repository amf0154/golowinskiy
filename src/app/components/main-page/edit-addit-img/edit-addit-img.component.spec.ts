import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdditImgComponent } from './edit-addit-img.component';

describe('EditAdditImgComponent', () => {
  let component: EditAdditImgComponent;
  let fixture: ComponentFixture<EditAdditImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdditImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdditImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
