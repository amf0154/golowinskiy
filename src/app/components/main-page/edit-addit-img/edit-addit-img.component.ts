import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormControl, ValidationErrors, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AuthService } from "@components/shared/services/auth.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { MainService } from "@components/shared/services/main.service";
import { ModifyAdvertsService } from "@components/shared/services/modify-adverts.service";
import { EnvService } from "src/app/env.service";

@Component({
    selector: "app-edit-addit-img",
    templateUrl: "./edit-addit-img.component.html",
    styleUrls: ["./edit-addit-img.component.css"]
})
export class EditAdditImgComponent {
    private isLinkProvideOnly: boolean = false;
    public submitted: boolean = false;
    advertForm = this.fb.group({
        youtubeLink: [
            this.data.img && this.data.img.video ? this.data.img.video : "",
            [
                // Validators.pattern(
                //     /^(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:\.be\/|be.com\/\S*(?:watch|embed)(?:(?:(?=\/[^&\s\?]+(?!\S))\/)|(?:\S*v=|v\/)))([^&\s\?]+)/
                // )
            ]
        ]
    });

    constructor(
        public dialogRef: MatDialogRef<EditAdditImgComponent>,
        public mainService: MainService,
        public fb: FormBuilder,
        public local: DictionaryService,
        private env: EnvService,
        private authService: AuthService,
        public modifyAdverts: ModifyAdvertsService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        if (this.data.hasOwnProperty("linkOnly")) this.isLinkProvideOnly = this.data.linkOnly;
    }

    get f() {
        return this.advertForm.controls;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    videoPasted(){
        const value = this.advertForm.get('youtubeLink').value;
        if(value && value.length &&['iframe', 'vk.com/video'].every((attr) => value.includes(attr))){
            const link = value.split('src="')[1].split('"')[0];
            if(link && ["oid", "id", "hash"].every((attr) => link.includes(attr))){
                this.advertForm.get('youtubeLink').setValue(link);
            }
        }
    }

    saveData(deleteLink: boolean = false) {
        if(deleteLink){
            this.advertForm.get('youtubeLink').setValue('');
        }
        if (!this.advertForm.valid) {
            return false;
        } else {
            if (this.isLinkProvideOnly) {
                this.dialogRef.close(this.advertForm.value);
                return;
            }
            const body = {
                prc_ID: this.data.advert.prc_ID,
                mediaOrder: this.data.img.imageOrder,
                mediaLink: this.data.img && this.data.img.audio ? this.data.img.audio : null,
                appcode: this.env.shopInfo.cust_id,
                cid: this.authService.getUserId(),
                youtubeLink: this.advertForm.value.youtubeLink.trim() === "" ? null : this.advertForm.value.youtubeLink
            };
            this.mainService.updAudioImage([body]).subscribe((res: Array<boolean>) => {
                this.dialogRef.close(res[0]);
            });
        }
    }
}
