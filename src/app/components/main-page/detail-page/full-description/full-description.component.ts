import { Component, OnInit, ViewChild, ElementRef, Input, AfterContentInit, Output , EventEmitter} from '@angular/core';
import { SharedService } from '@components/shared/services/shared.service';
import { StorageService } from '@components/shared/services/storage.service';

@Component({
  selector: 'app-full-description',
  templateUrl: './full-description.component.html',
  styleUrls: ['./full-description.component.scss']
})
export class FullDescriptionComponent {
  @Output() click = new EventEmitter();
  public showPlayer = true;
  constructor(
    private storage : StorageService,
    public shared: SharedService
  ) { 
    this.showPlayer = true;
  }

  @ViewChild('closer', { static: true }) closeButton: ElementRef
  @Input() headline: string;
  @Input() description: string;
  @Input() hasAudio: any;
  @Input() isShow: boolean;
  @Input() element: any;
  @Input() clickedImageOrder: any;
  @Input() showAudioControlBar: boolean;
  @Input() showEditAudio: boolean;
  @Output() closer = new EventEmitter();
  @Output() switchAudioControl = new EventEmitter();


  tempRecordedAudio() {
    const key: any = typeof this.clickedImageOrder == "number" ? this.clickedImageOrder : "";
    return this.element && this.storage.getTempAudio(this.element.prc_ID, key);
}


  close(){
    this.closer.emit('false');
    this.showPlayer = false;
    this.closeButton.nativeElement.classList.add("conditionalClose");
    setTimeout(()=>this.closeButton.nativeElement.classList.remove("conditionalClose"),1000);
  }

  switchControl(){
    this.switchAudioControl.emit(true);
  }

}
