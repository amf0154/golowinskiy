import {
    Component,
    OnInit,
    EventEmitter,
    AfterContentInit,
    OnDestroy,
    Input,
    Output,
    ViewChild,
    ElementRef,
    HostListener
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MainService } from "../../shared/services/main.service";
import { Message } from "../../shared/models/message.model";
import { of, Subject, Subscription } from "rxjs";
import { DeleteProduct, ShopInfo } from "../../shared/interfaces";
import { OrderService } from "../../shared/services/order.service";
import { AuthService } from "../../shared/services/auth.service";
import { Location } from "@angular/common";
import { EnvService } from "src/app/env.service";
import { StorageService } from "@components/shared/services/storage.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { DomSanitizer, Title } from "@angular/platform-browser";
import { StateService } from "@components/shared/services/state.service";
import { AdditionalImage } from "@components/shared/models/additional-image.model";
import { ModifyAdvertsService } from "@components/shared/services/modify-adverts.service";
import { NotifyService } from "@components/shared/services/notify.service";
import { SharedService } from "@components/shared/services/shared.service";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { ProductService } from "src/app/services/product.service";
import { GalleryItem } from "src/app/interfaces/adverts";
import { AdvertDetail } from "@components/shared/models/advert-list";
import { SetCopiedAudioComponent } from "@components/shared/set-copied-audio/set-copied-audio.component";
import { MatDialog } from "@angular/material";
import { LazyLoaderService } from "src/app/services/lazy-loader.service";
import { EditCommodityComponent } from "@components/commodity/edit-commodity/edit-commodity.component";
import { UserInfo } from "src/app/interfaces/user-info";
import { SiteSettingsService } from "@components/shared/services/sitesettings.service";
import { EditPersonalComponent } from "@components/main-page/auth-page/edit-personal/edit-personal.component";
import { Enums } from "@components/shared/models/enums";
import { FastEditAdvComponent } from "../fast-edit-adv/fast-edit-adv.component";
import { CopyAdvertsService } from "@components/shared/services/copyadverts.service";
import { RepeatService } from "@components/shared/services/repeat.service";
import { DollService } from "@components/shared/services/doll.service";
import { MoveAdvertsService } from "@components/shared/services/moveadverts.service";
const Swal = require("sweetalert2");

@Component({
    selector: "detail-advert",
    templateUrl: "./detail-page.component.html",
    styleUrls: ["./detail-page.component.scss"]
})
export class DetailPageComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild("sliderImg", { static: true }) sliderImg: ElementRef;
    @Input() advId?: any = this.route.snapshot.params.idProduct;
    @Input() catId?: any = this.route.snapshot.params.id;
    @Input() fromUrl?: any = null;
    @Output("close") closer = new EventEmitter<boolean>();
    @Input() set mode(type: Enums.CatalogMode) {
        this.setMode(type);
    }
    @Input() isCabinet?: boolean = window.location.pathname.includes("cabinet");
    @Input() gallery?: any = null;
    public isMobileView: boolean = window.innerWidth < 1079;
    @HostListener("window:resize", ["$event"])
    onResize() {
        this.isMobileView = window.innerWidth < 1079;
        // this.innerWidth = window.innerWidth;
    }
    public isRepeatContent = this.route.snapshot.routeConfig.path === "repeat";
    public element = null;
    public isStudy = false;
    public alive: boolean = true;
    public appCode: string = null;
    public showDescription = true;
    public showProduct = false;
    public showSpinner = true;
    public showPlayer = true;
    public showAdditionalImages = true;
    public additionalImages = [];
    public showEdit = false;
    public showEditAudio = false;
    public showBasket = false;
    public elCurrentId: string;
    public nextElementId;
    public prevElementId;
    public allGallery = [];
    public elementImage_Base: string = null;
    public message: Message = new Message("danger", "");
    public showPrevElementId = false;
    public showNextElementId = false;
    public ctlg_No: string;
    public ctlg_Name: string;
    public sup_ID: string;
    public prc_ID: string;
    public price: number;
    public count: number;
    public articul: any;
    public quantity = 1;
    public prc_Br: string = null;
    public tName: string = null;
    public kolItems: string | number = null;
    public ord_Id: string;
    public dataForDeleteItem: DeleteProduct = null;
    public cid: any = null;
    public audio: any;
    public hasAudio: boolean = false;
    public isShowFullDescr: boolean = false;
    public isRepeat: boolean = false;
    private $subscrIsAuth: Subscription;
    public playerAction = new Subject();
    public videoAction = new Subject();
    public nextSlide = new Subject();
    public showMapButton = false;
    private $subscrSiteSettings: Subscription;
    private selectedMode: Enums.CatalogMode;
    public showCopyLink: boolean;
    public fastModyfyType = Enums.FastModify;
    public descriptionClass = {
        maxHeight: "calc(100vh - 284px)", // without additional sliders and player;   calc(100vh - 375px) // with additional images //
        overflowY: "scroll"
    };
    public file_audio: any = null;
    public searchKey = null;
    public isDollSettingsFrozen = false;
    constructor(
        private mainService: MainService,
        private route: ActivatedRoute,
        private router: Router,
        private notify: NotifyService,
        public orderService: OrderService,
        private location: Location,
        public authService: AuthService,
        public fastModify: ModifyAdvertsService,
        public storage: StorageService,
        public local: DictionaryService,
        public titleService: Title,
        public siteSettings: SiteSettingsService,
        public dialog: MatDialog,
        public sanitizer: DomSanitizer,
        public lazyLoaderService: LazyLoaderService,
        public spinner: SpinnerService,
        public state: StateService,
        public product: ProductService,
        public shared: SharedService,
        public env: EnvService,
        public copyAdverts: CopyAdvertsService,
        public repeatService: RepeatService,
        public moveAdvers: MoveAdvertsService,
        public dollService: DollService
    ) {
        this.checkQueryParams();
    }

    public isEdittable: boolean = false;
    canModifyAdvert() {
        this.showEdit =
            this.shared.isAdmin ||
            (this.isEdittable && this.shared.isAdvOwner(this.element.creater_ID) && this.isCabinet);
        const pageAccess = ["cabinet", "categories/null"].some((e) =>
            window.location.pathname.includes(e)
        );
        this.showEditAudio = this.shared.isAdmin || (this.isEdittable && pageAccess);
    }

    private setMode(type: Enums.CatalogMode) {
        this.selectedMode = type;
        switch (type) {
            case Enums.CatalogMode.SEARCH: {
                break;
            }
            case Enums.CatalogMode.REPEAT: {
                this.isRepeat = true;
                break;
            }
            case Enums.CatalogMode.STUDY: {
                this.isStudy = true;
                break;
            }
        }
    }

    async loadEditComponent() {
        this.showPlayer = false;
        this.dialog
            .open(EditCommodityComponent, {
                width: "100%",
                height: "100vh",
                data: {
                    advert: this.element,
                    prc_ID: this.editPrcId,
                    isCopy: false
                }
            })
            .afterClosed()
            .subscribe((isUpdated) => {
                this.titleService.setTitle(this.element.tName);
                this.showEditComponent = false;
                if (isUpdated === true) {
                    this.closeEditComponent();
                } else if (isUpdated == 2) {
                    this.close(isUpdated);
                }
                this.showPlayer = true;
            });
    }

    productName: string;
    productPrice: string;
    setProductPriceName(element: AdvertDetail) {
        this.titleService.setTitle(element.tName);
        this.productName = element.tName;
        this.productPrice = element.prc_Br;
    }

    checkQueryParams() {
        const params = this.route.snapshot.queryParams;
        if (params && Reflect.has(params, "search")) {
            if (params.search) {
                this.searchKey = params.search;
                this.selectedMode = Enums.CatalogMode.SEARCH;
            }
        } else if (params && Reflect.has(params, "repeat")) {
            this.isRepeat = true;
        } else if (params && Reflect.has(params, "study")) {
            this.isStudy = true;
        }
    }

    showEditComponent: boolean = false;
    editPrcId: any = null;
    fastEditAdvert(el) {
        this.catId;
        if (el.id && el.prc_ID) {
            this.editPrcId = el.prc_ID;
        } else {
            this.editPrcId = this.advId;
        }
        this.showEditComponent = true;
        this.loadEditComponent();
    }

    closeEditComponent() {
        this.isClickedDetailImg = false;
        // this.spinner.display(true);
        this.showPlayer = true;
        this.getProduct(this.editPrcId, true);
        this.showEditComponent = false;
    }

    public isSlideshowAdvert: boolean = false;
    public isSlideshowExtraPict: boolean = false;
    public isStartAudio: boolean = this.env.shopDetails.playAudio;
    public contentType: number = 1;
    ngOnInit() {
        this.mainService.getShopInfo().subscribe((res: ShopInfo) => {
            this.appCode = res.cust_id;
            this.showMapButton = res.isMapShow;
            this.showCopyLink = res.isShowMediaLink;
            //this.isOldVersion = res.pageKind;
            //this.isStartAudio = res.playAudio;
            this.isEdittable = res.isEditing;
            this.dataForDeleteItem = {
                cid: this.authService.getUserId(),
                appCode: this.appCode,
                cust_ID: res.cust_id,
                prc_ID: this.advId
            };
            this.loadGalery(this.cid);
            this.showSpinner = true;
            this.userInfo();
        });
    }

    private userInfo() {
        this.$subscrIsAuth = this.authService.isAuthenticatedObservable().subscribe((loggedIn) => {
            if (loggedIn) {
                this.authService.getUserInfo().subscribe((uData: UserInfo) => {
                    this.$subscrSiteSettings = this.siteSettings.getSiteSettings().subscribe((r) => {
                        this.contentType = r.slideType;
                        this.countOfRepeats = r.audioRepeats;
                    });
                    this.siteSettings.getFrozeSettingsObs().subscribe((cfg) => {
                        const isFreeze = cfg[this.siteSettings.getDepartmentTypeByRoute(this.fromUrl)];
                        const r = isFreeze !== undefined ? isFreeze : cfg[Enums.SiteDepartment.MAIN];
                        this.isDollSettingsFrozen = r;
                        this.isSlideshowAdvert = r ? false : uData.isSlideshowAdvert;
                        this.isSlideshowExtraPict = r ? false : uData.isSlideshowExtraPict;
                        this.isStartAudio = r ? false : !uData.isNoStartAudio;
                    });
                    if (localStorage.getItem("slideType")) this.contentType = Number(localStorage.getItem("slideType"));
                });
            } else {
                if (this.$subscrSiteSettings) this.$subscrSiteSettings.unsubscribe();
            }
        });
    }

    public changeContentType($event) {
        this.userInfo();
        this.getProduct(this.element.prc_ID, false, true);
    }

    openPersonalMenu(type: Enums.UserSettings) {
        this.authService.getUserInfo(this.authService.getUserId()).subscribe((data) => {
            this.dialog
                .open(EditPersonalComponent, {
                    data: {
                        ...data,
                        type: type,
                        route: this.fromUrl
                    }
                })
                .afterClosed()
                .subscribe((r: any) => {
                    this.changeContentType(null);
                });
        });
    }

    sanitize(param, url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(param + ":" + url);
    }

    ngAfterContentInit() {
        //  document.body.style.backgroundColor = "#179590";
    }

    private goNextAdvert() {
        if (this.showNextElementId) {
            this.audio = false;
            setTimeout(() => this.routeNextProduct(this.element), 200);
        } else {
            if (this.isSlideshowExtraPict || this.isSlideshowAdvert) this.close();
        }
    }

    inputAdditSliderActions($event) {
        switch ($event) {
            // no more additional audio images
            case 1: {
                this.playerAction.next(2); // stop playing without repeat (change button)
                if (this.isSlideshowExtraPict && this.isSlideshowAdvert) {
                    this.goNextAdvert();
                } else if (this.isSlideshowExtraPict && !this.isSlideshowAdvert && this.isStartAudio) {
                    this.getProduct(this.element.prc_ID, false, true);
                } else {
                    if (this.isStartAudio) {
                        this.playerAction.next(1); // start playing audio again;
                    }
                }
                break;
            }
            case 3: {
                this.spinner.display(true);
                this.getProduct(this.element.prc_ID, true);
                break;
            }
        }
    }

    // actions from youtube player
    videoActions($event) {
        switch ($event.key) {
            // video has end;
            case 1: {
                if (this.isStartAudio) {
                    // if ended current
                    // // is additional image audio
                    if (this.youtubeLink !== this.element.youtube) {
                        if (this.isSlideshowExtraPict) {
                            this.nextSlide.next(true);
                        } else {
                            this.videoAction.next(1); // start playing audio again;
                        }
                    } else {
                        // // stop base audio
                        // if(this.element && this.element.mediaLink && this.element.mediaLink !== ''){
                        //     setTimeout(() => this.playerAction.next(3),100);
                        // }
                        // default audio
                        if (!this.isSlideshowExtraPict && this.isSlideshowAdvert) {
                            this.goNextAdvert();
                        } else {
                            if (this.isSlideshowExtraPict) {
                                // check if we have additional images before run slides
                                if (
                                    this.element &&
                                    this.element.additionalImages &&
                                    this.element.additionalImages.length
                                ) {
                                    this.nextSlide.next(true); /// ЗАПУСК с ПЕРВОГО СЛАЙДА!!!
                                } else {
                                    this.goNextAdvert();
                                }
                            } else this.videoAction.next(1); // start playing audio again;
                        }
                    }
                }
                // else {
                //     this.playerAction.next(2); // stop playing without repeat (change button)
                // }
                break;
            }
            // video started;
            case 2: {
                this.playerAction.next(3);
                break;
            }
        }
    }
    public countOfRepeats: number = 1;
    public currentRepeat: number = 1;
    private repeater(repeat, next) {
        this.currentRepeat += 1;
        if (this.currentRepeat <= this.countOfRepeats) {
            repeat();
        } else {
            next();
            this.currentRepeat = 1;
        }
    }
    inputPlayerAction(status: number): void {
        switch (status) {
            // when current audio ended;
            case 1: {
                if (this.isStartAudio) {
                    // is additional image audio
                    if (this.audio !== this.element.mediaLink) {
                        if (this.isSlideshowExtraPict) {
                            // this.nextSlide.next(true);
                            this.repeater(
                                () => this.playerAction.next(1),
                                () => this.nextSlide.next(true)
                            );
                        } else {
                            this.playerAction.next(1); // start playing audio again;
                        }
                    } else {
                        // default audio
                        if (!this.isSlideshowExtraPict && this.isSlideshowAdvert) {
                            this.repeater(
                                () => this.playerAction.next(1),
                                () => this.goNextAdvert()
                            );
                        } else {
                            if (this.isSlideshowExtraPict) {
                                // check if we have additional images before run slides
                                if (
                                    this.element &&
                                    this.element.additionalImages &&
                                    this.element.additionalImages.length
                                ) {
                                    //this.nextSlide.next(true); /// ЗАПУСК с ПЕРВОГО СЛАЙДА!!!
                                    this.repeater(
                                        () => this.playerAction.next(1),
                                        () => this.nextSlide.next(true)
                                    );
                                } else {
                                    if (this.isSlideshowAdvert) {
                                        this.repeater(
                                            () => this.playerAction.next(1),
                                            () => this.goNextAdvert()
                                        );
                                    } else {
                                        this.repeater(
                                            () => this.playerAction.next(1),
                                            () => this.playerAction.next(2)
                                        );
                                    }
                                }
                            } else this.playerAction.next(1); // start playing audio again;
                        }
                    }
                } else {
                    this.playerAction.next(2); // stop playing without repeat (change button)
                }
                break;
            }
            // pressed button 'REPEAT ON';
            case 2: {
                this.isStartAudio = true;
                break;
            }
            // pressed button 'REPEAT OFF';
            case 3: {
                this.isStartAudio = false;
                break;
            }
        } // end switch for playerr actions;
    }

    loadGalery(cid: any) {
        const request =
            this.searchKey != null
                ? this.mainService.searchDetailPage(this.catId, this.appCode, cid, this.searchKey)
                : this.isRepeat || this.isRepeat
                ? this.mainService.getMaterial(this.selectedMode)
                : this.gallery
                ? of(this.gallery)
                : this.mainService.getProducts(this.catId, this.appCode, cid);
        request.subscribe(
            (res) => {
                this.getProduct(this.advId);
                if (res.length !== 0) {
                    res.forEach((element, i) => {
                        this.allGallery.push({
                            i,
                            prc_ID: element.prc_ID,
                            src:
                                this.env.apiUrl +
                                "/api/Img?AppCode=" +
                                this.appCode +
                                "&ImgFileName=" +
                                element.image_Base,
                            default:
                                this.env.apiUrl +
                                "/api/Img?AppCode=" +
                                this.appCode +
                                "&ImgFileName=" +
                                element.image_Base,
                            name: element.image_Base
                        });
                    });
                    this.setSliderImageById(this.advId);
                    this.showPrevElementId = this.advId != this.allGallery[0].prc_ID;
                    this.showNextElementId = this.advId != this.allGallery[this.allGallery.length - 1].prc_ID;
                } else {
                    this.notify.showNotify("Что-то пошло не так, Вы будете перенаправлены на главную страницу!", "");
                    setTimeout(() => this.router.navigate(["/"]), 2000);
                }
            },
            () => {
                this.notify.showNotify(
                    "Не удается загрузить галерею, Вы будете перенаправлены на главную страницу!",
                    ""
                );
                setTimeout(() => this.router.navigate(["/"]), 2000);
            }
        );
    }

    deleteSpace = (text) => (text.tName ? text.tName.trim() : "");
    youtubeLink: string = null;
    recordFile: any = false;
    getProduct(productId, updInService = false, fakeReloading = false) {
        this.currentRepeat = 1;
        this.storage.clearTempAudios();
        this.audio = false;
        this.showAudioControlBar = false;
        this.showSpinner = true;
        this.showProduct = false;
        this.youtubeLink = null;
        this.selectedImg = null;
        this.previousName = null;
        const request = !fakeReloading
            ? this.mainService.getProductWithAudio(
                  productId,
                  this.isCabinet ? this.authService.getUserId() : this.appCode,
                  this.appCode,
                  this.authService.getUserId()
              )
            : of(this.element);
        request.subscribe(
            (res: AdvertDetail) => {
                if (res.youtube !== "" && res.youtube !== null) this.youtubeLink = res.youtube;
                this.setProductPriceName(res);
                this.element = res;
                if (updInService) {
                    this.product.updItem(this.element);
                }
                if (this.isCabinet) {
                    this.showBasket = false;
                    this.cid = this.authService.getUserId();
                } else {
                    this.showBasket = true;
                    this.cid = "";
                }
                this.canModifyAdvert();
                if (this.element.additionalImages.length != 0) {
                    this.element.additionalImages = this.element.additionalImages.map((el, i) => {
                        return {
                            ...Object.assign({}, el, { index: i + 1 })
                        };
                    });
                }
                setTimeout(() => {
                    this.hasAudio = (this.audio = res.mediaLink) ? !!res.mediaLink : false ? true : false;
                    const hasSlides = res.additionalImages && res.additionalImages.length;
                    const noDefaultMedia =
                        (this.contentType === 1 && !this.hasAudio) || (this.contentType === 2 && !this.element.youtube);
                    if (this.isSlideshowExtraPict && this.isSlideshowAdvert && noDefaultMedia) {
                        // play base, addit and got other adv;
                        hasSlides ? this.nextSlide.next(1) : this.goNextAdvert();
                    }
                    if (!this.isSlideshowExtraPict && this.isSlideshowAdvert && noDefaultMedia) {
                        this.goNextAdvert(); // go next advert;
                    }
                    if (hasSlides && this.isSlideshowExtraPict && !this.isSlideshowAdvert) {
                        if (!this.hasAudio && this.contentType === 1) {
                            this.nextSlide.next(true);
                        } else if (this.contentType === 2) {
                            if (noDefaultMedia) this.nextSlide.next(true);
                        }
                    }
                });

                this.elementImage_Base = res.t_imageprev;
                this.showProduct = true;
                if (res.mediaLink && this.element.additionalImages.length > 0) {
                    this.descriptionClass.maxHeight = "calc(100vh - 450px)"; // minus additional images height block and music player height block;
                } else if (!res.mediaLink && this.element.additionalImages.length > 0) {
                    this.descriptionClass.maxHeight = "calc(100vh - 430px)"; // minus additional images height block;
                } else if (!res.mediaLink && this.element.additionalImages.length == 0) {
                    this.descriptionClass.maxHeight = "calc(100vh - 305px)"; // minus additional images height block;
                }
                this.showSpinner = false;
                this.spinner.display(false);
            },
            () => this.spinner.display(false)
        );
    }

    public mainImageIndex: number = 0;
    setSliderImageById(id) {
        this.updateUrl(id);
        const mainImageName = this.allGallery.filter((item) => item.prc_ID == id);
        if (mainImageName.length !== 0) this.elementImage_Base = mainImageName[0].name;
        else {
            this.elementImage_Base = "";
            Swal.fire({
                icon: "error",
                title: "Обьявление не найдено!",
                showConfirmButton: false,
                timer: 3000,
                willClose: () => {
                    this.router.navigate(["/"]);
                }
            });
        }
    }

    setAudio(newLink: string = null) {
        this.audio = this.showPlayer = newLink ? newLink : this.element.mediaLink;
    }

    updateUrl(productId: number) {
        const search =
            this.selectedMode === Enums.CatalogMode.SEARCH ? "?search=" + localStorage.getItem("searchKey") : "";
        const replacePatch = this.isCabinet
            ? `/cabinet/categories/${this.catId}/products/${productId}`
            : `/categories/${this.catId}/products/${productId}` + search;
        this.location.replaceState(replacePatch);
    }

    private showMessage(text: string, type: string = "danger") {
        this.message = new Message(type, text);
        window.setTimeout(() => {
            this.message.text = "";
        }, 2000);
    }

    close(checkItems: boolean = false) {
        this.titleService.setTitle(this.env.shopInfo.shortDescr);
        // if gallery not empty = page is modal with saved gallery;
        if (this.gallery) {
            this.closer.emit(true);
            this.setBackRouteByMode();
        } else {
            // page was opened by link;
            if (this.isCabinet) {
                const defaultRoute = this.storage.fromCabinetAllItems
                    ? "/catcabinet"
                    : `/cabinet/categories/${this.catId}/products`;
                this.router.navigate([
                    checkItems ? (this.allGallery.length === 1 ? "/cabinet" : defaultRoute) : defaultRoute
                ]);
            } else {
                const defaultRoute = `/categories/${this.catId}/products`;
                this.router.navigate([checkItems ? (this.allGallery.length === 1 ? "/" : defaultRoute) : defaultRoute]);
            }
        }
    }

    private setBackRouteByMode() {
        switch (this.selectedMode) {
            case Enums.CatalogMode.SEARCH: {
                this.location.replaceState("/search");
                break;
            }
            case Enums.CatalogMode.REPEAT: {
                this.location.replaceState("/repeat");
                break;
            }
            case Enums.CatalogMode.STUDY: {
                this.location.replaceState("/study");
                break;
            }
        }
    }

    editProduct(el) {
        if (el.id && el.prc_ID) {
            this.router.navigate(["/modify"], {
                queryParams: {
                    category: this.catId,
                    adv: el.prc_ID
                }
            });
        } else {
            this.router.navigate(["/modify"], {
                queryParams: {
                    category: this.catId,
                    adv: this.advId
                }
            });
        }
    }

    deleteProduct(element?) {
        this.showSpinner = true;
        const dataForDeleteItem = {
            cid: this.authService.getUserId(),
            appCode: this.appCode,
            cust_ID: element.cust_id,
            prc_ID: element.prc_ID
        };
        this.mainService.deleteProduct(dataForDeleteItem).subscribe(
            (res) => {
                if (element.mediaLink.includes("cloudfront"))
                    this.mainService.deleteCloudContent(element.mediaLink).subscribe((res) => {});
                this.showSpinner = false;
                if (res) {
                    const indexOfDeletingItem = this.allGallery.indexOf(
                        this.allGallery.find((el) => el.prc_ID === element.prc_ID)
                    );
                    if (indexOfDeletingItem != -1) {
                        const prevId = this.allGallery[indexOfDeletingItem - 1]
                            ? this.allGallery[indexOfDeletingItem - 1].prc_ID
                            : null;
                        const nextId = this.allGallery[indexOfDeletingItem + 1]
                            ? this.allGallery[indexOfDeletingItem + 1].prc_ID
                            : null;
                        this.allGallery.splice(indexOfDeletingItem, 1);
                        this.product.deleteItems(
                            this.product.gallery.findIndex((i: GalleryItem) => i.prc_ID == element.prc_ID),
                            1
                        );
                        if (nextId) {
                            for (var i = 0; i < this.allGallery.length; i++) {
                                if (nextId == this.allGallery[i].prc_ID) {
                                    this.nextElementId = this.elCurrentId = this.allGallery[i].prc_ID;
                                    this.showNextElementId = !!this.allGallery[i + 1];
                                    this.prevElementId = prevId;
                                    this.setSliderImageById(this.nextElementId);
                                    this.getProduct(this.nextElementId);
                                }
                            }
                        } else if (prevId) {
                            for (var i = 0; i < this.allGallery.length; i++) {
                                if (prevId == this.allGallery[i].prc_ID) {
                                    this.prevElementId = this.elCurrentId = this.allGallery[i].prc_ID;
                                    this.showPrevElementId = !!this.allGallery[i - 1];
                                    this.showNextElementId = false;
                                    this.setSliderImageById(this.prevElementId);
                                    this.getProduct(this.prevElementId);
                                }
                            }
                        } else {
                            // all items in detail gallery deleted;
                            this.close(true);
                        }
                    } else {
                        // deleting element not found
                        this.close(true);
                    }
                } else {
                    this.showSpinner = false;
                    let timerInterval;
                    Swal.fire({
                        icon: "error",
                        title: "Обьявление не удалено!",
                        showConfirmButton: false,
                        timer: 1000,
                        willClose: () => {
                            clearInterval(timerInterval);
                        }
                    });
                }
            },
            (error) => {
                this.showSpinner = false;
                let timerInterval;
                Swal.fire({
                    icon: "error",
                    title: "Обьявление не удалено!",
                    showConfirmButton: false,
                    timer: 1000,
                    willClose: () => {
                        clearInterval(timerInterval);
                    }
                });
            },
            () => (this.showSpinner = false)
        );
    }

    swipeleft() {
        this.goNextAdvert();
    }

    swiperight() {
        if (this.showPrevElementId) this.routePrewProduct(this.element, true);
    }

    //additional images BEGIN
    public selectedImg: any = null;
    public previousName: any = null;
    public addImgYoutube: any = false;
    public initialElementImage_Base: any = null;
    public isClickedDetailImg: boolean = false;
    public clickedImageOrder: any = null;
    public mouseOverImg(el: AdditionalImage) {
        this.storage.clearTempAudios();
        if (el.t_image) {
            this.showAudioControlBar = false;
            if (this.clickedImageOrder === el.imageOrder || this.clickedImageOrder == null)
                this.isClickedDetailImg = !this.isClickedDetailImg;
            if (this.isClickedDetailImg) {
                this.resetSelectedAddImg(null, el.audio);
                if (el.video && this.contentType === 2) {
                    this.youtubeLink = el.video;
                    this.playerAction.next(3);
                } else {
                    this.youtubeLink = null;
                }
                if (el.audio && el.audio != "") {
                    this.showPlayer = false;
                    setTimeout(() => this.setAudio(el.audio));
                    if (this.contentType === 2) {
                        this.playerAction.next(3);
                    }
                } else {
                    if (this.audio != this.element.mediaLink) {
                        this.showPlayer = false;
                        setTimeout(() => this.setAudio());
                    }
                }
                this.initialElementImage_Base = Object.assign({}, { link: this.elementImage_Base });
                this.previousName = el;
                this.elementImage_Base = el.t_image;
                this.addImgYoutube = el.t_image;
                this.selectedImg = el.t_image;
                this.clickedImageOrder = el.imageOrder;
            } else {
                this.resetSelectedAddImg(el.audio);
                this.clickedImageOrder = null;
                this.youtubeLink = (this.element as any).youtube;
            }
        }
    }

    tempRecordedAudio() {
        const key: any = typeof this.clickedImageOrder == "number" ? this.clickedImageOrder : "";
        return this.element && this.storage.getTempAudio(this.element.prc_ID, key);
    }

    public resetSelectedAddImg(txt = null, hasImgAudio = null) {
        if (this.previousName && this.previousName.t_image) {
            this.elementImage_Base = this.initialElementImage_Base.link;
            this.selectedImg = null;
            this.previousName = null;
            if (this.previousName && this.previousName.t_imageprev) this.addImgYoutube = false;
            this.previousName = null;
        }
        if (txt) {
            this.showPlayer = false;
            setTimeout(() => this.setAudio());
        } else {
            if (this.audio !== this.element.mediaLink) {
                this.showPlayer = false;
                hasImgAudio ? this.setAudio() : setTimeout(() => this.setAudio());
            }
        }
    }
    //additional images END;

    public index: number = 0;
    public page: number = 1;
    public blockNext: boolean = false;
    public routeNextProduct(el) {
        this.file_audio = null;
        this.showPrevElementId = true;
        this.audio = false;
        this.showSpinner = true;
        if (this.elCurrentId == undefined) {
            this.elCurrentId = el.prc_ID;
        }

        for (var i = 0; i < this.allGallery.length - 1; i++) {
            if (this.elCurrentId == this.allGallery[i].prc_ID) {
                this.nextElementId = this.allGallery[i + 1].prc_ID;
                this.index = i + 2;
                this.showNextElementId = !!this.allGallery[i + 2];
                this.setSliderImageById(this.nextElementId);
                this.getProduct(this.nextElementId);
            }
        }
        this.elCurrentId = this.nextElementId;
    }

    public showFullDescription: boolean = false;
    public limitDescription: number = 210;
    public limiter = (text: string, limit: number = this.limitDescription) =>
        text ? (text.length > limit && !this.showFullDescription ? text.substring(0, limit - 3) + "..." : this.replaceLinkText(text)) : "";
    public showDescrButton = () => (this.showFullDescription = !this.showFullDescription);
    public routePrewProduct(el, isMobile: boolean = false) {
        this.file_audio = null;
        this.audio = false;
        this.showNextElementId = true;
        this.showSpinner = true;
        if (this.elCurrentId == undefined) {
            this.elCurrentId = el.prc_ID;
        }
        for (var i = 0; i < this.allGallery.length; i++) {
            if (this.elCurrentId == this.allGallery[i].prc_ID) {
                this.prevElementId = this.allGallery[i - 1].prc_ID;
                this.showPrevElementId = !!this.allGallery[i - 2];
                this.setSliderImageById(this.prevElementId);
                this.getProduct(this.prevElementId);
            }
        }
        this.elCurrentId = this.prevElementId;
    }

    public addToCart(el) {
        this.showSpinner = true;
        for (var i = 0; i < this.allGallery.length; i++) {
            if (el.id == this.allGallery[i].id) {
                if (this.allGallery[i].strike) {
                    this.allGallery[i].strike = false;
                } else {
                    this.allGallery[i].strike = true;
                }
                break;
            }
        }
        this.orderService.doClick();
        let registerOrder = this.orderService.countKol() === 0 ? this.mainService.registerOrder() : of({});
        registerOrder.subscribe(() => {
            this.mainService.getShopInfo().subscribe(
                (res) => {
                    this.showMapButton = res.isMapShow;
                    this.mainService.getProduct(el.prc_ID, res.cust_id, res.cust_id).subscribe((productRes: any) => {
                        this.ctlg_No = productRes.ctlg_No;
                        this.ctlg_Name = productRes.ctlg_Name;
                        this.sup_ID = productRes.sup_ID;
                        this.tName = productRes.tName;
                        this.prc_Br = productRes.prc_Br;

                        if (window.localStorage.getItem("kolItems")) {
                            this.kolItems = window.localStorage.getItem("kolItems");
                        } else {
                            this.kolItems = 1;
                        }
                        let data = {
                            OrdTtl_Id: this.orderService.getOrderId(),
                            OI_No: this.orderService.countKol() + 1,
                            Ctlg_No: productRes.ctlg_No,
                            Qty: 1,
                            Ctlg_Name: productRes.ctlg_Name,
                            Sup_ID: productRes.sup_ID,
                            Descr: productRes.tName
                        };
                        this.mainService.addToCart(data).subscribe((res: any) => {
                            this.showSpinner = false;
                            if (res.result == true) {
                                this.orderService.addToOrder(
                                    el,
                                    productRes.ctlg_No,
                                    productRes.ctlg_Name,
                                    productRes.sup_ID,
                                    productRes.t_imageprev
                                );
                                this.showMessage(`${res.message}`, "success");
                                this.showProduct = false;
                            }
                        });
                    });
                },
                (error) => alert(error.error.message)
            );
        });
    }
    public showHideDescription: boolean = true;
    public closeDescription() {
        this.showPlayer = true;
        this.isShowFullDescr = false;
    }
    public fullDescription() {
        this.showPlayer = false;
        this.isShowFullDescr = true;
    }

    public checkPrice() {
        return this.element.prc_Br == "" ? false : Number.parseInt(this.element.prc_Br) !== 0;
    }

    public changeAudioImgLink(index, link = null) {
        this.element.additionalImages.find((img) => img.imageOrder == index).audio = link;
    }

    hasProtocol(link) {
        return link && link.startsWith("https://") ? link : "https://" + link;
    }

    private fastUpdDefaultAudio(audio = "") {
        this.product.updItem(this.element); // update gallery data;
        const { prc_ID, tName } = this.element as any;
        this.fastModify.saveNewAudio(prc_ID, tName, audio).subscribe((resp) => {
            if (resp[0]) {
                this.notify.showNotify(this.local.getTranslate("adv_updated", `Обьявление успешно обновлено`), "");
            }
        });
    }

    private fastUpdAudioImgLink(index, link = null) {
        this.mainService
            .updAudioImage([
                {
                    prc_ID: this.element.prc_ID,
                    mediaOrder: index,
                    mediaLink: link,
                    appcode: this.appCode,
                    cid: this.authService.getUserId()
                }
            ])
            .subscribe((resp) => {
                if (resp[0]) {
                    this.notify.showNotify(this.local.getTranslate("adv_updated", `Обьявление успешно обновлено`), "");
                    if (!link) {
                        this.setAudio();
                    }
                }
            });
    }

    $isAudioUploaded($event) {
        this.showAudioControlBar = false;
        switch ($event.type) {
            case "uploaded": {
                const audioLink = this.hasProtocol($event.link);
                this.hasAudio = true;
                this.audio = audioLink;
                if (!$event.isAudioImg) {
                    this.element.mediaLink = audioLink;
                    this.fastUpdDefaultAudio(audioLink);
                } else {
                    this.fastUpdAudioImgLink($event.index, audioLink);
                    this.changeAudioImgLink($event.index, audioLink);
                    this.showPlayer = true;
                }
                break;
            }
            case "uploaded-temp": {
                break;
            }
            case "deleted-temp": {
                break;
            }
            case "deleted": {
                this.hasAudio = false;
                this.audio = "";
                if (!$event.isAudioImg) {
                    this.element.mediaLink = null;
                    this.fastUpdDefaultAudio();
                } else {
                    this.fastUpdAudioImgLink($event.index);
                    this.changeAudioImgLink($event.index);
                }
                break;
            }
        }
        if ($event.link) {
            this.hasAudio = $event.link;
        }
    }

    showAudioControlBar: boolean = false;
    switchAudioControl() {
        this.showAudioControlBar = !this.showAudioControlBar;
        if (!this.showAudioControlBar) {
            this.setAudio();
        }
    }

    ngOnDestroy(): void {
        this.storage.fromCabinetAllItems = false;
        if (this.$subscrSiteSettings) this.$subscrSiteSettings.unsubscribe();
        this.$subscrIsAuth.unsubscribe();
        this.getProduct = function () {};
        this.alive = false;
    }

    setCopiedAudio() {
        const link = localStorage.getItem("sharedAudioLink");
        if (link && link.length > 20) {
            this.dialog
                .open(SetCopiedAudioComponent, {
                    width: "400px",
                    data: {
                        prc_ID: this.element.prc_ID,
                        appCode: this.appCode,
                        images: this.element.additionalImages
                    }
                })
                .afterClosed()
                .subscribe((result) => {
                    if (result) {
                        this.spinner.display(true);
                        this.getProduct(this.element.prc_ID, true);
                    }
                });
        } else {
            Swal.fire({
                icon: "error",
                title: "Ссылка на аудио не выбрана",
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    copyLink(): void {
        let selBox = document.createElement("textarea");
        selBox.style.position = "fixed";
        selBox.style.left = "0";
        selBox.style.top = "0";
        selBox.style.opacity = "0";
        selBox.value = this.elementImage_Base;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand("copy");
        document.body.removeChild(selBox);
        this.notify.showNotify(
            this.local.getTranslate("link_image_copied", "Ссылка на изображение скопирована в буфер"),
            ""
        );
    }

    public fastEdit(editType: Enums.FastModify): void {
        const dialogRef = this.dialog.open(FastEditAdvComponent, {
            width: "400px",
            data: {
                ...this.element,
                editType: editType
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.notify.showNotify(
                    this.local.getTranslate("adv_updating", `Обьявление поставлено в очередь на обновление`),
                    "",
                    "warning"
                );
                this.productName = result.TName;
                // if (result.mediaLink == null || result.mediaLink === 0) {
                //     product.mediaLink = false;
                // }

                //   this.product.setGallery(product);
            }
        });
    }

    replaceLinkText(str: string) {
        if(!str) {
            return ''
        }
        const regex = /(https?:\/\/[^\s]+)/g;
        const replacedStr = str.replace(regex, '<a class="white_link" target="_blank" href="$1">$1</a>');
        
        return replacedStr;
    }
}
