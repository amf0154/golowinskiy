import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';

import { MainService } from '../../shared/services/main.service';
import { AuthService } from '../../shared/services/auth.service';
import {OrderService} from '../../shared/services/order.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { Observable } from 'rxjs';
import { SearchDataService } from 'src/app/search-data.service';
import { EnvService } from 'src/app/env.service';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { ModifyAdvertsService } from '@components/shared/services/modify-adverts.service';
const Swal = require('sweetalert2');
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  showBasket = false
  hideCabinet = false
  showMenu = false
  hideOnMainPage = false
  isThatCabinetPage: boolean = false;
  cabinetLinkParameter: number = 2; // 1  or 2;
  
  public isAuthenticated: Observable<boolean>;
  public isAdmin: boolean = false;
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private env: EnvService,
    public mainService: MainService,
    public authService: AuthService,
    public orderService: OrderService,
    public searchData: SearchDataService,
    public local: DictionaryService,
    public copyAdverts: CopyAdvertsService,
    public modifyAdverts: ModifyAdvertsService
  ) {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
    this.isAdmin = this.authService.getUserId() == this.mainService.getCustId() && this.mainService.getCustId() != null;
  }

  ngOnInit() {
    this.cabinetLinkParameter = this.router.routerState.snapshot.url.includes('catcabinet') ? 1 : 2;
    this.isThatCabinetPage = this.router.routerState.snapshot.url.includes('cabinet');
    if(this.isThatCabinetPage){
      this.showBasket = false
      this.hideCabinet = false
      this.hideOnMainPage = true
    }
    else{
      this.showBasket = true
      this.hideCabinet = true
      this.hideOnMainPage = false
    }
  }

  getHomeLink(){
    switch(this.cabinetLinkParameter){
      case 1: {
        return {
          text: 'Каталог',
          url: '/cabinet'
        }
      }
      case 2: {
        return {
          text: 'Все объявления',
          url: '/catcabinet'
        }
      }
      default: {
        return {
          text: 'Главная',
          url: '/'
        }
      }
    }
  }
  
  toOrder(){
    this.router.navigate([`${window.location.pathname}/order`])
  }
  openMenu(){
    this.showMenu = true
  }
  onLogout(){
    this.hideMenu()
    this.authService.logout()
  }
  back(){
    this.hideMenu()
    this.router.navigate(['/'])
  }
  closeMenu(){
    this.hideMenu()
  }

  hideMenu(){
    this.showMenu = false
  }

  cartClick() {
    if(this.router.url === '/') {
      this.router.navigate(['/order'])
    } else {
      this.router.navigate([`/categories/${this.route.snapshot.params.id}/products/order`])
    }
  }

  personal(){
    this.router.navigate(['/auth/personal']);
  }
  subscribe(){
    this.router.navigate(['subscribe']);
  }

  manual(){
    Swal.fire({
      title: '<strong>Инструкция</strong>',
    //  icon: 'info',
      html: this.mainService.shopInfo.manual,
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Понятно',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      // cancelButtonText:
      //   '<i class="fa fa-thumbs-down"></i>',
      // cancelButtonAriaLabel: 'Thumbs down'
    });
  }

  editCat(){
    window.open(this.env.adminUrl, '_blank');
   // this.router.navigate(['/modifycatalog']);
  }
}
