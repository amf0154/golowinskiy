import { Component, OnDestroy, OnInit } from '@angular/core';

import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { EnvService } from 'src/app/env.service';
import { Observable, Subscription } from 'rxjs';
import { MainService } from '@components/shared/services/main.service';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { ModifyAdvertsService } from '@components/shared/services/modify-adverts.service';
const Swal = require('sweetalert2');
@Component({
  selector: 'app-cabinet-user',
  templateUrl: './cabinet-user.component.html',
  styleUrls: ['./cabinet-user.component.scss']
})
export class CabinetUserComponent implements OnInit, OnDestroy {

  showUser = false
  public isAdmin: boolean = false;
  public isAuthenticated: boolean = false;
  constructor(
    public authService: AuthService,
    public router : Router,
    public mainService: MainService,
    public modifyAdverts: ModifyAdvertsService,
    private $subscrIsAuth: Subscription,
    public local: DictionaryService,
    public copyAdverts: CopyAdvertsService,
    private env: EnvService
  ) { }

  ngOnDestroy(): void {
    this.$subscrIsAuth.unsubscribe();
  }

  ngOnInit() {
    this.isAdmin = this.authService.getUserId() === this.mainService.getCustId();
    this.$subscrIsAuth = this.authService.isAuthenticatedObservable().subscribe(res => this.isAuthenticated = res)
    if(window.location.pathname.includes('cabinet')){
      this.showUser = true
    }
    else{
      this.showUser = false
    }
  } 

  onLogout(){
    this.authService.logout()
  }
  personal(){
    this.router.navigate(['/auth/personal'])
  }
  admin(){
    window.open(this.env.adminUrl, '_blank');
  }
  subscribe(){
    this.router.navigate(['subscribe'])
  }

  editCat(){
    window.open(this.env.adminUrl, '_blank');
   // this.router.navigate(['/modifycatalog']);
  }
  copyCat(){
    this.router.navigate(['/copy-catalog'])
  }
  manual(){
    Swal.fire({
      title: '<strong>Инструкция</strong>',
    //  icon: 'info',
      html: this.mainService.shopInfo.manual,
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Понятно',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      // cancelButtonText:
      //   '<i class="fa fa-thumbs-down"></i>',
      // cancelButtonAriaLabel: 'Thumbs down'
    });
  }

}
