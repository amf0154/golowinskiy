import { Component, OnDestroy, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OrderService } from "../../shared/services/order.service";
import { MainService } from "../../shared/services/main.service";
import { Message } from "@components/shared/models/message.model";
import { of, Subscription } from "rxjs";
import { StorageService } from "../../shared/services/storage.service";
import { AuthService } from "../../shared/services/auth.service";
import { CategoryItem } from "../../categories/categories.component";
import { CategoriesService } from "../../shared/services/categories.service";
import { EnvService } from "src/app/env.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { SearchDataService } from "src/app/search-data.service";
import { CopyAdvertsService } from "@components/shared/services/copyadverts.service";
import { ModifyAdvertsService } from "@components/shared/services/modify-adverts.service";
import { RepeatService } from "@components/shared/services/repeat.service";
import { FastEditAdvComponent } from "../fast-edit-adv/fast-edit-adv.component";
import { MatDialog } from "@angular/material";
import { NotifyService } from "@components/shared/services/notify.service";
import { StateService } from "@components/shared/services/state.service";
import { Location } from "@angular/common";
import { SharedService } from "@components/shared/services/shared.service";
import { MoveAdvertsService } from "@components/shared/services/moveadverts.service";
import { ProductService } from "src/app/services/product.service";
import { Enums } from "@components/shared/models/enums";
import { SiteSettingsService } from "@components/shared/services/sitesettings.service";
import { Title } from "@angular/platform-browser";
import { UserInfo } from "src/app/interfaces/user-info";
import { userInfo } from "os";
import { DollService } from "@components/shared/services/doll.service";
import { EditCommodityComponent } from "@components/commodity/edit-commodity/edit-commodity.component";
import { SpinnerService } from "@components/shared/services/spinner.service";

@Component({
    selector: "app-products-page",
    templateUrl: "./products-page.component.html",
    styleUrls: ["./products-page.component.scss"]
})
export class ProductsPageComponent implements OnInit, OnDestroy {
    @Input() isCabinetPage: boolean = false;
    @Input() set mode(type: Enums.CatalogMode) {
        this.setMode(type);
    }
    public selectedMode: Enums.CatalogMode;
    public selectedModes = Enums.CatalogMode;
    public isSearch: boolean = false;
    public Gallery = [];
    public apiRoot: any = null;
    public showSpinner = true;
    public categories: CategoryItem[] = [];
    public message: Message;
    public sub: Subscription;
    public $uInfoSubscr: Subscription;
    public clickCount: any = null;
    public clickSum: any = null;
    public cid: string = null;
    public custID: string;
    public ctlg_No: string;
    public ctlg_Name: string;
    public sup_ID: string;
    public prc_ID: string;
    public price: number;
    public prc_Br: any = null;
    public tName: string = null;
    public kolItems = null;
    public isEditing: boolean;
    public fastModyfyType = Enums.FastModify;
    public paginateConfig = this.storage.paginateConfig;
    public showDetail: boolean = false;
    public advId = null;
    public catId = null;
    public isEdittable: boolean = false;
    public showDate = false;
    public idPortal = null;

    constructor(
        private mainService: MainService,
        private route: ActivatedRoute,
        public router: Router,
        private notify: NotifyService,
        private storageService: StorageService,
        public authService: AuthService,
        public env: EnvService,
        private location: Location,
        public categoriesService: CategoriesService,
        public orderService: OrderService,
        public modifyAdverts: ModifyAdvertsService,
        public storage: StorageService,
        public local: DictionaryService,
        public copyAdverts: CopyAdvertsService,
        public repeatService: RepeatService,
        public searchData: SearchDataService,
        public state: StateService,
        public dialog: MatDialog,
        public shared: SharedService,
        public product: ProductService,
        public moveAdvers: MoveAdvertsService,
        public dollService: DollService,
        public siteSettings: SiteSettingsService,
        private titleService: Title
    ) {
        this.apiRoot = this.env.apiUrl;
        this.orderService.onClickSum.subscribe((cnt) => (this.clickSum = cnt));
        this.orderService.onClick.subscribe((cnt) => (this.clickCount = cnt));
        this.showDate = env.shopDetails.isShowGDate;
        this.$uInfoSubscr = this.authService.userInfo.asObservable().subscribe((uData: UserInfo) => {
            if (uData !== null) this.isEditing = uData.isEditing;
        });
        route.queryParams.subscribe((r) => {
            if (r && r.hasOwnProperty("redirect")) {
                setTimeout(() => {
                    this.router.navigate([`/cabinet/categories/${r.redirect}/products`]).then(() => {
                        window.location.reload();
                    });
                }, 2000);
            }
        });
    }

    private setMode(type: Enums.CatalogMode) {
        this.selectedMode = type;
        switch (type) {
            case Enums.CatalogMode.SEARCH: {
                this.isSearch = true;
                break;
            }
        }
    }

    ngOnInit() {
        this.showSpinner = true;
        this.titleService.setTitle(this.env.shopInfo.shortDescr);
        this.message = new Message("danger", "");
        this.cid = this.isCabinet() ? this.authService.getUserId() : "";
        this.mainService.getShopInfo().subscribe(
            (res) => {
                this.custID = res.cust_id;
                this.showSpinner = false;
                this.isAdmin = this.authService.getUserId() == res.cust_id;
                this.request(res.cust_id);
            },
            (error) => {
                alert(error.error.message);
                this.showSpinner = false;
                this.mainService.getErrorFonPicture();
            }
        );
        this.state.getShopInfo().then((res: any) => {
            this.isEdittable = res.isEditing;
        });
    }

    goToMainSearch() {
        this.router.navigate(["/"]);
    }

    canModifyAdvert(element) {
        const isAdvertOwner = element.creater_ID == this.authService.getUserId();
        return this.isEdittable && isAdvertOwner && this.isCabinet;
    }

    request(idPortal) {
        this.idPortal = idPortal;
        this.getGallery();
        this.mainService.getFonPictures();
        this.getCategories();
    }

    getCategories() {
        if (this.isCabinet()) {
            this.categoriesService.fetchCategoriesUser(this.idPortal).then((res) => this.getBreadcrumbs(res));
        } else {
            this.categoriesService.fetchCategoriesAll(this.idPortal).then((res) => this.getBreadcrumbs(res));
        }
    }

    replacePrice(price: any) {
        const valute = this.local.getTranslate("rub", "руб");
        return price.replace("руб", valute);
    }

    fastEdit(product, editType: Enums.FastModify) {
        const dialogRef = this.dialog.open(FastEditAdvComponent, {
            width: "400px",
            data: {
                ...product,
                editType: editType
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.notify.showNotify(
                    this.local.getTranslate("adv_updating", `Обьявление поставлено в очередь на обновление`),
                    "",
                    "warning"
                );
                product.tName = result.TName;
                // if (result.mediaLink && result.type == 2) {
                //     product.mediaLink = result.mediaLink.get("file");
                // }
                if (result.mediaLink == null || result.mediaLink === 0) {
                    product.mediaLink = false;
                }

                //   this.product.setGallery(product);
            }
        });
    }

    getBreadcrumbs(cats) {
        if (cats.length != 0) {
            // this.categoriesService.getBreadcrumbs(cats, this.route.snapshot.params["id"]).then((breadcrumbs: any) => {
            //     console.log(breadcrumbs)
            //     this.categories = breadcrumbs;
            //     this.storageService.selectedCategories.next(this.categories);
            // });
            const breadcrumbs = this.categoriesService.getBreadcrumbsV2 (cats, this.route.snapshot.params["id"],true);
            this.categories = breadcrumbs;
            this.storageService.selectedCategories.next(this.categories);
        }
    }

    ngAfterViewInit() {
        if (document.getElementsByTagName("header")[0]) {
            const action = () => {
                const header = document.getElementsByTagName("header")[0];
                const height = header && header.clientHeight !== undefined ? header.clientHeight : 0;
                if (document.getElementById("main-content"))
                    document.getElementById("main-content").style.paddingTop = `${height + 10}px`;
            };
            window.onresize = action;
            action();
        }
    }

    public convDate(date) {
        if (date) {
            const y = date.toString().substring(0, 4);
            const m = date.toString().substring(4, 6);
            const d = date.toString().substring(6, 8);
            return d + "." + m + "." + y;
        }
    }

    public isCabinet(): boolean {
        return window.location.pathname.includes("cabinet");
    }

    private showMessage(text: string, type: string = "danger") {
        this.message = new Message(type, text);
        window.setTimeout(() => {
            this.message.text = "";
        }, 2000);
    }

    closeDetail() {
        document.getElementById("closer-detail").classList.add("conditionalClose");
        this.location.replaceState(`${this.isCabinet() ? "/cabinet" : ""}/categories/${this.catId}/products`);
        setTimeout(() => {
            document.getElementById("closer-detail").classList.remove("conditionalClose");
            this.showDetail = false;
        }, 300);
    }

    detail(el) {
        this.advId = el.prc_ID;
        this.catId = el.id;
        this.showDetail = true;
    }

    breadcrumbsClick(event?) {
        this.storageService.setCategories(this.categories);
        this.storageService.breadcrumbFlag = true;
        this.router.navigate([this.router.url.includes("cabinet") ? "/cabinet" : "/"]);
    }

    async loadEditComponent(productId) {
        //    this.showPlayer = false;
        this.showSpinner = true;
        this.mainService
            .getProductWithAudio(
                productId,
                this.isCabinet ? this.authService.getUserId() : this.custID,
                this.custID,
                this.authService.getUserId()
            )
            .subscribe(
                (item) => {
                    this.showSpinner = false;
                    this.dialog
                        .open(EditCommodityComponent, {
                            width: "100%",
                            height: "100vh",
                            data: {
                                advert: item,
                                prc_ID: this.editPrcId,
                                isCopy: true
                            }
                        })
                        .afterClosed()
                        .subscribe((isUpdated) => {
                            this.showCopyComponent = false;
                            if (isUpdated) {
                                this.ngOnInit();
                            }
                        });
                },
                () => (this.showSpinner = false)
            );
    }

    public showCopyComponent: boolean = false;
    editPrcId: any = null;
    copy(el) {
        this.catId;
        if (el.id && el.prc_ID) {
            this.editPrcId = el.prc_ID;
        } else {
            this.editPrcId = this.advId;
        }
        this.showCopyComponent = true;
        this.loadEditComponent(el.prc_ID);

        // this.router.navigate(["/addProduct"], {
        //     queryParams: {
        //         copy: el.prc_ID,
        //         prev: JSON.stringify(this.router.routerState.snapshot.url)
        //     }
        // });
    }

    titleLimiter = (text: string) => (text && text.length > 40 ? text.substring(0, 34) + "..." : text);

    isAdmin: boolean = false;
    addToCart(el) {
        this.showSpinner = true;
        this.clickCount = this.orderService.countKol();
        this.clickSum = this.orderService.countSum();
        const registerOrder = this.orderService.countKol() === 0 ? this.mainService.registerOrder() : of({});
        registerOrder.subscribe(() => {
            this.mainService.getShopInfo().subscribe(
                (res) => {
                    this.custID = res.cust_id;
                    this.mainService.getProduct(el.prc_ID, this.custID, res.cust_id).subscribe((productRes: any) => {
                        this.ctlg_No = productRes.ctlg_No;
                        this.ctlg_Name = productRes.ctlg_Name;
                        this.sup_ID = productRes.sup_ID;
                        this.tName = productRes.tName;
                        this.prc_Br = productRes.prc_Br;
                        const image_Item = productRes.t_imageprev;
                        if (window.localStorage.getItem("kolItems")) {
                            this.kolItems = window.localStorage.getItem("kolItems");
                        } else {
                            this.kolItems = 1;
                        }
                        const data = {
                            OrdTtl_Id: this.orderService.getOrderId(),
                            OI_No: this.orderService.countKol() + 1,
                            Ctlg_No: productRes.ctlg_No,
                            Qty: 1,
                            Ctlg_Name: productRes.ctlg_Name,
                            Sup_ID: productRes.sup_ID,
                            Descr: productRes.tName
                        };
                        this.mainService.addToCart(data).subscribe((res: any) => {
                            this.showSpinner = false;
                            if (res.result == true) {
                                this.orderService.addToOrder(
                                    el,
                                    productRes.ctlg_No,
                                    productRes.ctlg_Name,
                                    productRes.sup_ID,
                                    image_Item
                                );
                                this.showMessage(`${res.message}`, "success");
                            }
                        });
                    });
                },
                (error) => alert(error.error.message)
            );
        });
    }

    navigateToCategory(items: CategoryItem[]) {
        const item = items[items.length - 1];
        const cabinet = this.isCabinet() ? "/cabinet" : "";
        if (item) {
            if (this.router.url == `${cabinet}/categories/${item.id}/products`) {
                this.showSpinner = true;
                this.request(this.idPortal);
            } else {
                this.router.navigate([`${cabinet}/categories/${item.id}/products`]).then((succeeded) => {
                    if (succeeded) {
                        this.showSpinner = true;
                        this.request(this.idPortal);
                    }
                });
            }
        }
    }

    clearItems(i: number) {
        this.product.clear();
    }

    moveToUp(): void {
        window.scrollTo(0, 0);
    }

    getGallery() {
        if (!this.isSearch) {
            this.showSpinner = true;
            const request = [Enums.CatalogMode.REPEAT, Enums.CatalogMode.STUDY, Enums.CatalogMode.DICTIONARY].includes(
                this.selectedMode
            )
                ? this.mainService.getMaterial(this.selectedMode)
                : this.mainService.getProducts(
                      this.route.snapshot.params["id"],
                      this.mainService.getCustId(),
                      this.cid
                  );
            request.subscribe((res) => {
                this.product.setGallery(res);
                this.showSpinner = false;
            });
        } else {
            this.product.setGallery(this.searchData.searchedData);
            //  this.Gallery = this.searchData.searchedData;
            this.showSpinner = false;
        }
    }

    deleteProduct(product) {
        const { prc_ID, appCode, mediaLink } = product;
        this.showSpinner = true;
        this.mainService
            .deleteProduct({
                cid: this.authService.getUserId(),
                appCode,
                cust_ID: this.custID,
                prc_ID
            })
            .subscribe((res) => {
                this.showSpinner = false;
                this.getCategories();
                if (res) {
                    this.product.deleteItems(this.product.gallery.indexOf(product), 1);
                    if (res && this.product.gallery.length == 0) this.breadcrumbsClick();
                    // if (mediaLink.includes("cloudfront"))
                    //     this.mainService.deleteCloudContent(mediaLink).subscribe((res) => {});
                }
            });
    }

    checkPrice(price) {
        return price == "" ? false : Number.parseInt(price) !== 0;
    }

    ngOnDestroy() {
        this.paginateConfig.currentPage = 1;
        if (this.sub) {
            this.sub.unsubscribe();
        }
        if (this.$uInfoSubscr) {
            this.$uInfoSubscr.unsubscribe();
        }
    }
}
