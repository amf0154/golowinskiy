import { Component, OnDestroy, OnInit, HostListener } from "@angular/core";
import { animate, state, style, transition, trigger } from "@angular/animations";
import { Subscription } from "rxjs";
import { MainService } from "../shared/services/main.service";
import { ClockService } from "../shared/services/clock.service";
import { Router } from "@angular/router";
import { CategoryItem } from "../categories/categories.component";
import { StorageService } from "../shared/services/storage.service";
import { AuthService } from "../shared/services/auth.service";
import { CategoriesService } from "../shared/services/categories.service";
import { DictionaryService } from "../shared/services/dictionary.service";
import { EnvService } from "src/app/env.service";
import { Title } from "@angular/platform-browser";
import { SharedService } from "@components/shared/services/shared.service";

@Component({
    selector: "main-page.component",
    templateUrl: "./main-page.component.html",
    styleUrls: ["./main-page.component.scss"],
    animations: [
        trigger("hideClock", [
            state(
                "start",
                style({
                    opacity: "1"
                })
            ),
            state(
                "end",
                style({
                    opacity: "0",
                    display: "none"
                })
            ),
            transition("start => end", animate("1000ms 0.5s ease-out"))
        ])
    ]
})
export class MainPageComponent implements OnInit, OnDestroy {
    public initialCategories: CategoryItem[] = [];
    public authSubscriber: Subscription;
    public clickClock = "start";

    constructor(
        public router: Router,
        public mainService: MainService,
        private clockService: ClockService,
        public storageService: StorageService,
        private authService: AuthService,
        public shared: SharedService,
        public categoriesService: CategoriesService,
        private env: EnvService,
        public local: DictionaryService,
        private titleService: Title
    ) {}

    ngOnDestroy(): void {
        if(this.authSubscriber)
            this.authSubscriber.unsubscribe();
    }

    /*
  @HostListener('document:scroll', ['$event']) 
    onWindowScroll(event){
  //   console.log(event)
  }
  */

    ngOnInit() {
        this.titleService.setTitle(this.env.shopInfo.shortDescr);
        this.authSubscriber = this.authService.isAuthenticatedObservable().subscribe((r) => {
            if (r && !this.shared.showMainButton()) {
                this.router.navigate(["/cabinet"]);
            }
        });
        this.mainService.getShopInfo().subscribe((res) => {
            this.mainService.getFonPictures();
            if (this.isCabinet()) {
                this.categoriesService.fetchCategoriesUser(res.cust_id);
            } else {
                this.categoriesService.fetchCategoriesAll(res.cust_id);
            }
        });

        this.initialCategories = this.storageService.breadcrumbFlag ? this.storageService.getCategories() : [];
        this.storageService.breadcrumbFlag = false;
    }

    ngAfterViewInit() {
        const innerElement = document.getElementById("inner");
        setTimeout(() => window.scrollTo(0, 0), 500);
        let action = () => {
            let header = document.getElementsByTagName("header")[0]
                ? document.getElementsByTagName("header")[0]
                : { clientHeight: 0 };
            const height = header.clientHeight;
            if (innerElement) innerElement.style.paddingTop = `${height + 20}px`;
        };
        window.onresize = action;
        action();

        if (document.getElementById("doc_time")) {
            this.clockService.getClock();
            if (innerElement)
                innerElement.style.minHeight = `calc(100vh - ${document.getElementById("doc_time").offsetHeight}px - ${
                    document.getElementById("footer").offsetHeight
                }px - 65px)`;
            window.setTimeout(() => {
                this.clickClock = "end";
            }, 3000);
        }
    }

    isCabinet(): boolean {
        return window.location.pathname.includes("cabinet") || window.location.pathname.includes("catcabinet");
    }

    onCategoriesClick(items: CategoryItem[]) {
        this.storageService.setCategories(items);
        const item = items[items.length - 1];
        if (window.location.pathname == "/catcabinet") {
            this.router.navigate([`/cabinet/categories/${item.id}/products`]);
        } else {
            this.router.navigate([`${window.location.pathname}/categories/${item.id}/products`]);
        }
    }
}
