import { Component, OnInit } from '@angular/core'
import { MainService } from '@components/shared/services/main.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public version = null;
  constructor(
    public mainService: MainService,
  ) { }

  ngOnInit() {
    this.version = environment.version;
  }

}
