import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Message } from "@components/shared/models/message.model";
import { AuthService } from "../../../shared/services/auth.service";
import { MainService } from "@components/shared/services/main.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { StateService } from "@components/shared/services/state.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { of, Subject } from "rxjs";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { Enums } from "@components/shared/models/enums";
import { takeUntil, tap } from "rxjs/operators";
import { SiteSettingsService } from "@components/shared/services/sitesettings.service";
const Swal = require("sweetalert2");

@Component({
    selector: "app-editpersonal",
    templateUrl: "./edit-personal.component.html",
    styleUrls: ["./edit-personal.component.scss"]
})
export class EditPersonalComponent implements OnInit, OnDestroy {
    public message: Message = new Message("danger", "");
    public showSpinner = false;
    public form: FormGroup = null;
    public $componentDestroyed = new Subject();
    public showForm: boolean = false;
    public siteSort = [
        { title: this.local.getTranslate("sort-not", "сортировка не выбрана"), value: null },
        { title: this.local.getTranslate("sort-price", "по цене"), value: 0 },
        { title: this.local.getTranslate("sort-date", "по дате размещения - новые вверху"), value: 3 },
        { title: this.local.getTranslate("sort-importer", "по поставщику"), value: 4 },
        { title: this.local.getTranslate("sort-name", "по наименованию"), value: 5 },
        { title: this.local.getTranslate("sort-letter", "по кол-ву букв в слове"), value: 8 },
        { title: this.local.getTranslate("sort-vocabulary", "Словарь"), value: 9 }
    ];

    public isAudioType = this.siteSettings.getAudioRepeats() ? this.siteSettings.getAudioRepeats() : false;

    public slideType = [
        { title: this.local.getTranslate("audio", "Аудио"), value: 1 },
        { title: this.local.getTranslate("video", "Видео"), value: 2 }
    ];

    // public siteDepartment = [
    //     { title: this.local.getTranslate('main','Главная'), value: Enums.SiteDepartment.MAIN },
    //     { title: this.local.getTranslate('personal_cab','ЛИЧНЫЙ КАБИНЕТ'), value: Enums.SiteDepartment.CABINET },
    //     { title: this.local.getTranslate('repeat','ПОВТОРИТЬ'), value: Enums.SiteDepartment.REPEAT },
    //     { title: this.local.getTranslate('learn','Выучить'), value: Enums.SiteDepartment.STUDY },
    // ];
    public repeats = [1, 2, 3, 4, 5, 7, 10];

    public modal_inner = { "min-height": [3, 4].includes(this.data.type) ? "50px" : "" };

    constructor(
        public authService: AuthService,
        public mainService: MainService,
        public local: DictionaryService,
        public siteSettings: SiteSettingsService,
        public spinner: SpinnerService,
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<EditPersonalComponent>,
        public route: ActivatedRoute,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public state: StateService
    ) {}

    ngOnInit() {
        this.getUserData();
        this.siteSettings.getDepartmentTypeByRoute(this.data.route);
    }

    manual() {
        const body = {
            cust_ID_Main: this.mainService.getCustId(),
            itemname: Enums.ManualType.SITESETTINGS
        };
        this.mainService.getManual(body).subscribe();
    }

    public showPause: number;
    public getUserData(load: boolean = false) {
        this.showSpinner = true;
        let isShuffleContent;
        const audioRepeats = this.siteSettings.getAudioRepeats()
            ? this.siteSettings.getAudioRepeats()
            : this.repeats[4];
        this.siteSettings
            .isShuffleContent()
            .pipe(takeUntil(this.$componentDestroyed))
            .subscribe((r) => {
                isShuffleContent = r;
            });
        const isSettingsFrozen = this.siteSettings.getFrozeSetting(
            this.siteSettings.getDepartmentTypeByRoute(this.data.route)
        );
        const sliderType = localStorage.getItem("slideType")
            ? Number(localStorage.getItem("slideType"))
            : this.slideType[0].value;
        const req =
            !load && this.data && this.data.fio
                ? of(this.data)
                : this.authService.getUserInfo(this.authService.getUserId());
        req.subscribe(
            (res: any) => {
                const {
                    address,
                    e_mail,
                    fio,
                    phone,
                    skype,
                    whatsapp,
                    isNoStartAudio,
                    isSlideshowAdvert,
                    isSlideshowExtraPict,
                    isSearchPageVariant,
                    showPause
                } = res;
                this.showPause = showPause;
                this.form = this.formBuilder.group({
                    Street: [address],
                    Email: [e_mail],
                    f: [fio],
                    Password: [""],
                    Phone: [phone],
                    Skype: [skype],
                    WhatsApp: [whatsapp],
                    isNoStartAudio: [{ value: !isNoStartAudio, disabled: isSettingsFrozen }],
                    isSearchPageVariant: [isSearchPageVariant],
                    isShuffleContent: [isShuffleContent],
                    isSlideshowAdvert: [{ value: isSlideshowAdvert, disabled: isSettingsFrozen }],
                    isSlideshowExtraPict: [{ value: isSlideshowExtraPict, disabled: isSettingsFrozen }],
                    slideType: [sliderType],
                    audioRepeats: [audioRepeats],
                    delRepeat: [],
                    isSettingsFrozen: [isSettingsFrozen],
                    isStudyPause: [this.showPause == 2]
                });
                this.isAudioType = sliderType == 1;
                this.form
                    .get("slideType")
                    .valueChanges.pipe(takeUntil(this.$componentDestroyed))
                    .subscribe((r) => {
                        this.isAudioType = r == 1;
                    });
                this.form
                    .get("isSettingsFrozen")
                    .valueChanges.pipe(takeUntil(this.$componentDestroyed))
                    .subscribe(() => this.freezeSettings());
                this.showForm = true;

                this.form
                    .get("isStudyPause")
                    .valueChanges.pipe(takeUntil(this.$componentDestroyed))
                    .subscribe((r) => {
                        const {value} = this.form.get("isStudyPause");
                            Swal.queue([
                                {
                                    title: "",
                                    confirmButtonText: this.local.getTranslate("yes", "Да"),
                                    text: !value ? this.local.getTranslate("pause-restore-study", "Вы действительно хотите возобновить обучение?") : this.local.getTranslate("pause-stop-study", "Вы действительно хотите прервать обучение?"),
                                    cancelButtonText: this.local.getTranslate("cancel",'Отмена'),
                                    showCancelButton: true,
                                    showLoaderOnConfirm: true,
                                    preConfirm: async () => {
                                        return this.updatePauseSettings(value)
                                            .toPromise()
                                            .then(() => {
                                                Swal.insertQueueStep({
                                                    icon: "success",
                                                    title: value ? this.local.getTranslate("pause-taken", "Пауза установлена") : this.local.getTranslate("pause-declined", "Пауза снята"),
                                                    showConfirmButton: false,
                                                    timer: 1000
                                                });
                                            })
                                            .catch(() => {
                                                Swal.insertQueueStep({
                                                    icon: "error",
                                                    title: this.local.getTranslate("error", "Ошибка установки"),
                                                    showConfirmButton: false,
                                                    timer: 1000
                                                });
                                            });
                                    }
                                }
                            ]).then((action) => {
                                if (action.dismiss === "cancel") {
                                    this.form
                                        .get("isStudyPause")
                                        .setValue(!this.form.get("isStudyPause").value, { emitEvent: false });
                                }
                            });
                    });
            },
            (err) => {},
            () => (this.showSpinner = false)
        );
    }

    public updatePauseSettings(value) {
        return this.mainService.setPause({
            cust_ID: this.authService.getUserId(),
            mark: value
        }).pipe(tap(()=>{this.authService.clearUserHistory()}))
    }

    public getValue(name: string) {
        return this.form.get(name);
    }

    private showMessage(type: string = "danger", text: string) {
        this.message = new Message(type, text);
        window.setTimeout(() => {
            this.message.text = null;
        }, 2000);
    }

    public onSubmit() {
        this.showSpinner = true;
        this.authService
            .updateProfile({
                ...this.form.getRawValue(),
                isNoStartAudio: !this.form.get("isNoStartAudio").value,
                Cust_ID: this.authService.getUserId(),
                Cust_ID_Main: this.mainService.getCustId()
            })
            .subscribe(
                () => {
                    // site settings start;
                    this.siteSettings.setAudioRepeats(this.form.get("audioRepeats").value);
                    this.siteSettings.isShuffleState(this.form.get("isShuffleContent").value);
                    localStorage.setItem("slideType", this.form.get("slideType").value);
                    this.siteSettings.updateConfig();
                    this.freezeSettings(true);
                    // site settings end;
                    this.showSpinner = false;
                    this.message.type = "success";
                    this.dialogRef.close();
                    this.authService.clearUserHistory();
                    this.getUserData(true);
                },
                (err) => {
                    this.showSpinner = false;
                    this.showMessage("danger", err.error.message);
                }
            );
    }

    private freezeSettings(apply = false): void {
        if (this.form.get("isSettingsFrozen").value) {
            this.form.get("isSlideshowAdvert").disable();
            this.form.get("isSlideshowExtraPict").disable();
            this.form.get("isNoStartAudio").disable();
            apply
                ? this.siteSettings.setFrozenSettings(true, this.siteSettings.getDepartmentTypeByRoute(this.data.route))
                : null;
        } else {
            this.form.get("isSlideshowAdvert").enable();
            this.form.get("isSlideshowExtraPict").enable();
            this.form.get("isNoStartAudio").enable();
            apply
                ? this.siteSettings.setFrozenSettings(
                      false,
                      this.siteSettings.getDepartmentTypeByRoute(this.data.route)
                  )
                : null;
        }
    }

    ngOnDestroy(): void {
        this.$componentDestroyed.next();
        this.$componentDestroyed.complete();
    }
}
