import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Message } from '@components/shared/models/message.model'
import { AuthService } from '../../../shared/services/auth.service'
import { MainService } from '@components/shared/services/main.service'
import { MatDialog, MatDialogRef } from '@angular/material'
import { RecoveryComponent } from '../recovery/recovery.component'
import { NotifyService } from '@components/shared/services/notify.service'


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  public form: FormGroup
  public message: Message
  public sub: Subscription
  public params;
  public shopId: string = null;  
  public showSpinner = false
  public agreeCondition: boolean = false;

  constructor(
    public authService: AuthService,
    public mainService: MainService,
    private notify: NotifyService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<RegistrationComponent>,
    public route: ActivatedRoute
    ) {
      this.mainService.getShopInfo().subscribe((res) => {
        this.shopId = res.cust_id;
      }, error=>alert(error.error.message));
     }

  ngOnInit() {
    this.form = new FormGroup({
      f: new FormControl(null),
      e_mail: new FormControl(null,[Validators.email]),     
      Phone1: new FormControl(null),
      Mobile: new FormControl(null),
      whatsApp: new FormControl(null),
      skype: new FormControl(null),
      Address: new FormControl(null),
      password: new FormControl(null, [Validators.required])     
    }); 

    this.message = new Message('danger', '')
    this.route.queryParams.subscribe((params: Params) => {
      this.params = params
    })
  }

  private showMessage(type: string = 'danger', text: string){
    this.message = new Message(type, text)
    window.setTimeout(() => {
      if(this.message.type == 'success'){
        this.message.text = ''
      //  this.router.navigate([`/auth/login`])
      }
    }, 2000);
  }

  ngOnDestroy(){
    if(this.sub){
      this.sub.unsubscribe()
    }    
  }

  onSubmit(){

    this.showSpinner = true

    this.form.disable()

    this.sub = this.authService.registration(this.shopId,this.form.value).subscribe(
      (res: any) => {
        this.showSpinner = false;
        if(res.result == false){
          this.showMessage('danger', res.txt);
        }
        else if(res.result == true){
          this.notify.showNotify(res.txt);
          this.dialogRef.close(true); 
          this.showMessage('success', res.message);          
        }
        
      },
      (error) => {    
        if(error.status == 500){
          this.showMessage('danger', 'Ошибка регистрации, попробуйте позже!')
        }else{
          this.showMessage('danger', error.error.message)
          this.form.enable();
        } 
        this.showSpinner = false
        this.form.enable();
      }
    )
  }

  public recovery(){
    this.dialogRef.close();
    this.dialog.open(RecoveryComponent, {
      width: "500px",
    });
  };
}
