import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Message } from "@components/shared/models/message.model";
import { AuthService } from "../../../shared/services/auth.service";
import { MainService } from "@components/shared/services/main.service";
import { CloseService } from "@components/shared/services/close.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { ShopInfo } from "@components/shared/interfaces";
import { StateService } from "@components/shared/services/state.service";
import { CategoriesService } from "@components/shared/services/categories.service";
import { MatDialog, MatDialogRef } from "@angular/material";
import { RegistrationComponent } from "../registration/registration.component";
import { RecoveryComponent } from "../recovery/recovery.component";
const Swal = require("sweetalert2");
@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit, OnDestroy {
    form: FormGroup;
    message: Message;
    sub: Subscription;
    params;
    showSpinner = false;
    shopId: string = null;
    constructor(
        public authService: AuthService,
        public router: Router,
        public mainService: MainService,
        public closeService: CloseService,
        public local: DictionaryService,
        private categoriesService: CategoriesService,
        public dialogRef: MatDialogRef<LoginComponent>,
        private state: StateService,
        public dialog: MatDialog,
        public route: ActivatedRoute
    ) {
        this.mainService.getShopInfo().subscribe(
            (res) => {
                this.shopId = res.cust_id;
            },
            (error) => console.error(error.error.message)
        );
    }

    ngOnInit() {
        this.form = new FormGroup({
            userName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
            password: new FormControl(null, [Validators.required, Validators.minLength(3)])
        });
        this.message = new Message("danger", "");
        this.route.queryParams.subscribe((params: Params) => {
            this.params = params;
        });
    }

    private showMessage(type: string = "danger", text: string, redirect: boolean = true) {
        this.message = new Message(type, text);
        window.setTimeout(() => {
            this.message.text = "";
            if (redirect) {
                this.closeService.clickCloseModal();
                //  this.router.navigate([`/${this.params.route}`])
            }
        }, 2000);
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    onSubmit() {
        this.showSpinner = true;
        this.form.disable();
        this.authService.login(this.shopId, this.form.value).subscribe(
            (res) => {
                this.showSpinner = false;
                this.state.getShopInfo().then(({ role, cust_id }: ShopInfo) => {
                    this.categoriesService.fetchCategoriesAll(cust_id);
                    if (role == "admin") {
                        this.showMessage(
                            "success",
                            this.local.getTranslate("login_as_admin", "Вы вошли в систему как администратор")
                        );
                    } else {
                        this.showMessage(
                            "success",
                            this.local.getTranslate("login_as_user", "Вы вошли в систему как пользователь")
                        );
                    }
                    this.dialogRef.close(true);
                });

                // if (res) {
                //     this.authService.clearUserHistory();
                //     this.authService.getUserInfo().subscribe();
                // }
            },
            (error) => {
                if (error && error.error && error.error.message) {
                    this.alertError(error.error.message);
                } else {
                    switch (error.status) {
                        case 500: {
                            this.alertError("Внутренняя ошибка сервера, попробуйте авторизоваться позже!");
                            break;
                        }
                        case 401: {
                            this.alertError("Ошибка, " + error.error);
                            break;
                        }
                    }
                }
                this.showSpinner = false;
                this.form.enable();
                this.form.reset();
            }
        );
    }

    alertError(message) {
        let timerInterval;
        Swal.fire({
            icon: "error",
            title: message,
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {
                clearInterval(timerInterval);
            }
        });
    }

    public recovery() {
        this.dialogRef.close();
        this.dialog.open(RecoveryComponent, {
            width: "500px"
        });
        //  this.router.navigate([`/auth/recovery`])
    }
    public register() {
        this.dialogRef.close();
        this.dialog.open(RegistrationComponent, {
            width: "500px"
        });
        // .afterClosed().subscribe((r)=>{
        //   if(r){
        //     setTimeout(()=> this.router.navigate(["/home"]), 1000);
        //   };
        // });

        //  this.router.navigate([`/auth/registration`])
    }
}
