import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastEditAdvComponent } from './fast-edit-adv.component';

describe('FastEditAdvComponent', () => {
  let component: FastEditAdvComponent;
  let fixture: ComponentFixture<FastEditAdvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastEditAdvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastEditAdvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
