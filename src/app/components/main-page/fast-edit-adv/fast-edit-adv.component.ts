import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Enums } from '@components/shared/models/enums';
import { AuthService } from '@components/shared/services/auth.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';
import { ModifyAdvertsService } from '@components/shared/services/modify-adverts.service';

@Component({
  selector: 'app-fast-edit-adv',
  templateUrl: './fast-edit-adv.component.html',
  styleUrls: ['./fast-edit-adv.component.scss']
})
export class FastEditAdvComponent implements OnInit{
  public submitted: boolean = false;
  public showSpinner = false;
  public dataLoaded = false;
  public editedAudioFile: any = 0;
  public audio: any = null;
  public mediaLink: any = '';
  public editType = Enums.FastModify.DISABLED;
  advertForm = this.fb.group({
    tName : [
      this.data.tName.trimRight(), 
      [
        Validators.required, 
        this.emptySpacesValidator, 
        Validators.minLength(1)
      ]
    ]
  });

  constructor(
    public dialogRef: MatDialogRef<FastEditAdvComponent>,
    public mainService: MainService,
    public fb: FormBuilder,
    public local: DictionaryService,
    private authService: AuthService,
    public modifyAdverts: ModifyAdvertsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editType = data && data.editType ? data.editType : this.modifyAdverts.fastEditKey;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){

  }

  public editedAudio(data){
    if(data){
      this.editedAudioFile = new FormData();
      const newFileName = this.uuidv4();
      let file;
      if(data.name){
        file = new File([data], "attached_"+ newFileName + '.mp3', {type: data.type});
      }else{
        file = new File([data], "voiceRecord_"+ newFileName + ".mp3")
      }
      this.editedAudioFile.append('file',file);
    }else{
      this.mediaLink = null;
      this.editedAudioFile = null;
    }

  }

  get f() {
    return this.advertForm.controls;
  }

  private emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value.trim().length === 0) {
      return { emptySpaces: this.local.getTranslate("input_correct_title",`Введите корректное название`)};
    }
    return null;
  }

  private uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
   
  onNoClick(): void {
    this.dialogRef.close();
  }

  saveData() {
    if(!this.advertForm.valid && this.editType == 1){
      return false
    }else{
      this.modifyAdverts.saveFastModify(
        this.data,
        this.advertForm.get('tName').value,
        this.editedAudioFile,
      );
      this.dialogRef.close(
        {
          TName: this.advertForm.get('tName').value,
          mediaLink: this.editedAudioFile,
          type: this.editType
        }
      );
    }
  }

  }

  
