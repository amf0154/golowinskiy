import {Component, ElementRef, OnInit, ViewChild, OnDestroy, Injectable} from '@angular/core';
import {AuthService} from '@components/shared/services/auth.service';
import {MainService} from '../shared/services/main.service';
import {CategoryItem, CategoriesComponent} from '../categories/categories.component';
import {StorageService} from '../shared/services/storage.service';
import {CategoriesService} from '../shared/services/categories.service';
import { CommonService } from '../shared/services/common.service';
import { EnvService } from 'src/app/env.service';
import { DictionaryService } from '../shared/services/dictionary.service';
import { ActivatedRoute } from '@angular/router';
import { CopyAdvertsService } from '../shared/services/copyadverts.service';
import { NgbDate, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
const Swal = require('sweetalert2');
import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { SharedService } from '@components/shared/services/shared.service';
import { Title } from '@angular/platform-browser';

const I18N_VALUES = {
  'ru': {
    weekdays: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Февраль', 'Ноябрь', 'Декабрь'],
    weekLabel: 'sem'
  }
  // other languages you would support
};

@Injectable()
export class I18n {
  language = 'ru';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }
  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}

@Component({
  selector: 'app-subscribe-page',
  templateUrl: './subscribe-page.component.html',
  styleUrls: ['./subscribe-page.component.scss'],
  providers:
      [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class SubscribePageComponent implements OnInit{
  public dateModel: any = "2021-04-11T15:04:54.302Z";
  public userId = null;
  public cust_id = null;
  public showSpinner = true
  public idCategory= null;
  public itemName = ''
  public categories: CategoryItem[] = []
  public fio = null;
  public userName = null;
  public phone = null;
  public showHideCalendarTimeRange: boolean = false;
  public isCanPromo: boolean = false;
  public loadingImage: string = null;
  public loadingMini: string = null;
  public loadingSpinner: string = null;
  public copyModeCatalog: boolean = false;
  public copyMode: boolean = false;
  public subscribeMode: boolean = false;
  public initialCategories: CategoryItem[] = []
  public advertId: string = "2";
  public copy_cust_id: any = null;
  public calendarModel: NgbDateStruct;
  public calendarTimeModel: NgbTimeStruct = {hour: 12, minute: 0, second: 0};
  public showCopyButton: boolean = false;
  public previousCategoryBreadcrumbs = null;
  public useRotate: boolean = false;
  constructor(
    private authService: AuthService,
    private mainService: MainService,
    public storageService: StorageService,
    public categoriesService: CategoriesService,
    public commonStore : CommonService,
    public route: ActivatedRoute,
    public env: EnvService,
    public shared: SharedService,
    public local: DictionaryService,
    private title: Title,
    private _location: Location,
    public copyAdverts : CopyAdvertsService
  ) { 
    this.loadingMini = this.loadingImage = this.commonStore.loadingLittleRedSpinner;
    this.loadingSpinner = this.commonStore.loadingImageSpinner;
    this.storageService.setCategories([]);
    this.copyAdverts.showFillCheckbox = true;
    this.copyAdverts.showDefSheckbox = true;
    this.categoriesService.mainCategories$.subscribe(res=>{
      this.showCopyButton = true; //res.length === 0 ? false : true;
    })
    switch(this.route.snapshot.routeConfig.path){
      case 'subscribe': {
        this.subscribeMode = true;
        this.title.setTitle(local.getTranslate("subscription","Подписка на рассылку"));
        break;
      };
      case 'copy': {
        this.copyMode = true;
        this.title.setTitle(local.getTranslate("copy_list","Копировать Списком"));
        this.copyAdverts.showFillCheckbox = false;
        break;
      }
      case 'copy-catalog': {
        this.title.setTitle(local.getTranslate('copy_cat','Копирование каталога'));
        this.copyModeCatalog = true;
        break;
      }
    }
  }

  $selectAllCats: boolean = false
  selectAllCats(){
    this.copyAdverts.selectAllCats(!this.$selectAllCats)
  }

  removeAllSubscribes(){
    this.mainService.addSubscription({
      AppCode: this.env.shopInfo.cust_id,
      CID: this.authService.getUserId(),
      ID: null,
      dellAll: true,
      Mark: false
    }).subscribe(res=>{
      this.mainService.clearCategoriesHistory();
      this.storageService.setCategories([]);
      this.categoriesService.fetchCategoriesAll(this.cust_id,this.userId, (this.copyMode || this.copyModeCatalog) ? "4" :this.advertId);
    });
  }

  $selectAllContain: boolean = false
  selectAllContain(){
    this.copyAdverts.selectAllCatsFill(!this.$selectAllContain)
  }

  ngOnInit() {
    this.fio = localStorage.getItem('fio')
    this.userName = false //localStorage.getItem('userName')
    this.phone = false //localStorage.getItem('phone')
    this.mainService.getShopInfo().subscribe(
      (res) => {
        this.cust_id = res.cust_id;
        this.copy_cust_id = this.cust_id;
        this.userId = this.authService.getUserId()
        this.showSpinner = false;
        this.categoriesService.fetchCategoriesAll(this.cust_id,this.userId, (this.copyMode || this.copyModeCatalog) ? "4" :this.advertId);
      }, error=>alert(error.error.message)
    )
    this.initialCategories = this.storageService.getCategories();
    this.storageService.breadcrumbFlag = false;
    this.authService.getUserInfo(this.authService.getUserId()).subscribe((res: any) => {
      this.isCanPromo = res.isCanPromo;
    });
    setTimeout(()=> window.scrollTo(0,0) ,300); 
  }

  ngAfterContentInit() {
    document.body.style.backgroundColor = "#ffffff";
  }

  $showHideCalendarTimeRange(){
    this.showHideCalendarTimeRange = !this.showHideCalendarTimeRange
  }

  updCat(){
    this.categoriesService.fetchCategoriesAll(this.copy_cust_id,this.env.defaultShopId != this.copy_cust_id.trim() ? this.copy_cust_id.trim() : this.userId, (this.copyMode || this.copyModeCatalog) ? "4" :this.advertId)
    this.copyAdverts.setNewPortalId(this.copy_cust_id.trim());
  }

  manual(){
    this.shared.manual(this.route.snapshot.routeConfig.path);
  }


  showCopyAdverts: boolean = false;
  controlCopyAdverts(){
    this.showCopyAdverts = !this.showCopyAdverts;


  }
  public newShopIdButton = false;
  public copy_cust_id_alias = null;
  reloadCatalog(){
    this.env.shopInfo.cust_id
    if(this.copy_cust_id.trim().length != 0){
      this.newShopIdButton = true;
      this.mainService.getShopIdByName(this.copy_cust_id).subscribe(res=>{
        this.newShopIdButton = false;
        this.copy_cust_id_alias = res.cust_id;
        if(this.copy_cust_id_alias !== this.cust_id){
          const formName = 'copyAuth2';
          Swal.fire({
            title: 'Авторизация',
            html:
              '<input id="swal-input1" placeholder="логин" class="swal2-input">' +
              '<input id="swal-input2" type="password" placeholder="пароль" class="swal2-input">' +
              '<input id="swal-input3" type="checkbox" placeholder="сохранить" class="swal3-input">',
            onOpen: () => {
              document.getElementById("swal-input1").focus()
              const form = this.shared.getCredentials(formName);
              if(form) {
                (document.getElementById("swal-input1") as any).value = form.login;
                (document.getElementById("swal-input2") as any).value = form.pwd;
                (document.getElementById("swal-input3") as any).checked = true;
              }
            },
            preConfirm: async () => {
              const {checked}: any = document.getElementById("swal-input3");
              checked ? this.shared.saveCredetials(formName,{
                login: (document.getElementById("swal-input1") as any).value,
                pwd: (document.getElementById("swal-input2") as any).value,
              }) : this.shared.saveCredetials(formName,{}, true);
              return new Promise((resolve,reject)=>{
                this.mainService.authAdmin(document.getElementById("swal-input1"),document.getElementById("swal-input2")).subscribe(res=>{
                  var that = this;
                  resolve({...res,that: that,unameElement: document.getElementById("swal-input1")});
                }, error=>{
                  resolve(Swal.insertQueueStep({
                    icon: 'error',
                    title: 'Ошибка авторизации!'
                  }));
                })
              })
            }
          }).then(function (result) {
            const {that,cust_ID_Main,role} = result.value
            // (role === "admin" && cust_ID_Main == that.copy_cust_id) || 
            if(role === "superadmin"){
              console.log('sup')
              that.copyAdverts.showFillCheckbox = false;
              that.copyAdverts.showDefSheckbox = false;
              that.categoriesService.fetchCategoriesAll(that.copy_cust_id,that.env.defaultShopId != that.copy_cust_id.trim() ? that.copy_cust_id.trim() : that.userId, (that.copyMode || that.copyModeCatalog) ? "4" :that.advertId)
              that.copyAdverts.setNewPortalId(that.copy_cust_id.trim());
            }else{
              Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Авторизация провалена!',
                showConfirmButton: false,
                timer: 2000
              });
            }
          })
        }else{
          this.copyAdverts.showFillCheckbox = true;
          this.copyAdverts.showDefSheckbox = true;
          this.categoriesService.fetchCategoriesAll(this.copy_cust_id_alias,this.env.defaultShopId != this.copy_cust_id_alias ? this.copy_cust_id_alias : this.userId, (this.copyMode || this.copyModeCatalog) ? "4" :this.advertId)
          this.copyAdverts.setNewPortalId(this.copy_cust_id_alias);
        }if(this.copy_cust_id_alias !== this.cust_id){
          const formName = 'copyAuth';
          Swal.fire({
            title: 'Авторизация',
            html:
              '<input id="swal-input1" placeholder="логин" class="swal2-input">' +
              '<input id="swal-input2" type="password" placeholder="пароль" class="swal2-input">' +
              '<p style="display: flex; align-items: center; margin-right: 7px;"><input id="swal-input3" style="transform: scale(1.5)" type="checkbox" placeholder="сохранить" class="swal3-input">&nbsp;&nbsp;запомнить данные </p>',
            onOpen: () => {
              document.getElementById("swal-input1").focus();
                const form = this.shared.getCredentials(formName);
                if(form) {
                  (document.getElementById("swal-input1") as any).value = form.login;
                  (document.getElementById("swal-input2") as any).value = form.pwd;
                  (document.getElementById("swal-input3") as any).checked = true;
                }
            },
            preConfirm: async () => {
              const {checked}: any = document.getElementById("swal-input3");
              checked ? this.shared.saveCredetials(formName,{
                login: (document.getElementById("swal-input1") as any).value,
                pwd: (document.getElementById("swal-input2") as any).value,
              }) : this.shared.saveCredetials(formName,{}, true);
              return new Promise((resolve,reject)=>{
                this.mainService.authAdmin(document.getElementById("swal-input1"),document.getElementById("swal-input2")).subscribe(res=>{
                  var that = this;
                  resolve({...res,that: that,unameElement: document.getElementById("swal-input1")});
                }, error=>{
                  resolve(Swal.insertQueueStep({
                    icon: 'error',
                    title: 'Ошибка авторизации!'
                  }));
                })
              })
            }
          }).then(function (result) {
            const {that,cust_ID_Main,role} = result.value
            // (role === "admin" && cust_ID_Main == that.copy_cust_id) || 
            if(role === "superadmin"){
              const theSameShop = that.env.defaultShopId == that.copy_cust_id_alias;
              that.copyAdverts.showFillCheckbox = that.copyMode ? false : theSameShop;
              that.copyAdverts.showDefSheckbox = that.copyMode || !theSameShop;
              that.categoriesService.fetchCategoriesAll(that.copy_cust_id_alias,that.env.defaultShopId != that.copy_cust_id_alias ? that.copy_cust_id_alias : that.userId, (that.copyMode || that.copyModeCatalog) ? "4" :that.advertId)
              that.copyAdverts.setNewPortalId(that.copy_cust_id_alias.trim());
            }else{
              Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Авторизация провалена!',
                showConfirmButton: false,
                timer: 2000
              });
            }
          })
        }else{
          this.copyAdverts.showFillCheckbox = true;
          this.copyAdverts.showDefSheckbox = true;
          this.categoriesService.fetchCategoriesAll(this.copy_cust_id_alias,this.env.defaultShopId != this.copy_cust_id_alias ? this.copy_cust_id_alias : this.userId, (this.copyMode || this.copyModeCatalog) ? "4" :this.advertId)
          this.copyAdverts.setNewPortalId(this.copy_cust_id_alias);
        }
      },err=>{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: err.error.message,
          showConfirmButton: false,
          timer: 2000
        });
      },() => this.newShopIdButton = false)
    }else{
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Введите идентификатор магазина!',
        showConfirmButton: false,
        timer: 2000
      });
    }
    
  }

  refreshCat: boolean = false;
  breadcrumbsClick(i) {
    this.storageService.setCategories(this.categories.slice(0,i))
    this.categories = []
    this.storageService.breadcrumbFlag = true
    this.initialCategories = this.storageService.getCategories();
    this.refreshCat = true;
    setTimeout(()=>this.refreshCat = false);
    this.idCategory = '';
    this.itemName = '';  
  }

  newDateTime(el){
    
  }

  public isCabinet(): boolean {
    return window.location.pathname.includes('cabinet')
  }
  onCategoriesClick(items: CategoryItem[]) {
    this.storageService.setCategories(items)
  }
  categorySelect(event){}
}
