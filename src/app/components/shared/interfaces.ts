export interface User {
    Cust_ID_Main: string;
    Mobile?: string;
    Phone1?: string;
    e_mail: string;
    f?: string;
    password: string;
}

export interface AdditionalImagesRequest {
    catalog: string;
    id: string;
    prc_ID: string;
    imageOrder: number;
    tImage: Blob | string;
    appcode: string;
    cid: string;
}

export interface AdditionalImagesData {
    request: AdditionalImagesRequest;
    imageData?: FormData;
}

export interface Product {
    prc_ID: number;
    cust_ID: number;
    appCode: string;
}

export interface DeleteProduct {
    prc_ID: number | string;
    cust_ID: number | string;
    appCode: string | string;
    cid: number | string;
}

export interface ShopInfo {
    cust_id: any;
    mainImage: String;
    mainPictureAccountUser: String;
    dz: string;
    email: String;
    phone: any;
    shortDescr: string;
    addr: String;
    welcome: String;
    manual: String;
    isEditing: boolean;
    role?: any;
    pageKind: boolean;
    playAudio: boolean;
    isMoveAllow: boolean;
    isRequestGoods: boolean;
    isWithHomePage: boolean;
    isSlideShow: boolean;
    isMapShow: boolean;
    isShowMediaLink: boolean;
    isLoadShowVideo: boolean;
    isMakeAdvFromExtPict: boolean;
}

export interface ShopDetails {
    dz: string;
    eMail: string;
    eMailBlankRequest: string;
    fName: string;
    firma: string;
    geoAddress: string;
    isSearchPageVariant: number;
    isShowGDate: true;
    mobileOpt: string;
    pageKind: boolean;
    phone: string;
    playAudio: true;
    repres: string;
    returnURL: string;
    wordEnter: string;
    isMoveAllow: boolean;
    isWithHomePage: boolean;
    isMakeAdvFromExtPict: boolean;
}

export interface AwsImageResponse {
    success: boolean;
    data: string;
}

export interface PermissionItem {
    isShow: boolean;
    alias: string;
    txt: string;
}
