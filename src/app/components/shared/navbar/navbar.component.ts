import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';
import { MainService } from '../../shared/services/main.service';
import { AuthService } from '../../shared/services/auth.service';
import {OrderService} from '../../shared/services/order.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { SearchDataService } from 'src/app/search-data.service';
import { EnvService } from 'src/app/env.service';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { ModifyAdvertsService } from '@components/shared/services/modify-adverts.service';
import { TokenService } from '../services/token.service';
import { StateService } from '../services/state.service';
import { CategoriesService } from '../services/categories.service';
import { RepeatService } from '../services/repeat.service';
import { SharedService } from '../services/shared.service';
import { SiteSettingsService } from '../services/sitesettings.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() public title;
  public showHeadline = true;
  public canAddAdvert: boolean = localStorage.getItem("canAdd") == 'true';
  public showBasket = false
  public isStudy: boolean = false;
  public hideCabinet = false
  public userName: string = '';
  public hideOnMainPage = false
  public isThatCabinetPage: boolean = false;
  public cabinetLinkParameter: number = 2; // 1  or 2;
  public background = '#e3f2fd';
  public isAuthenticated: Observable<boolean>;
  public isAdmin: boolean = false;
  public showAddingButton: boolean = false;
  public isSendRepeatButton: boolean = false;
  public isRepeatButton: boolean = false;
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public env: EnvService,
    public mainService: MainService,
    public authService: AuthService,
    public orderService: OrderService,
    public searchData: SearchDataService,
    public local: DictionaryService,
    public tokenService: TokenService,
    public repeatService: RepeatService,
    public copyAdverts: CopyAdvertsService,
    public shared: SharedService,
    public modifyAdverts: ModifyAdvertsService,
    public categoriesService: CategoriesService,
    public siteSettings: SiteSettingsService,
    private state: StateService,
  ) {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
    this.authService.getUserData.subscribe((res)=>{
      if(res)
        this.userName = res.fio;
    })
    this.isAdmin = this.authService.getUserId() == this.mainService.getCustId() 
    && this.mainService.getCustId() != null;
  }

  ngOnDestroy(): void {

  }

  public searchBarStatus = new BehaviorSubject(false);
  public newAdvertLink: string = this.env.shopDetails.pageKind ? '/addProduct' : '/commodity/new';

  ngOnInit() {
    this.cabinetLinkParameter = this.router.routerState.snapshot.url.includes('catcabinet') ? 1 : 2;
    this.isThatCabinetPage = this.router.routerState.snapshot.url.includes('cabinet');
    if(this.isThatCabinetPage){
      this.showBasket = false
      this.hideCabinet = false
      this.hideOnMainPage = true
    }else{
      this.showBasket = true
      this.hideCabinet = true
      this.hideOnMainPage = false
    }
    this.isAuthenticated.subscribe((res)=>{
      if(res){
        this.authService.getUserInfo(this.authService.getUserId())
        .subscribe((res: any)=>{
          this.isRepeatButton = res.isRepeat;
          this.isStudy = res.isStudy;
          this.showAddingButton = res.isEditing;
          this.isSendRepeatButton = res.isSendRepeat;
        });
      }
    })
  }

  headlineLimiter(text: String){
    return text ? text.length > 72 ? text.substring(0, 69)+ '...' : text : "";
  }

  get generateAdvLink(){
    if(this.env.advertUrl.trim().length == 0){
      return window.location.protocol +  "//" +  window.location.host.split(".").splice(-2).join(".")
    }else{
      return this.env.advertUrl
    }
  }
  get showAdvLink(){
    return window.location.host.split(".").length === 3;
    //return window.location.protocol +  "//" +  window.location.host.split(".").splice(-2).join(".") !== window.location.origin
  }

  getHomeLink(){
    switch(this.cabinetLinkParameter){
      case 1: {
        return {
          text: 'Каталог',
          url: '/cabinet'
        }
      }
      case 2: {
        return {
          text: 'Все объявления',
          url: '/catcabinet'
        }
      }
      default: {
        return {
          text: 'Главная',
          url: '/'
        }
      }
    }
  }

  isCabinet(): boolean {
    return window.location.pathname.includes('cabinet') || 
    window.location.pathname.includes('catcabinet')
  }

  showHideSearch(status){
    this.showHeadline = !status;
  }
  
  toOrder(){
    this.router.navigate([`${window.location.pathname}/order`])
  }

  back(){
    this.router.navigate(['/'])
  }


  cartClick() {
    if(this.router.url === '/') {
      this.router.navigate(['order'])
    } else {
      this.router.navigate([`categories/${this.route.snapshot.params.id}/products/order`])
    }
  }

  personal(){
    this.router.navigate(['auth/personal']);
  }
  subscribe(){
    this.router.navigate(['subscribe']);
  }


}
