import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CategoryItem } from "@components/categories/categories.component";
import { EnvService } from "src/app/env.service";
import { MoveAdvertsResp } from "src/app/interfaces/adverts";
import { Enums } from "../models/enums";
import { AuthService } from "../services/auth.service";
import { CategoriesService } from "../services/categories.service";
import { CommonService } from "../services/common.service";
import { CopyAdvertsService } from "../services/copyadverts.service";
import { DictionaryService } from "../services/dictionary.service";
import { DollService } from "../services/doll.service";
import { MainService } from "../services/main.service";
import { MoveAdvertsService } from "../services/moveadverts.service";
import { SpinnerService } from "../services/spinner.service";
import { StateService } from "../services/state.service";
import { StorageService } from "../services/storage.service";
const Swal = require("sweetalert2");

@Component({
    selector: "app-catalog-select-action",
    templateUrl: "./catalog-select-action.component.html",
    styleUrls: ["./catalog-select-action.component.scss"]
})
export class CatalogSelectActionComponent implements OnInit {
    constructor(
        private authService: AuthService,
        private mainService: MainService,
        public storageService: StorageService,
        public categoriesService: CategoriesService,
        public spinnerService: SpinnerService,
        public moveAdverts: MoveAdvertsService,
        public dollService: DollService,
        public commonStore: CommonService,
        public route: ActivatedRoute,
        public router: Router,
        public state: StateService,
        public env: EnvService,
        public local: DictionaryService,
        public copyAdverts: CopyAdvertsService
    ) {}
    title: string | null = null;
    action_title: string = null;
    showSpinner: boolean = false;
    initialCategories = [];
    selectedCategory: any = null;
    categories: CategoryItem[] = [];
    public catalogSelectItemsMode = Enums.CatalogSelectItemsMode;
    actionType: Enums.CatalogSelectItemsMode | null = null;
    private cust_id;

    ngOnInit() {
        this.definePageActionType();
    }

    private loadCatalog() {
        this.mainService.getShopInfo().subscribe(
            (res) => {
                this.cust_id = res.cust_id;
                this.showSpinner = false;
                this.categoriesService.fetchCategoriesAll(this.cust_id, this.authService.getUserId(), "4");
            },
            (error) => alert(error.error.message)
        );
        this.initialCategories = this.storageService.breadcrumbFlag ? this.storageService.getCategories() : [];
        this.storageService.breadcrumbFlag = false;
        setTimeout(() => window.scrollTo(0, 0), 300);
    }

    /* DOC
  page type cases:
  1 - for move adverts to another catalog category
  */
    definePageActionType() {
        switch (this.route.snapshot.routeConfig.path) {
            case "catalog/move": {
                this.actionType = Enums.CatalogSelectItemsMode.MOVE_TO_CATEGORY;
                this.title = this.local.getTranslate("move_adv_oth", "Количество выбранных обьявлений");
                this.action_title = this.local.getTranslate("cat_move", "Перемещение выбранных обьявлений");
                this.loadCatalog();
                break;
            }
            case "catalog/doll": {
                this.actionType = Enums.CatalogSelectItemsMode.DOLL;
                this.title = this.local.getTranslate("doll", "Матрешка");
                this.action_title = this.local.getTranslate("make_doll", "Создать матрешку");
                this.loadCatalog();
                break;
            }
            default:
                this.actionType = null;
        }
    }

    actionButton() {
        switch (this.actionType) {
            case Enums.CatalogSelectItemsMode.MOVE_TO_CATEGORY: {
                this.moveAdvertsAction();
                break;
            }
            case Enums.CatalogSelectItemsMode.DOLL: {
                this.makeDoll();
                break;
            }
            default:
                null;
        }
    }

    moveError() {
        this.spinnerService.display(false);
        Swal.fire({
            position: "center",
            icon: "warning",
            title: this.local.getTranslate("err_move", "Ошибка переноса. Попробуйте позже."),
            showConfirmButton: false,
            timer: 2000
        });
    }

    makeDoll() {
        // if (this.selectedCategory) {
        //     if (this.dollService.count) {
        //         const body = {
        //             prc_IDs: this.dollService.getSelectedAdverts().map((el) => el.prc_ID),
        //             prc_ID_Dst: this.selectedCategory.id,
        //             appCode: this.cust_id,
        //             cid: this.authService.getUserId()
        //         };
        //         this.spinnerService.display(true);
        //         this.mainService.makeDoll(body).subscribe(
        //             ({results}: any) => {
        //               this.spinnerService.display(false);
        //                 if (results.every((r) => r == true)) {
        //                     Swal.fire({
        //                         icon: "success",
        //                         title: this.local.getTranslate("doll_created", "Матрешка успешно перемещены"),
        //                         showConfirmButton: false,
        //                         timer: 2000,
        //                         willClose: () => {
        //                             this.router.navigate([`/cabinet/categories/${this.selectedCategory.id}/products`]);
        //                         }
        //                     });
        //                     this.dollService.cancel();
        //                     this.dollService.clearData();
        //                 } else {
        //                     this.moveError();
        //                 }
        //             },
        //             () => this.moveError()
        //         );
        //     } else {
        //         Swal.fire({
        //             position: "center",
        //             icon: "warning",
        //             title: this.local.getTranslate("adv_not_selected", "Обьявления для переноса не выбраны!"),
        //             showConfirmButton: false,
        //             timer: 2000
        //         });
        //     }
        // } else {
        //     Swal.fire({
        //         position: "center",
        //         icon: "warning",
        //         title: this.local.getTranslate("te_advertPageContainerSwitch", "Выберите категорию каталога"),
        //         showConfirmButton: false,
        //         timer: 2000
        //     });
        // }
    }

    moveAdvertsAction() {
        if (this.selectedCategory) {
            if (this.moveAdverts.count) {
                const body = {
                    prc_IDs: this.moveAdverts.getSelectedAdverts().map((el) => el.prc_ID),
                    id: this.selectedCategory.id,
                    appCode: this.cust_id,
                    cid: this.authService.getUserId()
                };
                this.spinnerService.display(true);
                this.mainService.moveAdverts(body).subscribe(
                    ({ result }: MoveAdvertsResp) => {
                        if (result) {
                            this.mainService.clearCategoriesHistory();
                            Swal.fire({
                                icon: "success",
                                title: this.local.getTranslate("move_done", "Обьявления успешно перемещены"),
                                showConfirmButton: false,
                                timer: 2000,
                                willClose: () => {
                                    setTimeout(()=>{
                                        this.router.navigate([`/cabinet/categories/${this.moveAdverts.lastDepartmentId || this.selectedCategory.id}/products`]);
                                    }, 1500);
                                    // this.router.onSameUrlNavigation = 'reload';
                                    // this.router.navigate([`/cabinet/categories/${this.selectedCategory.id}/products`],{ queryParams: { redirect: this.moveAdverts.lastDepartmentId} });
                                }
                            });
                           // this.moveAdverts.cancel();
                            this.moveAdverts.clearData();
                        } else {
                            this.moveError();
                        }
                        this.spinnerService.display(false);
                    },
                    () => this.moveError()
                );
            } else {
                Swal.fire({
                    position: "center",
                    icon: "warning",
                    title: this.local.getTranslate("adv_not_selected", "Обьявления для переноса не выбраны!"),
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        } else {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: this.local.getTranslate("te_advertPageContainerSwitch", "Выберите категорию каталога"),
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    previousItem: {} = null;
    categorySelect(items: CategoryItem[]) {
        if (!this.previousItem) {
            this.previousItem = items[items.length - 1];
        } else {
            if (JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length - 1])) {
                this.previousItem = items[items.length - 1];
            }
        }
        setTimeout(() => {
            this.categories = items;
            this.selectedCategory = items[items.length - 1];
        });
    }

    breadcrumbsClick(i) {
        this.selectedCategory = null;
        this.storageService.setCategories(this.categories.slice(0, i));
        this.categories = [];
        this.storageService.breadcrumbFlag = true;
        this.initialCategories = this.storageService.getCategories();
    }

    public isCabinet(): boolean {
        return window.location.pathname.includes("cabinet");
    }
    onCategoriesClick(items: CategoryItem[]) {
        this.storageService.setCategories(items);
    }
}
