import { Component, Input, OnInit, Output, EventEmitter, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { EnvService } from "src/app/env.service";
import { ShopInfo } from "../interfaces";
import { AuthService } from "../services/auth.service";
import { CategoriesService } from "../services/categories.service";
import { CopyAdvertsService } from "../services/copyadverts.service";
import { DictionaryService } from "../services/dictionary.service";
import { MainService } from "../services/main.service";
import { ModifyAdvertsService } from "../services/modify-adverts.service";
import { RepeatService } from "../services/repeat.service";
import { MoveAdvertsService } from "../services/moveadverts.service";
import { StateService } from "../services/state.service";
import { UserInfo } from "src/app/interfaces/user-info";
import { SharedService } from "../services/shared.service";
import { Enums } from "../models/enums";
import { Models } from "../models/models";
import { SpinnerService } from "../services/spinner.service";
import { BehaviorSubject, Subscription } from "rxjs";
import { DollService } from "../services/doll.service";
import { MatDialog } from "@angular/material";
import { PopupMenuComponent } from "./popup-menu/popup-menu.component";
const Swal = require("sweetalert2");

@Component({
    selector: "menu-header",
    templateUrl: "./menu-header.component.html",
    styleUrls: ["./menu-header.component.scss"]
})
export class MenuHeaderComponent implements OnInit, OnDestroy {
    constructor(
        public authService: AuthService,
        public modifyAdverts: ModifyAdvertsService,
        public mainService: MainService,
        public local: DictionaryService,
        public state: StateService,
        public env: EnvService,
        public copyAdverts: CopyAdvertsService,
        public repeatService: RepeatService,
        public moveService: MoveAdvertsService,
        public dollService: DollService,
        public categoriesService: CategoriesService,
        public shared: SharedService,
        public router: Router,
        public route: ActivatedRoute,
        public spinner: SpinnerService,
        private dialog: MatDialog
    ) {}
    @Input() isDetail: boolean = false;
    @Input() asPopup: boolean = false;
    @Input() showList: boolean = false;
    @Output() isClicked = new BehaviorSubject<boolean>(false);
    public isRepeatButton: boolean;
    public showAddingButton: boolean;
    public isSendRepeatButton: boolean;
    private $subscrIsAuth: Subscription;
    private $subscrCatId: Subscription;
    public isMoveAllow: boolean = false;
    public isMoveButton: boolean;
    public showMapButton: boolean;
    public isRussianDoll: boolean;
    public isRequestGoods: boolean;
    public showVacabulary = false;
    public isUserRepeatRegistationMode = false;
    public catalogSelectItemsMode = Enums.CatalogSelectItemsMode;
    public catId = null;
    public isThatCabinetPage = this.router.routerState.snapshot.url.includes("cabinet");
    @ViewChild("modalMenu", { static: true }) modalMenu: ElementRef;

    ngOnInit() {
        this.$subscrIsAuth = this.authService.isAuthenticatedObservable().subscribe((loggedIn) => {
            setTimeout(() => {
                this.isUserRepeatRegistationMode = localStorage.getItem("isShowMenuRepeat") === "1" && loggedIn;
            }, 500);
            if (loggedIn) {
                this.authService.getUserInfo().subscribe((uData: UserInfo) => {
                    this.isRepeatButton = uData.isRepeat;
                    this.showAddingButton = uData.isEditing;
                    this.isRussianDoll = uData.isRussianDoll;
                    this.isSendRepeatButton = localStorage.getItem("isShowMenuRepeat") === "1" || uData.isSendRepeat;
                    this.showVacabulary = uData.isStudyAll === undefined ? true : uData.isStudyAll;
                    this.isMoveButton = uData.isMoveAdvert || this.shared.isAdmin;
                    if (localStorage.getItem("isShowMenuRepeat") === "1") {
                        setTimeout(() => {
                            this.modeAction(this.catalogSelectItemsMode.REPEAT, true);
                        }, 500);
                    }
                });
            } else {
                this.repeatService.repeatMode = false;
            }
            this.mainService.getShopInfo().subscribe(({ isMoveAllow, isRequestGoods, isMapShow }) => {
                this.isMoveAllow = isMoveAllow;
                this.isRequestGoods = isRequestGoods;
                this.showMapButton = isMapShow;
            });
        });
        this.$subscrCatId = this.state.currentCatId.subscribe((r) => {
            this.catId = r;
        });
    }

    ngOnDestroy(): void {
        if (this.$subscrIsAuth) {
            this.$subscrIsAuth.unsubscribe();
        }

        if (this.$subscrCatId) {
            this.$subscrCatId.unsubscribe();
        }
    }

    public getMapLink() {}

    manual() {
        const route = this.isDetail ? Models.PageRoutes.Detail : this.route.snapshot.routeConfig.path;
        this.shared.manual(route);
    }

    isMobile() {
        return window.innerWidth < 600;
    }

    admin() {
        window.open(this.env.adminUrl, "_blank");
    }

    onLogout() {
        this.isRepeatButton = false;
        this.showAddingButton = false;
        this.isSendRepeatButton = false;
        this.authService.logout();
        this.state.clearShopInfo();
        this.state.getShopInfo().then((response: ShopInfo) => {
            const { cust_id } = response;
            this.mainService.setShopInfo(response);
            this.categoriesService.fetchCategoriesAll(cust_id);
            this.mainService.loadBackgrounds(response);
        });
    }

    /* DOC
  type of mode:
    1 - repeat mode;
    2 - copy mode;
    3 - move mode (move items to other category);
  */
    modeAction(type: Enums.CatalogSelectItemsMode, value: boolean | null = null) {
        switch (type) {
            case Enums.CatalogSelectItemsMode.REPEAT: {
                this.repeatService.repeatMode = value !== null ? value : !this.repeatService.repeatMode;
                this.copyAdverts.copyMode = false;
                this.dollService.dollMode = false;
                this.moveService.moveMode = false;
                if (!this.repeatService.repeatMode) {
                    this.repeatService.clearData();
                }
                break;
            }
            case Enums.CatalogSelectItemsMode.COPY_ITEMS: {
                this.copyAdverts.copyMode = value !== null ? value : !this.copyAdverts.copyMode;
                this.repeatService.repeatMode = false;
                this.dollService.dollMode = false;
                this.moveService.moveMode = false;
                if (!this.copyAdverts.copyMode) {
                    this.copyAdverts.clearData();
                }
                break;
            }
            case Enums.CatalogSelectItemsMode.MOVE_TO_CATEGORY: {
                this.moveService.moveMode = value !== null ? value : !this.moveService.moveMode;
                this.repeatService.repeatMode = false;
                this.dollService.dollMode = false;
                this.copyAdverts.copyMode = false;
                if (!this.moveService.moveMode) {
                    this.moveService.clearData();
                }
                break;
            }
            case Enums.CatalogSelectItemsMode.DOLL: {
                this.dollService.dollMode = value !== null ? value : !this.dollService.dollMode;
                this.moveService.moveMode = false;
                this.repeatService.repeatMode = false;
                this.copyAdverts.copyMode = false;
                if (!this.moveService.moveMode) {
                    this.moveService.clearData();
                }
                break;
            }
            default:
                null;
        }
    }

    public editCat(): void {
        window.open(this.env.adminUrl + "/categories", "_blank");
    }

    public openSite() {
        Swal.fire({
            title: this.local.getTranslate("site_name", "Введите имя сайта"),
            input: "text",
            inputAttributes: {
                autocapitalize: "off"
            },
            showCancelButton: true,
            cancelButtonText: this.local.getTranslate("cancel", "Отмена"),
            confirmButtonText: this.local.getTranslate("ti_Page", "Войти"),
            showLoaderOnConfirm: true,
            preConfirm: (site) => {
                return this.state.changeShop(site).then((response: any) => {
                    if (!response) {
                        throw new Error(response.statusText);
                    }
                    return response;
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result) {
                if (result.value) {
                    const { cust_id } = result.value;
                    this.authService.logoutUserOnly();
                    this.mainService.setShopInfo(result.value);
                    this.mainService.loadBackgrounds(result.value);
                    this.mainService.getFonPictures();
                    if (this.isThatCabinetPage) {
                        this.categoriesService.fetchCategoriesUser(cust_id);
                    } else {
                        this.categoriesService.fetchCategoriesAll(cust_id);
                    }
                }
            }
        });
    }
}
