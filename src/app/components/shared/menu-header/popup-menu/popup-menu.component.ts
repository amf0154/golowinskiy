import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-popup-menu',
  templateUrl: './popup-menu.component.html',
  styleUrls: ['./popup-menu.component.scss']
})
export class PopupMenuComponent {

  constructor(public dialogRef: MatDialogRef<PopupMenuComponent>) { }

  public isClicked(e: boolean): void {
    if(e) {
      this.dialogRef.close();
    }
  }
}
