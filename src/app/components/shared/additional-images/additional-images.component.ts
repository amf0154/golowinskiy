import { Component, Input, OnInit, Output, EventEmitter, OnDestroy, NgModule } from "@angular/core";
import { MatDialog } from "@angular/material";
import { EditAdditImgComponent } from "@components/main-page/edit-addit-img/edit-addit-img.component";
import { Observable, Subscription } from "rxjs";
import { AdditionalImage } from "../models/additional-image.model";
import { Enums } from "../models/enums";
import { ModifyAdvertsService } from "../services/modify-adverts.service";

@Component({
    selector: "slider-images",
    templateUrl: "./additional-images.component.html",
    styleUrls: ["./additional-images.component.scss"]
})
export class AdditionalImagesComponent implements OnInit, OnDestroy {
    constructor(public modifyAdverts: ModifyAdvertsService, private dialog: MatDialog) {}
    @Input() selectedImg: any;
    @Input() nextSlide?: Observable<boolean>;
    @Output() click: EventEmitter<AdditionalImage> = new EventEmitter();
    @Input() contentType: number = 1;
    @Output() resetSelectedAddImg: EventEmitter<boolean> = new EventEmitter();
    @Output() outputActions: EventEmitter<number> = new EventEmitter(); // 1 - no more audioimages;
    @Input() set data(detail: any) {
        this.advert = detail;
        this.additionalImages = detail.additionalImages;
        //  this.reset();
    }
    public additionalImages = [];
    private $nextSlideSubscription: Subscription;
    private perPage = 5;
    private advert;
    public videoType = Enums.ModifyAdvertType.EDIT_VIDEO;
    public images = [];
    public currentPage: number = 1;
    private currentIndexSlide: number = null;
    public totalPages: number = 0;
    private pages = new Map();

    ngOnInit(): void {
        this.initImgs();
        if (this.nextSlide instanceof Observable)
            this.$nextSlideSubscription = this.nextSlide.subscribe(() => {
                this.nextAudioSlide();
            });
    }

    initImgs() {
        this.totalPages = Math.ceil(this.additionalImages.length / this.perPage);
        // generate map {index : page} START;
        Array.from({ length: this.additionalImages.length }).forEach((_, i) => {
            const index = i;
            this.pages.set(index, Math.floor(index / 5) + 1);
        });
        // generate map END;
        this.images = this.additionalImages.slice(0, this.perPage);
        this.currentIndexSlide = null;
    }

    move(v: number) {
        v == 0 ? this.currentPage-- : this.currentPage++;
        const distinct = this.currentPage * this.perPage;
        this.images = this.additionalImages.slice(distinct - this.perPage, distinct);
    }

    mouseOverImg(img: AdditionalImage) {
        if (img.t_image) this.click.emit(img);
        this.currentIndexSlide = this.additionalImages.indexOf(img);
    }

    reset() {
        this.currentIndexSlide = null;
        this.currentPage = 1;
        this.initImgs();
    }

    private nextAudioSlide() {
        const otherImgStack = Number.isInteger(this.currentIndexSlide)
            ? this.additionalImages.slice(this.currentIndexSlide + 1, this.additionalImages.length)
            : this.additionalImages;
        if (otherImgStack && otherImgStack.length) {
            const nextAudioImg = otherImgStack.filter((img) => (this.contentType === 1 ? img.audio : img.video))[0];
            if (nextAudioImg) {
                if (!this.currentIndexSlide) {
                    this.currentIndexSlide = this.additionalImages.indexOf(nextAudioImg);
                }
                const nextSlideIndex = this.additionalImages.indexOf(nextAudioImg);
                const page = this.pages.get(nextSlideIndex);
                this.currentPage = page;
                const distinct = page * this.perPage;
                this.images = this.additionalImages.slice(distinct - this.perPage, distinct);
                this.mouseOverImg(this.images[this.images.indexOf(nextAudioImg)]);
            } else {
                this.reset();
                this.outputActions.emit(1);
            }
        } else {
            this.reset();
            this.outputActions.emit(1);
        }
    }

    ngOnDestroy(): void {
        if (this.$nextSlideSubscription) this.$nextSlideSubscription.unsubscribe();
    }

    videoClick(el) {
        this.dialog
            .open(EditAdditImgComponent, {
                width: "40%",
                data: {
                    advert: this.advert,
                    img: el
                }
            })
            .afterClosed()
            .subscribe((isUpdated) => {
                if (isUpdated) {
                    this.outputActions.emit(3);
                }
            });
    }
}
