import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DictionaryService } from '../services/dictionary.service';
import { MainService } from '../services/main.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'edit-audio',
  templateUrl: './edit-audio.component.html',
  styleUrls: ['./edit-audio.component.scss']
})
export class EditAudioComponent implements OnInit {

  @Input() audio: any = null;
  @Input() advert: any = null;
  @Output('outputfile') backData: any = new EventEmitter();
  @Output('audioDeleted') audioDeleted: any = new EventEmitter();
  public  showRecorder: boolean = false;
  constructor(
    public local: DictionaryService,
    public mainService: MainService
  ) { }


  ngOnInit() {
  }


  showHideRecorder(val){
    if(val)
      this.audioRecord(null);
    this.showRecorder = !this.showRecorder
  }

  recordFileBlob = null;
  audioRecord($event){  
    this.recordFileBlob = $event;
    this.backData.emit(this.recordFileBlob);
  }

  fileToUpload: File = null;
  handleFileInput(files: FileList){
    this.fileToUpload = files.item(0);
    this.backData.emit(this.fileToUpload);
  }

  clear(){
    this.fileToUpload = null;
    this.backData.emit(null)
  }

  deleteAudio(){
    if(this.audio != null && typeof this.audio === 'string' && this.audio.includes('cloudfront')){
      let body = {
        title: this.audio.replace(/^https?:\/\//,''),
        isCloud: true
      };
      this.deleteAudioRecord(body);
    }else{
      this.audio = null;
      this.recordFileBlob = null;
    }
  }



  deleteAudioRecord(body){
    Swal.queue([{
      title: this.local.getTranslate("delete_audio",'Удаление аудиозаписи'),
      confirmButtonText: this.local.getTranslate("delete",'Удалить'),
      text: this.local.getTranslate("want_delete_audio",'Вы действительно хотите удалить аудиозапись?'),
      cancelButtonText: this.local.getTranslate("cancel",'Отмена'),
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        this.audio = null;
        this.recordFileBlob = null;
        this.backData.emit(null);
        return this.mainService.deleteCloudContent(body.title)  
          .subscribe(() => {
              Swal.fire({
                icon: 'success',
                title: this.local.getTranslate("audio_deleted","Аудиозапись успешно удалена"),
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.audio = null;
                  this.audioDeleted.next(true);
                  this.recordFileBlob = null;
                  this.mainService.deleteMediaLink(this.advert.prc_ID);
                }
              });
            
          })
      }
    }])
  }

}
