import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';

@Injectable()
export class SuperAdminRoutes{
    private routes = [
        'Localization/Update',
        'Localization/AddDelete',
        'Catalog/DeleteAll',
        'Catalog/DeleteAllConfirmed'
    ];
    checkRoute(request: HttpRequest<any>,currentToken){
        return  this.routes.some((r)=> request.url.includes(r)) ? 
        sessionStorage.getItem('token') : currentToken;
    }
}

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private tokenService: TokenService,private saRoutes: SuperAdminRoutes) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        if (this.tokenService.isTokenExpired()) {
            const headers = new HttpHeaders({
                'Authorization': `Bearer ${this.saRoutes.checkRoute(request,this.tokenService.getAccessToken())}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
              });
            request = request.clone({headers});
        }

        return next.handle(request);
    }
}