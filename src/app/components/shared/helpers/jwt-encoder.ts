import { JwtHelperService } from '@auth0/angular-jwt';
const helper = new JwtHelperService();
export class EncoderForToken {

  public getEncodeToken(token) {
    return helper.decodeToken(token);
  }

  public isTokenExpired(): boolean {
    let token: string = localStorage.getItem('token');
    let tokenExpired: boolean = token != null && helper.isTokenExpired(token);
    return !helper.isTokenExpired(token);
  }
}