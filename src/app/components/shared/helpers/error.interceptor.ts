import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { OrderService } from '../services/order.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService,private authService: AuthService, 
        public router: Router, private orderService: OrderService ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) { // || err.status === 403
                // auto logout if 401 response returned from api
                this.authenticationService.isAuthenticatedBehaviorSubject.next(false);
                this.authenticationService.clearLocalStorage();
                this.orderService.clearCartAndOrder()
                this.router.navigate([`/auth/login`], {
                    queryParams: {
                        route: this.router.routerState.snapshot.root.queryParams.route
                    }
            })
            const error = err.message || err.statusText;
            return throwError(error);
        };
    }))
}};