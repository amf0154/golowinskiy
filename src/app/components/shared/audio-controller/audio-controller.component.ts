import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AuthService } from '../services/auth.service';
import { MainService } from '../services/main.service';
import { SharedService } from '../services/shared.service';
import { StorageService } from '../services/storage.service';
import { UploadService } from '../services/upload.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'audio-controller',
  templateUrl: './audio-controller.component.html',
  styleUrls: ['./audio-controller.component.scss']
})
export class AudioControllerComponent implements OnInit {
  constructor(
    private uploadService: UploadService,
    private shared: SharedService,
    private authService: AuthService,
    private storage: StorageService,
    private mainService: MainService,
    public dialog: MatDialog
    ) { }
  @Output() isAudioUploaded = new EventEmitter();
  @Input() inputFile = null;
  @Input() additionalImages? = [];
  @Input() isImgAudio?: boolean = false;
  @Input() selectedImg?: string = null;
  @Input() advId?: any = null;
  @Input() isAudioOwner: boolean = false;
  selectedFiles: FileList;
  fileInfos = null;
  progressInfos = [];
  message = '';
  indexMainImg: number  = 0;
  private audioImageIndex = null;

  ngOnInit(): void {
      if(this.isImgAudio){
        if(this.isAudioOwner){
          const file = this.additionalImages.find((img)=> img.t_image == this.selectedImg);
          this.audioImageIndex = file.imageOrder;
          this.fileInfos = { link: file && file.audio ? file.audio : null };
        }else{
          const file = this.additionalImages.find((img)=> img.t_image == this.selectedImg);
          this.audioImageIndex = file.imageOrder;
          const audio = this.storage.getTempAudio(this.advId,this.audioImageIndex);
          this.fileInfos = {
            link: audio ? audio : null
          };
        }
      }else{
        if(this.isAudioOwner){
          this.fileInfos = Object.assign({},this.inputFile);
        }else {
          const audio = this.storage.getTempAudio(this.advId);
          this.fileInfos = {
            link: audio ? audio : null
          };
        }
      };
  }

  selectFiles(event): void {
    if(event.target.files.length === 1){
      this.selectedFiles = event.target.files;
      if(this.isAudioOwner){
        this.uploadFiles()
      }else{
        const file = this.additionalImages.find((img)=> img.t_image == this.selectedImg);
        this.storage.setTempAudio(this.advId, this.isImgAudio ? file.imageOrder : '', this.selectedFiles[0]);
        this.fileInfos = {
          type: 'uploaded-temp',
          link: event.target.files[0],
          isAudioImg: this.isImgAudio,
          index: this.isImgAudio ? this.audioImageIndex : null,
        }; 
        this.isAudioUploaded.emit(this.fileInfos);  
      }
    }else{
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Не выбран 1 файл для загрузки',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }

  uploadFiles(): void {
    this.message = '';
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

  upload(idx, file): void {  
    this.progressInfos[idx] = { value: 0, fileName: null };
    const imgName = this.shared.uuidv4()+'.mp3';
    this.uploadService.upload(file,imgName, 2).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
            const {data, success} = event.body;
          if(success){
            this.fileInfos = {
              type: 'uploaded',
              link: data,
              isAudioImg: this.isImgAudio,
              index: this.isImgAudio ? this.audioImageIndex : null
            }; 
            this.progressInfos.splice(idx,1); 
            this.isAudioUploaded.emit(this.fileInfos);   
          }
        }
      },
      err => {
        this.progressInfos[idx].value = 0;
        this.message = 'Could not upload the file:' + file.name;
      });

  }

  fullLink(link){
    return link instanceof Blob ? link : link.includes('https://') ? link : 'https://' + link;
  }

  delete(){
    if(this.isAudioOwner){
      const {link} = this.fileInfos;
      if(link){
        Swal.queue([{
          title: 'Удаление аудиозаписи',
          confirmButtonText: 'Удалить',
          text: 'Вы действительно хотите удалить аудиозапись?',
          cancelButtonText: 'Отмена',
          showCancelButton: true,
          showLoaderOnConfirm: true,
          preConfirm: () => {
            const req = this.isImgAudio ? 
            this.mainService.deleteAudioImgLink([{
              prc_ID: this.advId,
              mediaOrder: this.audioImageIndex,
              mediaLink: this.fileInfos.link,
              appcode: this.mainService.getCustId(),
              cid: this.authService.getUserId()}]
            ) : 
            this.mainService.deleteCloudContent(link);  
            req.subscribe(() => {
                Swal.fire({
                  icon: 'success',
                  title: "Аудиозапись успешно удалена",
                  showConfirmButton: false,
                  timer: 2000,
                  willClose: () => {
                    this.fileInfos.link = null;
                    this.isAudioUploaded.emit({
                      isAudioImg: this.isImgAudio, 
                      type: 'deleted', 
                      index: this.audioImageIndex
                    }); 
                  }
                });
            })
          }
        }])
      }
    }else{
      const imageOrder = this.isImgAudio ? this.additionalImages.find((img)=> img.t_image == this.selectedImg).imageOrder : '';
      this.storage.setTempAudio(this.advId, imageOrder, null);
      this.isAudioUploaded.emit({
        isAudioImg: this.isImgAudio, 
        type: 'deleted-temp', 
        index: this.isImgAudio ? this.audioImageIndex : null
      }); 
    }
  }

  isRecordMode: boolean = false;
  record(){
    this.isRecordMode = !this.isRecordMode;
    // this.dialog.open(VoiceRecorderTwoComponent, {
    //   width: '250px',
    //   data: [],
    // }).afterClosed()
    // .subscribe((file)=>{
    //   if(file instanceof Blob){
    //     console.log(file)
    //     this.selectFiles({
    //       target: {
    //         files: [file]
    //       }
    //     })
    //   }
    // })
  }

  outputAudio(data){
    if(data instanceof Blob){
      this.selectFiles({
        target: {
          files: [data]
        }
      })
    };
    this.isRecordMode = !this.isRecordMode;
  }

}
