import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioControllerComponent } from './audio-controller.component';

describe('AudioControllerComponent', () => {
  let component: AudioControllerComponent;
  let fixture: ComponentFixture<AudioControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
