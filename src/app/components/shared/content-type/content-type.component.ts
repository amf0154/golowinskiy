import { Component, EventEmitter, Output } from "@angular/core";
import { DictionaryService } from "../services/dictionary.service";

@Component({
    selector: "content-type",
    templateUrl: "./content-type.component.html",
    styleUrls: ["./content-type.component.css"]
})
export class ContentTypeComponent {
    constructor(public readonly local: DictionaryService) {}
    @Output() onChange = new EventEmitter();
    public slideType = [
        { title: this.local.getTranslate("audio", "Аудио"), value: 1 },
        { title: this.local.getTranslate("video", "Видео"), value: 2 }
    ];
    public currentType = localStorage.getItem("slideType")
        ? Number(localStorage.getItem("slideType"))
        : this.slideType[0].value;
    public selectedType = this.slideType.find((el) => el.value === this.currentType).title;
    public changeType(type: any) {
        this.currentType = type;
        this.selectedType = this.slideType.find((el) => el.value === type).title;
        localStorage.setItem("slideType", type);
        this.onChange.emit(type);
    }
}
