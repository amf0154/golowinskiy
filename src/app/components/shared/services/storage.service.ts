import {Injectable} from '@angular/core';
import {CategoryItem} from '../../categories/categories.component';
import {Subject} from 'rxjs';

// service for runtime storage
@Injectable({
  providedIn: 'root'
})
export class StorageService {
  // used for save selected categories after breadcrumbs click
  private categories: CategoryItem[] = []
  private _breadcrumbFlag = false;
  private tempAudioStorage = new Map();
  fromCabinetAllItems: boolean = false;
  paginateConfig = {
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 0
  }
  // used for sync selected categories between categories component and mobile component
  public selectedCategories: Subject<CategoryItem[]> = new Subject<CategoryItem[]>()

  setCategories(categories: CategoryItem[]) {
    this.categories = categories || []
  }

  getCategories(): CategoryItem[] {
    return this.categories
  }

  public get breadcrumbFlag() {
    return this._breadcrumbFlag;
  }

  public set breadcrumbFlag(flag: boolean) {
    this._breadcrumbFlag = flag;
  }

  getTempAudio(articleId,index = ''){
    return this.tempAudioStorage.get(articleId+'_ind'+index);
  }

  setTempAudio(articleId,index, audio){
    if(audio){
      this.tempAudioStorage.set(articleId+'_ind'+index,audio)
    }else{
      const audio = this.tempAudioStorage.get(articleId+'_ind'+index);
      if(audio)
        this.tempAudioStorage.delete(articleId+'_ind'+index);
    }
  }
  clearTempAudios(){
    this.tempAudioStorage.clear();
  }

}
