import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
const helper = new JwtHelperService();
@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private jwtHelper = new JwtHelperService();
  public isTokenExpired(): boolean {
    try{
      let token: string = localStorage.getItem('token');
      let tokenExpired: boolean = token != null && helper.isTokenExpired(token);
      return !helper.isTokenExpired(token);
    }catch(e){
      return false;
    }
  }
  public getAccessToken(): string {
    return localStorage.getItem('token');
  }
}
