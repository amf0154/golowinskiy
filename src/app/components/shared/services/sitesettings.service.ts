import { Injectable, OnDestroy } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Enums } from "../models/enums";
const Swal = require("sweetalert2");
@Injectable({
    providedIn: "root"
})
export class SiteSettingsService implements OnDestroy {
    //  public audioRepeats = null;

    private settingParams = {
        slideType: localStorage.getItem("slideType") ? Number(localStorage.getItem("slideType")) : 1,
        audioRepeats: this.getAudioRepeats() ? this.getAudioRepeats() : 1
    };
    private $isShuffleContent = new BehaviorSubject(localStorage.getItem("isShuffleContent") === "true");
    private $frozenSettings = new BehaviorSubject(JSON.parse(localStorage.getItem("freezeSettings")));
    private $settingSubscriber = new Subject();
    public siteSettings = new BehaviorSubject(this.settingParams);

    public updateConfig() {
        this.settingParams["slideType"] = localStorage.getItem("slideType")
            ? Number(localStorage.getItem("slideType"))
            : 1;
        this.settingParams["audioRepeats"] = this.getAudioRepeats() ? this.getAudioRepeats() : 5;
        this.siteSettings.next(this.settingParams);
    }

    public getSiteSettings(): Observable<{ slideType; audioRepeats }> {
        return this.siteSettings.asObservable().pipe(takeUntil(this.$settingSubscriber));
    }

    public updSlideType(value): void {
        if (value === 1 || value === 2) {
            localStorage.setItem("slideType", value);
            this.updateConfig();
        }
    }

    public updAudioRepeats(value): void {
        localStorage.setItem("slideType", value);
        this.updateConfig();
    }

    ngOnDestroy(): void {
        this.$settingSubscriber.next();
        this.$settingSubscriber.complete();
    }

    public setAudioRepeats(count: number) {
        localStorage.setItem("audioRepeats", count.toString());
        this.updateConfig();
    }

    public getAudioRepeats(): number {
        return Number(localStorage.getItem("audioRepeats"));
    }

    public isShuffleContent(): Observable<boolean> {
        return this.$isShuffleContent.asObservable();
    }
    public isShuffleState(state = false): void {
        localStorage.setItem("isShuffleContent", state.toString());
        this.$isShuffleContent.next(state);
    }

    public setFrozenSettings(value: boolean,key: Enums.SiteDepartment): void {
       const settings = JSON.parse(localStorage.getItem("freezeSettings"));
       settings[key] = value;
       this.$frozenSettings.next(settings);
       localStorage.setItem("freezeSettings",JSON.stringify(settings));
    }

    public getFrozeSetting(key:Enums.SiteDepartment): boolean | Observable<boolean> {
        const settings = JSON.parse(localStorage.getItem("freezeSettings"));
        return settings[key];
    }

    public getFrozeSettingsObs(): Observable<boolean> {
        return this.$frozenSettings.asObservable();
    }

    public getDepartmentTypeByRoute(route: string | null) : Enums.SiteDepartment | null {
        if(!route) {
            return null;
        }
        const department = route.split('/').length > 1 ? route.split('/')[1] : null;
        switch(department) {
            case Enums.SiteDepartment.REPEAT: return Enums.SiteDepartment.REPEAT;
            case Enums.SiteDepartment.STUDY: return Enums.SiteDepartment.STUDY;
            case Enums.SiteDepartment.CABINET:
            case '':
            case Enums.SiteDepartment.MAIN: return Enums.SiteDepartment.MAIN;
          //  case Enums.SiteDepartment.CABINET: return Enums.SiteDepartment.CABINET;
            default: return null;
        }
    }

}
