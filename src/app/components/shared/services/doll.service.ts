import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { MainService } from "./main.service";
import { StateService } from "./state.service";
import { AuthService } from "./auth.service";
import { DictionaryService } from "./dictionary.service";
import { Router } from "@angular/router";
import { SpinnerService } from "./spinner.service";
import { EnvService } from "src/app/env.service";
const Swal = require("sweetalert2");

@Injectable({
    providedIn: "root"
})
export class DollService {
    private selectedAdvertsStack = new BehaviorSubject([]);
    public dollMode = false;
    public buildedAdverts = [];
    public showSpinner = false;
    public stackOfAllAdverts = [];
    public has = (item: any) => this.selectedAdvertsStack.getValue().find((el) => el.prc_ID == item.prc_ID);

    public get count() {
        return this.selectedAdvertsStack.getValue().length;
    }

    constructor(
        private state: StateService,
        public router: Router,
        public local: DictionaryService,
        private env: EnvService,
        private authService: AuthService,
        private mainService: MainService,
        private spinnerService: SpinnerService
    ) {}

    getSelectedAdverts() {
        return this.selectedAdvertsStack.getValue();
    }

    deleteAdv(prc_ID: number) {
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find((el) => el.prc_ID == prc_ID);
        if (searchItem) {
            let index = existStack.indexOf(searchItem);
            existStack.splice(index, 1);
            this.selectedAdvertsStack.next(existStack);
        }
    }

    public put(item) {
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find((el) => el.prc_ID == item.prc_ID);
        if (!searchItem) {
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        } else {
            existStack.splice(existStack.indexOf(searchItem), 1);
        }
        if(this.buildedAdverts.length){
            this.selectedAdvertsStack.next([
                {
                    prc_ID: item.prc_ID,
                    title: item.tName,
                    author: item.suplier,
                    date: new Date().toISOString()
                }
            ]);
        }else {
            this.selectedAdvertsStack.next(existStack);
        }
        
    }

    public clearData() {
        this.selectedAdvertsStack.next([]);
        this.buildedAdverts = [];
    }

    public cancel() {
        this.clearData();
        this.dollMode = false;
    }

    public selectAll() {
        let existStack = [];
        this.stackOfAllAdverts.forEach((item) => {
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        });
        let newStack = this.selectedAdvertsStack.value;
        const preveiousDataIDS = this.selectedAdvertsStack.value.map((el) => el.prc_ID);
        for (let curr of existStack) {
            if (!preveiousDataIDS.includes(curr.prc_ID)) {
                newStack.push(curr);
            }
        }
        this.selectedAdvertsStack.next(newStack);
    }

    moveError() {
        this.spinnerService.display(false);
        Swal.fire({
            position: "center",
            icon: "warning",
            title: this.local.getTranslate("err_move", "Ошибка переноса. Попробуйте позже."),
            showConfirmButton: false,
            timer: 2000
        });
    }

    createDoll() {
            if (this.count) {
                const body = {
                    prc_IDs: this.buildedAdverts,
                    prc_ID_Dst: this.selectedAdvertsStack.value[0].prc_ID,
                    appCode: this.env.shopInfo.cust_id,
                    cid: this.authService.getUserId()
                };
                this.spinnerService.display(true);
                this.mainService.makeDoll(body).subscribe(
                    ({results}: any) => {
                      this.spinnerService.display(false);
                        if (results.every((r) => r == true)) {
                            Swal.fire({
                                icon: "success",
                                title: this.local.getTranslate("doll_created", "Матрешка успешно создана"),
                                showConfirmButton: false,
                                timer: 2000,
                                willClose: () => {
                                    this.clearData();
                                }
                            });
                        } else {
                            this.moveError();
                        }
                    },
                    () => this.moveError()
                );
            } else {
                Swal.fire({
                    position: "center",
                    icon: "warning",
                    title: this.local.getTranslate("adv_not_selected", "Обьявления для переноса не выбраны!"),
                    showConfirmButton: false,
                    timer: 2000
                });
            }
    }


    public build() {
        if (this.selectedAdvertsStack.getValue().length === 0) {
            Swal.fire({
                icon: "warning",
                title: this.local.getTranslate("adv_not_selected", "Обьявления для переноса не выбраны!"),
                showConfirmButton: false,
                timer: 2000
            });
        } else {
            const selectedAdverts = [...this.selectedAdvertsStack.getValue().map((el) => el.prc_ID)];
            this.clearData();
            this.buildedAdverts = selectedAdverts;
        }
    }
}
