import { Injectable } from "@angular/core"
import { Router } from "@angular/router"
import { BehaviorSubject } from "rxjs";
import { Location } from '@angular/common';
import { MainService } from "./main.service";
import { EnvService } from "src/app/env.service";
import { ShopInfo } from "../interfaces";
import { StateService } from "./state.service";
import { AuthService } from "./auth.service";
const Swal = require('sweetalert2');
@Injectable({
    providedIn: 'root'
})
export class RepeatService{
    private selectedAdvertsStack = new BehaviorSubject([]);
    public repeatMode: boolean = false;
    public showSpinner: boolean = false;
    public stackOfAllAdverts = [];
    public portalId: any = null;
    public has = (item) => this.selectedAdvertsStack.getValue().find(el=>el.prc_ID == item.prc_ID); 
    get count(){
        return this.selectedAdvertsStack.getValue().length;
    } 
    constructor(
        private router: Router,
        private _location: Location,
        private state: StateService,
        private env: EnvService,
        private authService: AuthService,
        private mainService: MainService){
            this.portalId = this.authService.getPortal();
        // let savedItems = localStorage.getItem("copiedItems");
        // if(savedItems != null && JSON.parse(localStorage.getItem("copiedItems")).length != 0){
        //     this.selectedAdvertsStack.next(JSON.parse(localStorage.getItem("copiedItems")));
        //     this.selectedCategoriesStack.next(JSON.parse(localStorage.getItem("copiedCategories")))
        // }   
    }

    getSelectedAdverts(){
        return this.selectedAdvertsStack.getValue();
    }

    deleteAdv(prc_ID:  number){
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find(el=>el.prc_ID == prc_ID);
        if(searchItem){
           let index = existStack.indexOf(searchItem);
           existStack.splice(index,1);
           this.selectedAdvertsStack.next(existStack);
           this.saveData();
        }
    }

    public put(item){
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find(el=>el.prc_ID == item.prc_ID);
        if(!searchItem){
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        }else{
            existStack.splice(existStack.indexOf(searchItem),1);
        }
        this.selectedAdvertsStack.next(existStack);
        this.saveData();
    }


    public clearData(){
        this.selectedAdvertsStack.next([]);
        if (localStorage.getItem("isShowMenuRepeat") === "1") {
            localStorage.setItem("isShowMenuRepeat","0");
        }
    }

    public cancel(){
        this.clearData();
        this.repeatMode = false;
    }

    public selectAll(){
        let existStack = [];
        this.stackOfAllAdverts.forEach((item) => {
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        });
        let newStack = this.selectedAdvertsStack.value;
        const preveiousDataIDS = this.selectedAdvertsStack.value.map((el) => el.prc_ID);
        for (let curr of existStack) {
            if (!preveiousDataIDS.includes(curr.prc_ID)) {
                newStack.push(curr);
            }
        }
        this.selectedAdvertsStack.next(newStack);
        this.saveData();
    }

    public doRepeat(){
        if(this.selectedAdvertsStack.getValue().length === 0){
            Swal.fire({
                icon: 'warning',
                title: "Выберите материал для повторения!",
                showConfirmButton: false,
                timer: 2000,
            });
        }else{
            this.showSpinner = true;
            this.state.getShopInfo().then((res: ShopInfo) => {
                const { cust_id } = res;
                this.mainService.doRepeat({
                    prc_IDs: this.selectedAdvertsStack.getValue().map((el)=>el.prc_ID),
                    cust_ID: this.mainService.getUserIdFromToken(),
                    appCode: cust_id,
                  }).subscribe((r)=>{
                      if(r){
                        this.clearData();
                        Swal.fire({
                            icon: 'success',
                            title: "Успешно выполнено!",
                            timer: 2500,
                            willClose: () => {
                                this.showSpinner = false;
                            }
                        });
                      }else{
                        Swal.fire({
                            icon: 'error',
                            title: "Произошла ошибка повторения!",
                            showConfirmButton: false,
                            timer: 2500,
                            willClose: () => {}
                        });
                      }
                },()=>{
                    Swal.fire({
                        icon: 'error',
                        title: "Произошла ошибка повторения!",
                        showConfirmButton: false,
                        timer: 2500,
                        willClose: () => {}
                    });
                    this.showSpinner = false;  
                });
            });
        }
    }


    public saveData(){
        localStorage.setItem("repeatAdverts",JSON.stringify(this.selectedAdvertsStack.getValue()));
    } 



}