import { Injectable } from "@angular/core"
import { Router } from "@angular/router"
import { BehaviorSubject } from "rxjs";
import { AuthService } from "./auth.service";
import { DictionaryService } from "./dictionary.service";
import { SharedService } from "./shared.service";
const Swal = require('sweetalert2');
@Injectable({
    providedIn: 'root'
})
export class MoveAdvertsService{
    private selectedAdvertsStack = new BehaviorSubject([]);
    public lastDepartmentId = null;
    public moveMode: boolean = false;
    public showSpinner: boolean = false;
    public stackOfAllAdverts = [];
    public portalId: any = null;
    public has = (item) => {
        if(item === null) {
            return false;
        }
        return this.selectedAdvertsStack.getValue().find(el=>el.prc_ID == item.prc_ID)
    }; 
    get count(){
        return this.selectedAdvertsStack.getValue().length;
    } 
    constructor(
        private router: Router,
        private local: DictionaryService,
        private shared: SharedService,
        private authService: AuthService){
        this.portalId = this.authService.getPortal();   
    }

    getSelectedAdverts(){
        return this.selectedAdvertsStack.getValue();
    }

    deleteAdv(prc_ID:  number){
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find(el=>el.prc_ID == prc_ID);
        if(searchItem){
           let index = existStack.indexOf(searchItem);
           existStack.splice(index,1);
           this.selectedAdvertsStack.next(existStack);
           this.saveData();
        }
    }

    public put(item, id = null){
        let existStack = this.selectedAdvertsStack.getValue();
        let searchItem = existStack.find(el=>el.prc_ID == item.prc_ID);
        if(!searchItem){
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
            this.lastDepartmentId = id !== null ? id : item.id;
        }else{
            existStack.splice(existStack.indexOf(searchItem),1);
        }
        this.selectedAdvertsStack.next(existStack);
        this.saveData();
    }


    public clearData(){
        this.selectedAdvertsStack.next([]);
    //    localStorage.removeItem('moveAdverts');
    }

    public cancel(){
        this.clearData();
        this.moveMode = false;
    }

    public selectAll(){
        let existStack = [];
        this.stackOfAllAdverts.forEach((item)=>{
            if(this.shared.isAdmin || (this.shared.isAdvOwner(item.creater_ID)))
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        });
        if(this.stackOfAllAdverts && this.stackOfAllAdverts.length) {
            this.lastDepartmentId = this.stackOfAllAdverts[0].id;
        }
        this.selectedAdvertsStack.next(existStack);
        this.saveData();
    }

    public move(){
        if(this.selectedAdvertsStack.getValue().length === 0){
            Swal.fire({
                icon: 'warning',
                title: this.local.getTranslate("adv_not_selected","Обьявления для переноса не выбраны!"),
                showConfirmButton: false,
                timer: 2000,
            });
        }else{
            this.router.navigate(['/catalog/move']);
        }
    }

    public saveData(){
       // localStorage.setItem("moveAdverts",JSON.stringify(this.selectedAdvertsStack.getValue()));
    } 
}