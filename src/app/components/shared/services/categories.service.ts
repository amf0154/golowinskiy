import { Injectable } from "@angular/core";
import { CategoryItem } from "../../categories/categories.component";
import { MainService } from "./main.service";
import { BehaviorSubject, Observable } from "rxjs";
import { AuthService } from "./auth.service";
const Swal = require("sweetalert2");
@Injectable({
    providedIn: "root"
})
export class CategoriesService {
    private _mainCategories$: BehaviorSubject<CategoryItem[]> = new BehaviorSubject([]);
    private _userCategories$: BehaviorSubject<CategoryItem[]> = new BehaviorSubject([]);
    private _createCategories$: BehaviorSubject<CategoryItem[]> = new BehaviorSubject([]);
    public mainCategories$ = this._mainCategories$.asObservable();
    public userCategories$ = this._userCategories$.asObservable();
    public createCategories$ = this._createCategories$.asObservable();
    private _isCategoriesLoaded = new BehaviorSubject<boolean>(true);
    constructor(private mainService: MainService, private authService: AuthService) {}

    async fetchCategoriesAll(idPortal, userId?: string, advertId?: string) {
        this._isCategoriesLoaded.next(true);
        return new Promise((resolve) => {
            this.mainService.getCategories(idPortal, userId, advertId).subscribe(
                (res) => {
                    this._mainCategories$.next(res);
                    resolve(res);
                    this._isCategoriesLoaded.next(false);
                    return res;
                },
                (err) => {
                    this._isCategoriesLoaded.next(false);
                    Swal.fire({
                        position: "center",
                        icon: "warning",
                        title: "Ошибка загрузки каталога!",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            );
        });
    }

    getMainCategories() {
        return this._mainCategories$.asObservable();
    }

    getUserCategories() {
        return this._userCategories$.asObservable();
    }

    async fetchCategoriesUser(idPortal) {
        this._isCategoriesLoaded.next(true);
        return new Promise((resolve) => {
            this.mainService.getCategories(idPortal, this.authService.getUserId()).subscribe((res) => {
                this._userCategories$.next(res);
                resolve(res);
                this._isCategoriesLoaded.next(false);
            });
        });
    }

    fetchCategoriesCreate() {
        this._isCategoriesLoaded.next(true);
        this.mainService.getShopInfo().subscribe(
            (res) => {
                this.mainService.getCategories(res.cust_id, this.authService.getUserId()).subscribe((res) => {
                    this._createCategories$.next(res);
                    this._isCategoriesLoaded.next(false);
                });
            },
            (error) => alert(error.error.message)
        );
    }

    public isCategoriesLoaded(): Observable<boolean> {
        return this._isCategoriesLoaded.asObservable();
    }

    public cachedBreadcrumbs = new Map();
    public getBreadcrumbs(arr, searchId) {
        return new Promise((resolve) => {
            let findId = searchId;
            let breadcrumbs = [];
            findItem(arr);
            function findItem(obj) {
                obj.forEach((element) => {
                    if (element.id === findId) {
                        let el = element;
                        // delete el.listInnerCat;
                        breadcrumbs.push(el);
                        if (element.parent_id != 0) {
                            findId = element.parent_id;
                            findItem(arr);
                        } else {
                            resolve(breadcrumbs.reverse());
                        }
                    }
                    if (element.listInnerCat) {
                        findItem(element.listInnerCat);
                    }
                });
            }
        });
    }

    private cachedHistory = new Map();
    public getBreadcrumbsV2(arr: any[], id: string, isCached = false) {
        let result = [];
        function recursiveSearch(array, targetId, parentObjects) {
            for (var i = 0; i < array.length; i++) {
                if (array[i].id === targetId) {
                    result = parentObjects.concat(array[i]);
                    break;
                } else if (array[i].listInnerCat && array[i].listInnerCat.length > 0) {
                    recursiveSearch(array[i].listInnerCat, targetId, parentObjects.concat(array[i]));
                }
            }
        }

        if (isCached && !result.length) {
            if (this.cachedHistory.has(id)) {
                result = this.cachedHistory.get(id);
            } else {
                recursiveSearch(arr, id, []);
                this.cachedHistory.set(id, result);
            }
        } else {
            recursiveSearch(arr, id, []);
        }

        return result;

    }
}
