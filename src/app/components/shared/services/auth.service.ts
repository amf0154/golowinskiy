import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { Observable, BehaviorSubject, of } from "rxjs"
import { tap } from 'rxjs/operators'
import { Router } from "@angular/router";
import { OrderService } from './order.service';
import { EnvService } from "src/app/env.service"
import { EncoderForToken } from '@components/shared/helpers/jwt-encoder';
import { TokenService } from "./token.service";
import { UserInfo } from "src/app/interfaces/user-info";
import { DictionaryService } from "./dictionary.service";
const punycode = require('punycode');
const Swal = require('sweetalert2');

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private encoder = new EncoderForToken();
    public isAuthenticatedBehaviorSubject = new BehaviorSubject<boolean>(this.tokenService.isTokenExpired());
    public superAdmin = new BehaviorSubject(!!sessionStorage.getItem('superadmin'));
    public userInfo = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('userInfo')));
    public isAdmin = new BehaviorSubject<boolean>(false);
    constructor(
        private http: HttpClient,
        private router: Router,
        private tokenService: TokenService,
        private dictionary: DictionaryService,
        private orderService: OrderService,
        private env: EnvService
        ){}

    public user = {
        Cust_ID_Main: null,
    }

    public isSuperAdmin(): Observable<boolean>{
        return this.superAdmin.asObservable()
    }

    public setSuperAdminStatus(status = false){
        if(!status){
            sessionStorage.removeItem("superadmin");
        }
        this.superAdmin.next(status);
    }


    registration(shopId: string,body){
        this.user.Cust_ID_Main = shopId;
        return this.http.put(`${this.env.apiUrl}/api/Authorization`, {Cust_ID_Main: shopId, ...body})
    }

    public getPortal() {
        if(localStorage.getItem("cust_id")){
            return punycode.toUnicode(localStorage.getItem("cust_id"));
        }else{
            const shopName = this.env.shopName;
            if(shopName && shopName.trim().length !== 0){
              return shopName
            }else{
              const host = punycode.toUnicode(location.hostname.split('.')[0])
              if(host)
                return this.env.exceptDomains.includes(host) ? this.env.defaultShopId : host;
              else
                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'Не удается определить ID магазина!',
                    showConfirmButton: true,
                });
            }
       }
    };

    login(shopId: string, body): Observable<{
        accessToken: string,
        userId: string,
        role: string,
        fio: string,
        userName: string,
        phone: string,
        whatsApp: string,
        skype: string,
        email: string,
        address: string,
    }>{
        this.user.Cust_ID_Main = shopId;
        return this.http.post<{
            accessToken: string,
            userId: string,
            role: string,
            fio: string,
            userName: string,
            phone: string,
            whatsApp: string,
            skype: string,
            email: string,
            address: string,

        }>(`${this.env.apiUrl}/api/Authorization`, {
            cust_ID_Main: shopId, 
            ...body
        }).pipe(
                tap(
                    (data) => {
                        const {
                            accessToken, userId, role, whatsApp, 
                            skype, fio, email, userName, phone, address
                        } = data;
                        localStorage.setItem('token', accessToken)
                        localStorage.setItem('fio', fio)
                        localStorage.setItem('email', email)
                        localStorage.setItem('userName', userName)
                        localStorage.setItem('phone', phone),
                        localStorage.setItem('userInfo',JSON.stringify(data));
                        this.setToken(accessToken);
                        this.isAuthenticatedBehaviorSubject.next(true);
                        this.userInfo.next(data);
                        if(shopId == userId)
                            this.isAdmin.next(true);
                    }
                )
            )
    }

    updateProfile(parameters: {
        f: string,
        Email: string,     
        Phone: Number,
        WhatsApp: string,
        Skype: string,
        Password: string,
        Cust_ID: any;
    }){
        return this.http.put(`${this.env.apiUrl}/api/Authorization/UpdateUser`, parameters);
    }

    get getUserData(): Observable<UserInfo>{
        return this.userInfo.asObservable();
    }

    get getUserData2(): Observable<UserInfo>{
        return this.userInfo;
    }

    recovery(body){
        return this.http.post(`${this.env.apiUrl}/api/password`, body)
    }

    private userInfoHistory = new Map()
    getUserInfo(userId = this.getUserId()): Observable<UserInfo>{
        const historyKey = JSON.stringify(userId);  
        return this.userInfoHistory.has(historyKey) ? of<UserInfo>(this.userInfoHistory.get(historyKey)) : 
        this.http.get<UserInfo>(`${this.env.apiUrl}/api/UserInfo/${userId}`).pipe(tap((data)=>{
            if(!localStorage.getItem('isShowMenuRepeat') && data.isShowMenuRepeat){
                localStorage.setItem('isShowMenuRepeat', '1');
            }
            if(localStorage.getItem('isShowMenuRepeat') && !data.isShowMenuRepeat){
                localStorage.removeItem('isShowMenuRepeat');
            }
            this.userInfo.next(data);
            this.env.userInfo = data;
            if(!this.userInfoHistory.has(historyKey)){
                this.userInfoHistory.set(historyKey,data);
            }  
        }));
    }
    clearUserHistory(){
        this.userInfoHistory.clear()
    }

    

    getUserId() {
        if(localStorage.getItem('token')){
            return this.encoder.getEncodeToken(localStorage.getItem('token')).user_id;
        }else{
            return null;
        }    
    }

    setToken(token: string){
        localStorage.setItem('token', token)
    }

    getToken(): string{
        return localStorage.getItem('token')
    }

    isAuthenticated(): boolean{
        return this.tokenService.isTokenExpired();
    }
    isAuthenticatedObservable(): Observable<boolean>{
        return this.isAuthenticatedBehaviorSubject.asObservable()
    }
    isAdminObservable(): Observable<boolean>{
        return this.isAdmin.asObservable()
    }

    clearLocalStorage(){
        this.dictionary.changeLanguage(localStorage.getItem('defaultLocale'))
        localStorage.removeItem('token');
        localStorage.removeItem('fio');
        localStorage.removeItem('email');
        localStorage.removeItem('userName');
        localStorage.removeItem('phone');
        localStorage.removeItem('userInfo');
        localStorage.removeItem('cart');
        localStorage.removeItem('mainLogo');
        localStorage.removeItem('personalLogo');
        localStorage.removeItem('audioRepeats');
        this.clearUserHistory();
    }

    logout(){
        this.isAuthenticatedBehaviorSubject.next(false);
        this.clearLocalStorage()
        this.isAdmin.next(false);
        this.orderService.clearCartAndOrder();
        this.userInfo.next({});
        this.router.navigate(['/'])
    }
    logoutUserOnly(){
        this.isAuthenticatedBehaviorSubject.next(false);
        localStorage.removeItem('token');
      //  localStorage.removeItem("other_cust_id");
    }
}
