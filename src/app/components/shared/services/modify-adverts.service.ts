import { Injectable } from "@angular/core"
import { ProductService } from "src/app/services/product.service";
import { Enums } from "../models/enums";
import { AuthService } from "./auth.service";
import { DictionaryService } from "./dictionary.service";
import { MainService } from "./main.service";
import { NotifyService } from "./notify.service";
const Swal = require('sweetalert2');
@Injectable({
    providedIn: 'root'
})
export class ModifyAdvertsService{

    public modifyMode: boolean = false;
    public fastEditKey: number = 0; // 1 - title; 2 - audio;

    get getModifyIcon(){
        switch(this.fastEditKey){
            case 1: {
                return 'fa-edit'
                break;
            }
            case 2: {
                return 'fa-microphone'
                break;
            }
            default: ''
        }
    }
    public portalId: any = null;
    private selectedAdvert = null;
    private confirmButtonHTML(showSpinner = false){
        return  (
            `<div style="display: flex; align-items: center;">
                <div>Переименовать&nbsp;</div>
                ${showSpinner ? `
                    <div class="spinner-border spinner-border-sm" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>` : 
                ''}
            </div>`
        );
    }
    get confirmButton(): HTMLButtonElement{
        return document.getElementsByClassName("swal2-confirm")[0] as HTMLButtonElement
    }
    constructor(
        private mainService: MainService,
        private notify: NotifyService,
        public local: DictionaryService,
        public product: ProductService,
        private authService: AuthService){
        this.portalId = this.authService.getPortal();
    }

    public fastEdit(key: Enums.ModifyAdvertType = 0){
        this.modifyMode = !this.modifyMode;
        // if(this.modifyMode){
        //   this.fastEditKey = key;
        // }else{
        //   this.fastEditKey = 0;
        // }
        if(this.modifyMode){
            switch(key){
                case Enums.ModifyAdvertType.EDIT_TITLE: {
                    this.fastEditKey = Enums.ModifyAdvertType.EDIT_TITLE;
                    break;
                };
                case Enums.ModifyAdvertType.EDIT_AUDIO: {
                    this.fastEditKey = Enums.ModifyAdvertType.EDIT_AUDIO;
                    break;
                };
                case Enums.ModifyAdvertType.EDIT_VIDEO: {
                    this.fastEditKey = Enums.ModifyAdvertType.EDIT_VIDEO;
                    break;
                }; 
                default: null;              
            };
        }else{
            this.fastEditKey = Enums.ModifyAdvertType.DISABLE;
        };
    };

    public modifyById(item){
        Swal.fire({
            title: 'Переименовать обьявление',
            text: item.tName,
            html:   `<input id="swal-input1" `+
                    `placeholder="новое имя обьявления" `+
                    `value='${item.tName}' class="swal2-input">`,
            showCancelButton: true,
            confirmButtonText: 'Переименовать',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            didOpen: () => {
                document.getElementById("swal-input1").focus();
                this.confirmButton.innerHTML = this.confirmButtonHTML(true);
                this.confirmButton.disabled = true;
            },
            preConfirm: () => {
                const name = (document.getElementById("swal-input1") as any).value;
                return this.saveAdvert(name)
                .then((response: any) => {
                    if(response.result)
                        item.tName = name;
                    return response;
                })
                .catch(error => {
                    Swal.showValidationMessage(
                    `Request failed: ${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
            if (result.value && result.value.result) {
                Swal.fire({
                    icon: 'success',
                    title: "Обьявление успешно переименовано!",
                    timer: 1000,
                    });
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: "Обьявление не было переименовано!",
                    timer: 700,
                    }); 
            }
        });
        this.mainService.getProduct(
            item.prc_ID, 
            this.mainService.getCustId(), 
            this.mainService.getCustId()
        ).subscribe((advert: any) => {
            this.selectedAdvert = advert;
            this.mainService.retailPrice(item.prc_ID).subscribe((retail: {retail: number,isChange:boolean})=>{
                this.selectedAdvert.retail = retail.retail;
                this.confirmButton.disabled = false;
                this.confirmButton.innerHTML = this.confirmButtonHTML()
            });
        });
    }

    public async saveAdvert(name){
        const {id, ctlg_Name,ctlg_No, tName, tDescription, t_imageprev, youtube, mediaLink, retail} = this.selectedAdvert;
        const preparedObject = {
            "Catalog": this.mainService.getCustId(),
            "Id": id,
            "Ctlg_Name": ctlg_Name,
            "TArticle": ctlg_No,
            "TName": name, //  name
            "TDescription":  tDescription,
            "TCost": retail,
            "TImageprev": t_imageprev,
            "Appcode": this.mainService.getCustId(),
            "TypeProd":"",
            "PrcNt":"",
            "TransformMech":"",
            "CID": this.authService.getUserId(),
            "video": youtube,
            "audio": mediaLink
        };
        return await this.mainService.editProduct(preparedObject).toPromise();
    }



    saveFastModify(product,newName,editedAudioFile){
        const {prc_ID} = product;
        const sendToSave = (data) =>{
         this.mainService.editAdvNameOrAudio(data)
         .subscribe((result:any)=> {
            if(result[0]){
                console.log(result[0])
                this.notify.showNotify(
                    this.local.getTranslate("adv_updated",`Обьявление успешно обновлено`),
                    newName,
                    'success'
                );
            }
         });
       }
       this.mainService.getProduct(
         prc_ID, 
         this.mainService.getCustId(), 
         this.mainService.getCustId()
       ).subscribe((advert: any) => {
           let selectedAdvert = advert;
         //  this.audio = advert.mediaLink && advert.mediaLink.length > 10 ? advert.mediaLink : null;
               if(editedAudioFile === null){
                 selectedAdvert.mediaLink = "";
               }
               const preparedObject = {
                "cust_ID_Main": this.mainService.getCustId(),
                "prc_ID": prc_ID,
                "tName": newName,
                "mediaLink": null
              }
             if(editedAudioFile){ 
                 this.mainService.uploadFileAws(editedAudioFile).subscribe((res: any)=>{
                   const link = "https://"+ res.data;
                   preparedObject.mediaLink = link;
                   product.mediaLink = link;
                   this.product.updItem({
                    "prc_ID": prc_ID,
                    "mediaLink": link
                   });
                   sendToSave(preparedObject)
                 },()=>{
                   sendToSave(preparedObject)
                 })
               }else{
                 sendToSave(preparedObject)
               }
         }); 
   
    }


    public saveNewAudio(prc_ID, tname, audiolink){
        const preparedObject = {
            "cust_ID_Main": this.mainService.getCustId(),
            "prc_ID": prc_ID,
            "tName": tname,
            "mediaLink": audiolink
          }
        return this.mainService.editAdvNameOrAudio(preparedObject);
    }
   
}