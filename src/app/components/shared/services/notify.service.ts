import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
// export declare type MatSnackBarHorizontalPosition = 'start' | 'center' | 'end' | 'left' | 'right';
// /** Possible values for verticalPosition on MatSnackBarConfig. */
// export declare type MatSnackBarVerticalPosition = 'top' | 'bottom';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {
  constructor(
    private _snackBar: MatSnackBar,
  ) { }

  showNotify(
    message: string,
    msgRight: string = "",
    status: string = "success", // or error
    duration : number = 2500,
    hPosition: MatSnackBarHorizontalPosition = 'right',
    vPosition: MatSnackBarVerticalPosition = 'top'
  ){
    this._snackBar.open(message, msgRight, {
      horizontalPosition: hPosition,
      verticalPosition: vPosition,
      panelClass: status == "success" ? 
      ['success-snackbar']  : 
      status == "warning" ? 
      ['warning-snackbar'] :
      ['error-snackbar'],
      duration: duration,
  });
  }
}
