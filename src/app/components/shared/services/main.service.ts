import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { forkJoin, from, Observable, of, BehaviorSubject, Observer } from "rxjs";
import { catchError, map, mergeMap, switchMap, tap } from "rxjs/operators";
import { CategoryItem } from "../../categories/categories.component";
import { AdditionalImagesData, AdditionalImagesRequest, DeleteProduct, ShopInfo } from "../interfaces";
import { OrderService } from "./order.service";
import { CommonService } from "./common.service";
import { EnvService } from "src/app/env.service";
import { throwError } from "rxjs/internal/observable/throwError";
//import { Dropbox } from 'dropbox';
import { EncoderForToken } from "@components/shared/helpers/jwt-encoder";
import { StateService } from "./state.service";
import { AdvertDetail } from "../models/advert-list";
import { GalleryItem, MoveAdverts, MoveAdvertsResp } from "src/app/interfaces/adverts";
import { CopiedLink } from "src/app/interfaces/copied-link";
import { Models } from "../models/models";
import { SpinnerService } from "./spinner.service";
import { Enums } from "../models/enums";
const Swal = require("sweetalert2");
const Dropbox = null;
@Injectable({
    providedIn: "root"
})
export class MainService {
    public idPortal: string = null;
    public cust_id = null;
    private encoder = new EncoderForToken();
    public currentPage: number = 1;
    public shopInfo: ShopInfo = {} as ShopInfo;
    constructor(
        private http: HttpClient,
        private orderService: OrderService,
        public commonService: CommonService,
        private spinner: SpinnerService,
        private state: StateService,
        private env: EnvService
    ) {}

    private bodyToQuery = (params) =>
        Object.keys(params)
            .map((key) => key + "=" + params[key])
            .join("&");
    public setCustId(cust_id: string) {
        localStorage.setItem("cust_id", cust_id);
        this.cust_id = cust_id;
    }

    public getCustId(): string {
        return this.cust_id ? this.cust_id : localStorage.getItem("cust_id");
    }

    public getUserId() {
        return this.http.get(`${this.env.apiUrl}/api/Load/${this.getCustId()}`);
    }

    public getUserIdFromToken(): string {
        if (localStorage.getItem("token")) return this.encoder.getEncodeToken(localStorage.getItem("token")).user_id;
    }

    public setShopInfo(data: ShopInfo) {
        //  this.shopInfo = data;
        const {
            cust_id,
            mainImage,
            mainPictureAccountUser,
            dz,
            email,
            phone,
            shortDescr,
            addr,
            welcome,
            manual,
            isEditing
        } = data;
        this.shopInfo.cust_id = cust_id;
        this.shopInfo.mainImage = mainImage;
        this.shopInfo.mainPictureAccountUser = mainPictureAccountUser;
        this.shopInfo.dz = dz;
        if (email && email.length != 0) this.shopInfo.email = email;
        if (phone && phone.length != 0) this.shopInfo.phone = phone;
        if (shortDescr && shortDescr.length != 0) this.shopInfo.shortDescr = shortDescr;
        if (addr && addr.length != 0) this.shopInfo.addr = addr;
        if (welcome && welcome.length != 0) this.shopInfo.welcome = welcome;
        if (manual && manual.length != 0) this.shopInfo.manual = manual;
        //  this.shopInfo.isEditing = isEditing;
    }

    public updateCategory(body: { Id: number; Name: string; ImgName: string; CustIdMain: number }): Observable<any> {
        return this.http.put(`${this.env.apiUrl}/api/Catalog`, body).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    public addNewCategory<T>(body: { Id: number; Name: string; ImgName: string; CustIdMain: any }): Observable<T> {
        return this.http.post<T>(`${this.env.apiUrl}/api/Catalog`, body).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    public deleteCategory(body): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                accessToken: "Bearer " + localStorage.getItem("token"),
                id: body.id,
                custIdMain: body.cust_id
            }
        };
        return this.http.delete(`${this.env.apiUrl}/api/Catalog`, options).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    public getAudioLink(prc_ID, order): Observable<any> {
        return this.http
            .post(`${this.env.apiUrl}/api/Media/GetAudioLink`, {
                Prc_ID: prc_ID,
                MediaOrder: order,
                Appcode: this.env.shopInfo.cust_id
            })
            .pipe(
                catchError((error: any) => {
                    return throwError(error);
                })
            );
    }

    public authAdmin(login, password): Observable<any> {
        const preparedBody = {
            password: password.value,
            userName: login.value
        };
        return this.http.post(`${this.env.apiUrl}/api/Admin/Login`, preparedBody).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    public async deleteCategoryAsync(body) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                id: body.id,
                custIdMain: body.cust_id
            }
        };
        return await this.http.delete(`${this.env.apiUrl}/api/Catalog`, options).toPromise();
    }

    public async deleteNotEmptyCategoryAsync(body) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                id: body.id,
                custIdMain: body.cust_id
            }
        };
        return await this.http.delete(`${this.env.apiUrl}/api/Catalog/DeleteConfirmed`, options).toPromise();
    }

    getShopInfo() {
        return new Observable<ShopInfo>((observer: Observer<ShopInfo>) => {
            this.state.getShopInfo().then((res: ShopInfo) => {
                this.setShopInfo(res);
                this.setCustId(res.cust_id);
                observer.next(res);
            });
        });
    }

    getShopIdByName(name) {
        return this.http.get<ShopInfo>(`${this.env.apiUrl}/api/shopinfo/${name}`);
    }

    getShopDetails(name) {
        return this.http.get<ShopInfo>(`${this.env.apiUrl}/api/shopdetails/${name}`);
    }

    getFonPictures(img?) {
        const fontElement = document.getElementById("fon-image");
        if (fontElement) {
            if (window.location.pathname.includes("cabinet")) {
                const link = img ? img : this.backgroundPersonalWallpaper;
                fontElement.style.background = `url(${link}) no-repeat`;
            } else {
                const link = img ? img : this.backgroundMainWallpaper;
                fontElement.style.background = `url(${link}) no-repeat`;
            }
        }
    }

    getErrorFonPicture() {
        if (document.getElementById("fon-image")) {
            document.getElementById("fon-image").style.background = "url('/assets/images/fon2.jpg') no-repeat 50% 50%";
        }
    }

    addSubscription(data) {
        return this.http.post<{
            AppCode: string;
            CID: string;
            ID: string;
            dellAll: boolean;
            Mark: Boolean;
        }>(`${this.env.apiUrl}/api/Notification`, data);
    }

    private categoriesHistory = new Map();
    getCategories(idPortal: any, userId?: string, advert?: string) {
        let prepatedObject = {
            cust_ID_Main: idPortal,
            cust_Id: this.getUserIdFromToken(),
            cid: userId,
            advert: advert
        };
        if (!prepatedObject.advert) delete prepatedObject.advert;
        const historyKey = JSON.stringify(prepatedObject);
        return this.categoriesHistory.has(historyKey)
            ? of(this.categoriesHistory.get(historyKey))
            : this.http.post<CategoryItem[]>(`${this.env.apiUrl}/api/categories`, prepatedObject).pipe(
                  tap((data) => {
                      if (!this.categoriesHistory.has(historyKey)) {
                          this.categoriesHistory.set(historyKey, data);
                      }
                  })
              );
    }

    clearCategoriesHistory() {
        this.categoriesHistory.clear();
    }

    getProducts(id, cust_id, userId): Observable<Array<GalleryItem>> {
        return this.http.post<Array<GalleryItem>>(`${this.env.apiUrl}/api/Gallery/`, {
            Cust_ID: cust_id,
            ID: id === "null" ? null : id,
            Client_ID: this.getUserIdFromToken(),
            CID: userId
        });
    }

    getMaterial(type: Enums.CatalogMode): Observable<Array<GalleryItem>> {
        return this.http.post<Array<GalleryItem>>(`${this.env.apiUrl}/api/Gallery/`, {
            Cust_ID: this.getCustId(),
            Client_ID: this.getUserIdFromToken(),
          //  item : 1,
            isSearchPageVariant: type
        });
    }

    search(word): Observable<any> {
        return this.http.post(`${this.env.apiUrl}/api/Gallery/`, {
            Cust_ID: this.getCustId(),
            Client_ID: this.getUserIdFromToken(),
            ID: null,
            searchDescr: word
        });
    }
    searchDetailPage(id, cust_id, userId, word): Observable<any> {
        return this.http.post(`${this.env.apiUrl}/api/Gallery/`, {
            Cust_ID: cust_id,
            CID: userId,
            Client_ID: this.getUserIdFromToken(),
            //  ID: id === 'null' ? null : id,
            searchDescr: word
        });
    }

    getProductsPaginate(
        sortByCatalog: boolean,
        id,
        cust_id,
        userId,
        itemsPerPage: number = 12,
        currentPage: number = 1
    ): Observable<any> {
        return this.http.post(
            `${this.env.apiUrl}/api/${
                sortByCatalog ? "GalleryPagination" : "AllGalleryPagination"
            }?itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`,
            {
                Cust_ID: cust_id,
                ID: id,
                CID: userId
            }
        );
    }

    getProduct(prc_ID: any, cust_id, appCode) {
        return this.http.post(`${this.env.apiUrl}/api/Img`, {
            prc_ID: prc_ID,
            cust_ID: cust_id,
            appCode: appCode
        });
    }

    createImageAdvert(body: any): any {
        return this.http.post(`${this.env.apiUrl}/api/Product/CreateFromExtraImages`, body);
    }

    getProductWithAudio(prc_ID: any, cust_id, appCode, userId): Observable<AdvertDetail> {
        return this.http
            .post<AdvertDetail>(`${this.env.apiUrl}/api/Img`, {
                prc_ID: prc_ID,
                cust_ID: cust_id,
                appCode: appCode
            })
            .pipe(
                switchMap((data: AdvertDetail) =>
                    data.additionalImages.length ? this.getAttachedAudioImg(data, appCode, userId) : of(data)
                )
            );
    }

    private prepareProtocolLink(link) {
        return link && link.startsWith("https://") ? link : link ? "https://" + link : null;
    }

    getManual(data: Models.ManualParamsGet): Observable<Models.ManualResponse> {
        return this.http
            .get<Models.ManualResponse>(`${this.env.apiUrl}/api/Manual/GetText?` + this.bodyToQuery(data))
            .pipe(
                tap(
                    (res) => {
                        Swal.fire({
                            title: "<strong>Инструкция</strong>",
                            html: res.manual,
                            customClass: "swal-wide",
                            showCloseButton: true,
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Понятно',
                            confirmButtonAriaLabel: "Thumbs up, great!"
                        });
                    },
                    () => this.spinner.display(false),
                    () => this.spinner.display(false)
                )
            );
    }

    getAttachedAudioImg(advData: AdvertDetail, appCode, userId) {
        const getAudioByOrder = (orderId, audios) => {
            const content = audios.find((a) => a.mediaOrder == orderId);
            return {
                audio: content ? this.prepareProtocolLink(content.mediaFileName) : null,
                video: content ? this.prepareProtocolLink(content.youtubeLink) : null
            };
        };
        const preparedBody = advData.additionalImages.map((_, i) => ({
            prc_ID: advData.prc_ID,
            mediaOrder: _.imageOrder,
            appcode: appCode,
            cid: userId
        }));
        return this.http.post(`${this.env.apiUrl}/api/Media/GetFileLink`, { mediaFiles: preparedBody }).pipe(
            map((audios: Array<any>) => {
                const existedAudio = audios.filter((a) => a);
                advData.additionalImages = advData.additionalImages.map((img) => ({
                    ...img,
                    ...getAudioByOrder(img.imageOrder, existedAudio)
                }));
                return advData;
            })
        );
    }

    getImageByLink(imageUrl: string): Observable<Blob> {
        return this.http.get((imageUrl += "?" + new Date().getTime()), { responseType: "blob" });
    }

    uploadBackground(data: any) {
        return this.http.post(`${this.env.apiUrl}/api/Background`, data);
    }

    uploadFileAws<T>(data: FormData): Observable<T> {
        return this.http.post<T>(`${this.env.apiUrl}/api/FileS3/UploadFile?shopId=${this.getCustId()}`, data).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    deleteCloudContent(name: String) {
        const link = name.includes("https://") ? name.split("https://")[1] : name;
        return this.http.delete(`${this.env.apiUrl}/api/FileS3/DeleteImage?file=${link}`);
    }

    deleteMediaLink(prc_ID) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                prc_ID: prc_ID,
                appCode: this.getCustId()
            }
        };
        return this.http
            .delete(`${this.env.apiUrl}/api/Product/MediaLink`, options)
            .pipe(
                map((response) => {
                    return response;
                }),
                catchError((error: any) => {
                    return throwError(error);
                })
            )
            .subscribe();
    }

    retailPrice(prc_ID) {
        return this.http.post(`${this.env.apiUrl}/api/Product/RetailPrice`, {
            prc_ID: prc_ID,
            cust_ID_Main: this.env.shopInfo.cust_id
        });
    }

    public backgroundMainWallpaper: string = localStorage.getItem("mainLogo");
    public backgroundPersonalWallpaper: string = localStorage.getItem("personalLogo");
    public downloadBackgroundPersonal<T>(cust_id, orientation: string = "H"): Observable<T> {
        return this.http
            .get<T>(
                `${this.env.apiUrl}/api/Background/file?appCode=${cust_id}&mark=0&orientation=${orientation}&place=L`
            )
            .pipe(
                map((response: any) => {
                    const url = response.length !== 0 ? "https://" + response[0].fileName : null;
                    this.backgroundPersonalWallpaper = this.checkBackgroundLink(url, orientation === "V");
                    localStorage.setItem("personalLogo", this.backgroundPersonalWallpaper);
                    this.getFonPictures();
                    return response;
                }),
                catchError((error: any) => {
                    return throwError(error);
                })
            );
    }

    private checkBackgroundLink(url, isMobile = false) {
        const noImage = isMobile ? this.env.noImgMobile : this.env.noImgDesktop;
        return url && url.includes("cloudfront") ? url : noImage;
    }

    public downloadBackgroundMain<T>(cust_id, orientation: string = "H"): Observable<T> {
        return this.http
            .get<T>(
                `${this.env.apiUrl}/api/Background/file?appCode=${cust_id}&mark=0&orientation=${orientation}&place=G`
            )
            .pipe(
                map((response: any) => {
                    const url = response.length !== 0 ? "https://" + response[0].fileName : null;
                    this.backgroundMainWallpaper = this.checkBackgroundLink(url, orientation === "V");
                    localStorage.setItem("mainLogo", this.backgroundMainWallpaper);
                    this.getFonPictures(this.backgroundMainWallpaper);
                    return response;
                }),
                catchError((error: any) => {
                    return throwError(error);
                })
            );
    }

    public loadBackgrounds({ cust_id }) {
        if (
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
                navigator.userAgent
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                navigator.userAgent.substr(0, 4)
            )
        ) {
            this.downloadBackgroundMain(cust_id, "V").subscribe();
            this.downloadBackgroundPersonal(cust_id, "V").subscribe();
        } else {
            this.downloadBackgroundMain(cust_id).subscribe();
            this.downloadBackgroundPersonal(cust_id).subscribe();
        }
    }

    deleteBackgrounds(data) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            }),
            body: data
        };
        return this.http.delete(`${this.env.apiUrl}/api/Background`, options);
    }

    uploadImage(data: any) {
        return this.http.post(`${this.env.apiUrl}/api/img/upload/`, data);
    }
    uploadImageXHR(data: any, i?, callback?) {
        return this.postImagesXHR(`${this.env.apiUrl}/api/img/upload/`, data, i, callback);
    }

    uploadImageXhrCloud(data: any, i?, callback?) {
        return this.postImagesXHR(
            `${this.env.apiUrl}/api/FileS3/UploadImage?shopId=` + this.getCustId(),
            data,
            i,
            callback
        );
    }

    private ACCESS_DROPBOX_TOKEN: string = "v1YGzbkFmd4AAAAAAAAAAdTt3Nwbf9UWErseCF9ae08K8kJfLGOtaLCciCPpCOmD";
    async uploadDropbox(file, prc_id) {
        let dbx = new Dropbox({ accessToken: this.ACCESS_DROPBOX_TOKEN });
        return dbx.filesUpload({ path: "/" + "file" + "_" + prc_id, contents: file });
    }

    async downloadDropbox(prc_id) {
        let dbx = new Dropbox({ accessToken: this.ACCESS_DROPBOX_TOKEN });
        return await dbx.filesDownload({ path: "/" + "file" + "_" + prc_id });
    }

    sizeCounter: number = 0;
    postImagesXHR(url, data, i?, callback?) {
        return from(
            new Promise((res, rej) => {
                const xhr = new XMLHttpRequest();
                xhr.open("POST", url);
                xhr.upload.onprogress = (event) => callback(i, event.loaded, event.total, xhr);
                xhr.upload.onerror = () => rej(xhr);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.response.length != 0) {
                            res(JSON.parse(xhr.response));
                        }
                    }
                };
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
                xhr.send(data);
            })
        );
    }

    uploadImageGroup(data: FormData[]) {
        return from(data).pipe(mergeMap((item) => this.uploadImage(item)));
    }

    addProduct(data: any, headers) {
        return this.http.post(`${this.env.apiUrl}/api/product`, data, headers).pipe(
            tap(() => {
                this.clearCategoriesHistory();
            })
        );
    }

    updAudioImage(
        data: Array<{
            prc_ID: any;
            mediaOrder: number;
            mediaLink: string;
            appcode: string;
            cid: any;
            youtubeLink?: string;
        }>
    ) {
        const audiosImgBody = {
            mediaFiles: data
        };
        return this.http.post(`${this.env.apiUrl}/api/Media/AddFile`, audiosImgBody);
    }

    setCopiedAudio(data: CopiedLink) {
        return this.http.post(`${this.env.apiUrl}/api/Media/SetLink`, data);
    }

    deleteAudioImgLink(
        data: Array<{
            prc_ID: any;
            mediaOrder: number;
            mediaLink: string;
            appcode: string;
            cid: any;
        }>
    ) {
        const getOptions = () => {
            return {
                headers: new HttpHeaders({
                    "Content-Type": "application/json"
                }),
                body: { mediaFiles: data }
            };
        };
        return this.http.delete(`${this.env.apiUrl}/api/Media/DeleteFile`, getOptions());
    }

    deleteProduct(data: DeleteProduct) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            }),
            body: data
        };
        return this.http.delete(`${this.env.apiUrl}/api/Product/DeleteWithImages`, options).pipe(
            tap(() => {
                this.clearCategoriesHistory();
            })
        );
    }

    deleteProductAsync(data: DeleteProduct) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            }),
            body: data
        };
        return this.http.delete(`${this.env.apiUrl}/api/product`, options).toPromise();
    }

    editProduct(data: any) {
        return this.http.put(`${this.env.apiUrl}/api/product`, data);
    }

    public setPause(data: { cust_ID: string | number; mark: boolean }): Observable<any> {
        return this.http.put(`${this.env.apiUrl}/api/UserInfo/SetPause`, data);
    }

    editAdvNameOrAudio(data: any) {
        return this.http.put(`${this.env.apiUrl}/api/Product/Rename`, { mediaFiles: [data] });
    }

    editAdditionalImg(data: any) {
        return this.http.put(`${this.env.apiUrl}/api/AdditionalPicture/Update`, { pictures: [data] });
    }
    deleteAdditionalImg(data: any) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            }),
            body: {
                files: [data]
            }
        };
        return this.http.delete(`${this.env.apiUrl}/api/AdditionalPicture/Delete`, options).subscribe();
    }

    additionalImagesGroup(data: AdditionalImagesData[]) {
        return from(data).pipe(mergeMap((item) => forkJoin(this.uploadImage(item.imageData))));
    }
    uploadImagesArray(data: any) {
        return from(data).pipe(mergeMap((item) => this.uploadImage(item)));
    }
    additionalImagesArray(data: any) {
        return from(data).pipe(mergeMap((item: any) => this.additionalImageUpload(item.request)));
    }

    moveToMainAdditionalPicture(prc_id, appCode) {
        this.http
            .put(`${this.env.apiUrl}/api/AdditionalPicture/MoveToMain`, {
                appCode: appCode,
                prc_ID: prc_id
            })
            .subscribe();
    }

    additionalImageUpload(additionalData: AdditionalImagesRequest) {
        return this.http.post(`${this.env.apiUrl}/api/AdditionalPicture/Create`, { pictures: [additionalData] });
    }

    changeQty(data: {}) {
        return this.http.post(`${this.env.apiUrl}/api/order/changeqty/ `, data);
    }

    saveOrder(data: {}, headers) {
        return this.http.post(`${this.env.apiUrl}/api/order/save/ `, data, headers);
    }

    registerOrder() {
        return this.getUserId().pipe(
            switchMap((res) => this.http.post(`${this.env.apiUrl}/api/order/`, { Cust_ID: res, Cur_Code: 810 })),
            switchMap((res: any) => {
                localStorage.setItem("ord_No", res.ord_No);
                this.orderService.setOrderId(res.ord_ID);
                return of(res);
            })
        );
    }

    addToCart(data) {
        return this.http.post(`${this.env.apiUrl}/api/addtocart`, data);
    }

    doMultiCart(data) {
        return this.http.post(`${this.env.apiUrl}/api/Product/CopyToCatalogs`, data).pipe(
            tap(() => {
                this.clearCategoriesHistory();
            })
        );
    }
    doRepeat(data) {
        return this.http.post(`${this.env.apiUrl}/api/Product/Recopy`, data);
    }

    doCopyCatalog(data) {
        return this.http.post(`${this.env.apiUrl}/api/Category/Copy`, data).pipe(
            tap(() => {
                this.clearCategoriesHistory();
            })
        );
    }

    moveAdverts(data: MoveAdverts): Observable<MoveAdvertsResp> {
        return this.http.put<MoveAdvertsResp>(`${this.env.apiUrl}/api/Product/MoveToSection`, data);
    }

    makeDoll(data: any): Observable<any> {
        return this.http.post<any>(`${this.env.apiUrl}/api/Product/CopyToAddImage`, data);
    }

    public checkAdmin(data): Observable<any> {
        const [userName, password] = data.split(":");
        const preparedBody = {
            userName: userName,
            password: password
        };
        return this.http.post(`${this.env.apiUrl}/api/Admin/Login`, preparedBody).pipe(
            map((response: { role: string }) => {
                const res = response.role === "superadmin";
                if (!res) {
                    sessionStorage.removeItem("superadmin");
                }
                return res;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }
}
