import { Injectable, OnInit } from "@angular/core";
import { FormControl, ValidationErrors } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { EditPersonalComponent } from "@components/main-page/auth-page/edit-personal/edit-personal.component";
import { LoginComponent } from "@components/main-page/auth-page/login/login.component";
import { EnvService } from "src/app/env.service";
import { PopupMenuComponent } from "../menu-header/popup-menu/popup-menu.component";
import { Enums } from "../models/enums";
import { Models } from "../models/models";
import { AuthService } from "./auth.service";
import { DictionaryService } from "./dictionary.service";
import { MainService } from "./main.service";
import { SpinnerService } from "./spinner.service";
const Swal = require("sweetalert2");
@Injectable({
    providedIn: "root"
})
export class SharedService {
    constructor(
        private authService: AuthService,
        private mainService: MainService,
        public spinner: SpinnerService,
        private route: ActivatedRoute,
        public router: Router,
        public local: DictionaryService,
        private env: EnvService,
        public dialog: MatDialog
    ) {}

    public loadingImage =
        "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";

    emptySpacesValidator(control: FormControl): ValidationErrors {
        const value = control.value;
        if (value && value.trim().length === 0) {
            return { emptySpaces: "Введите корректное название" };
        }
        return null;
    }

    openNewAdvert() {
        const newAdvertLink = this.env.shopDetails.pageKind ? "/addProduct" : "/commodity/new";
        const url = this.router.serializeUrl(this.router.createUrlTree([newAdvertLink]));
        window.open(url, "_blank");
    }

    uuidv4() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    }

    get isAdmin() {
        return this.authService.getUserId() == this.mainService.getCustId();
    }

    public isSlideshowEnabled() {
        return Reflect.has(this.env.shopInfo, "isSlideShow")
            ? this.env.shopInfo.isSlideShow || this.isAdmin
            : this.isAdmin;
    }

    isAdvOwner(ownerId) {
        return this.isAdmin || this.authService.getUserId() == ownerId;
    }

    public showMainButton(): boolean {
        return this.env.userInfo.isHomePageShow || this.env.shopInfo.isWithHomePage; // || this.isAdmin
    }

    public checkIfCloudLink(link) {
        return link.includes("cloudfront") ? "https://" + link : link;
    }

    checkNoImageLink(link) {
        return link.includes("noimage") || !link.includes("cloudfront")
            ? window.location.origin + "/assets/images/no_image.png"
            : "https://" + link;
    }

    openPersonalMenu(type: Enums.UserSettings) {
        //  this.spinner.display(true);
        this.authService.getUserInfo(this.authService.getUserId()).subscribe(
            (data) => {
                this.dialog.open(EditPersonalComponent, {
                    data: {
                        ...data,
                        type: type,
                        route: this.router.url
                    }
                });
            },
            () => this.spinner.display(false),
            () => this.spinner.display(false)
        );
    }

    showMenuAsModal() {
        this.dialog.open(PopupMenuComponent, {
            width: '100%'
          })
          .afterClosed().subscribe(result => {});
    }

    openAuth(path) {
        return this.dialog
            .open(LoginComponent, {
                width: "0px",
                data: {
                    path: path
                }
            })
            .afterClosed()
            .subscribe((r) => {
                if (r && !this.showMainButton()) {
                    setTimeout(() => this.router.navigate(["/cabinet"]), 300);
                }
            });
    }

    manual(route) {
        this.spinner.display(true);
        let type: Enums.ManualType;
        switch (route) {
            case Models.PageRoutes.Detail: {
                type = Enums.ManualType.DETAIL;
                break;
            }
            case Models.PageRoutes.Products: {
                type = Enums.ManualType.MAIN;
                break;
            }
            case Models.PageRoutes.CabinetProducts:
            case Models.PageRoutes.CabinetMain: {
                type = Enums.ManualType.PERSONAL;
                break;
            }
            case Models.PageRoutes.Subscribe: {
                type = Enums.ManualType.SUBSCRIBE;
                break;
            }
            case Models.PageRoutes.CopyList: {
                type = Enums.ManualType.COPYLIST;
                break;
            }
            case Models.PageRoutes.CopyCat: {
                type = Enums.ManualType.COPYCATALOG;
                break;
            }
            default:
                type = Enums.ManualType.MAIN;
        }
        const body = {
            cust_ID_Main: this.mainService.getCustId(),
            itemname: type
        };
        this.mainService.getManual(body).subscribe();
    }

    getParameterByName(name, url = window.location.href) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    saveCredetials(key, obj, del = false): void {
        let str = JSON.stringify(obj);
        if(del && localStorage.getItem(key)) {
            localStorage.removeItem(key);
            return;
        }
        if(!del) {
            localStorage.setItem(key, str);
        }
    };

    getCredentials(key): any {
      return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;  
    }
}
