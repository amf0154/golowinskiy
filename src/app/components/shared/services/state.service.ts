import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core"
import { BehaviorSubject, Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { EnvService } from "src/app/env.service";
import { ShopInfoModel } from "../models/shop-info.model";
import { AuthService } from "./auth.service";
const Swal = require('sweetalert2');
const punycode = require('punycode');
@Injectable({
    providedIn: 'root'
})
export class StateService{
    private shopInfo = new Promise(res => res(null));
    private $shopInfo = new BehaviorSubject(null);
    public currentCatId = new BehaviorSubject(null);
    constructor(
        private http: HttpClient,
        private env: EnvService,
        private authService: AuthService,
    ){}

    public clearShopInfo(){
        this.shopInfo = this.getShopInfoFromApi();
    }

    public setShopInfo(shopInfo){
        this.shopInfo = Promise.resolve(shopInfo);
        this.$shopInfo.next(shopInfo);
    }

    public $getShopInfo(): Observable<ShopInfoModel>{
        return this.$shopInfo.asObservable();
    }

    private async getShopInfoFromApi(val = null) {
        const slug = val ? val : this.authService.getPortal();
        return this.http.get<any>(`${this.env.apiUrl}/api/shopinfo/${slug}`)
        .pipe(tap((data)=>{
            this.$shopInfo.next(data);
            localStorage.setItem("cust_id",data.cust_id);
        }))
        .toPromise()
        .catch((error)=>{
            const message = error && error.error 
            && error.error.message ? error.error.message : null;
            Swal.fire({
                position: 'center',
                icon: 'warning',
                title: message ? message : 
                'Не удается загрузить информацию о магазине, попробуйте позже!',
                showConfirmButton: true,
            });
            throw error;
        });
    };

    public async getShopInfo() {
        const data = await this.shopInfo;
        const result =  data ? data : this.shopInfo = this.getShopInfoFromApi();
       return result;
    };

    public async changeShop(name) {
        return this.shopInfo = this.getShopInfoFromApi(punycode.toUnicode(name))
    };
}


