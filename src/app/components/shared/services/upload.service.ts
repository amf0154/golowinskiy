import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { SharedService } from './shared.service';
import { StateService } from './state.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

 private imgUrl = this.env.apiUrl + '/api/FileS3/UploadImage?shopId='+ this.env.shopInfo.cust_id;
 private filesUrl = this.env.apiUrl + '/api/FileS3/UploadFile?shopId='+ this.env.shopInfo.cust_id;
 
  constructor(
    private http: HttpClient,
    private shared: SharedService,
    private env: EnvService,
    private state: StateService
    ) {}

  // getApiUrlByIndex(index){
  //   switch(index){
  //     case 1: return this.imgUrl;
  //     case 3: return this.additImgAudio;
  //     default: return this.filesUrl; 
  //   }
  // }

  upload(file: File| Blob,filename: string = this.shared.uuidv4(),type = 1): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', new File([file],filename));
    const req = new HttpRequest('POST', `${type === 1 ? this.imgUrl : this.filesUrl}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }
}
