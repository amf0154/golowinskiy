import { Injectable, AfterViewInit } from '@angular/core';
//import * as XLSX from 'xlsx';
import { DictionaryService } from './dictionary.service';
@Injectable({
  providedIn: 'root'
})
export class LocalizeService implements AfterViewInit {
  constructor(
  //  public dictionary : DictionaryService
  private dictionaryService: DictionaryService
  ) {
  }
  public dictionaryRU = new Map();
/*
  public importFromFile(bstr: string): void {
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
    const data = <XLSX.AOA2SheetOpts>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
    console.log(data);
  }

  public importLocalizeConfiguration(file,run){
  //  that = this;
    const oReq = new XMLHttpRequest();
    oReq.open("GET", file, true);
    oReq.responseType = "arraybuffer";
    oReq.onload = function(e) {
      var arraybuffer = oReq.response;
      var data = new Uint8Array(arraybuffer);
      var arr = new Array();
      for(var i = 0; i != data.length; ++i) 
        arr[i] = String.fromCharCode(data[i]);
      run(arr.join(""));
    }
    oReq.send();
  }


  public exportToFile(fileName: string, element_id: string) {
    if (!element_id) throw new Error('Element Id does not exists');

    let tbl = document.getElementById(element_id);
    let wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, fileName + '.xlsx');
  }
  */

 async loadDictionaryBase() {
   return new Promise((res,rej)=>{
    const rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", './assets/localization.json', true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status === 200) {
            res(JSON.parse(rawFile.responseText));
        }
    }
    rawFile.send();
   })
}

//usage:
/*
readTextFile("/Users/Documents/workspace/test.json", function(text){
  var data = JSON.parse(text);
  console.log(data);
});
*/

 public importLocalizeConfiguration(file){
  var data = JSON.parse(file);
  //console.log(data);
 }



 makeDictinary(file){
   const dictionaryRU = new Map();
//   this.dictionaryRU = new Map();
  const data = JSON.parse(file);
 // console.log(data.Sheet1);
 /*
  const lastAlias = data.Sheet1.slice(-1).Alias;
  data.Sheet1.forEach(item => {
    dictionaryRU.set(item.Alias,item.Ru)
    console.log(dictionaryRU)
  });
*/
  const orderPromises = data.Sheet1.map(item => this.dictionaryRU.set(item.Alias,item.Ru));
  Promise.all(orderPromises).then(arrayOfResponses => {
     // console.log(this.dictionaryRU)
    //  window.localStorage.setItem('RU',JSON.stringify(dictionaryRU));
  })
    
 }

 ngAfterViewInit() {

}
  
 
}
