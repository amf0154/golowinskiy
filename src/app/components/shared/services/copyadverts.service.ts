import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { NgbDate } from "@ng-bootstrap/ng-bootstrap";
import { BehaviorSubject } from "rxjs";
import { Location } from "@angular/common";
import { MainService } from "./main.service";
import { EnvService } from "src/app/env.service";
import { AuthService } from "./auth.service";
import { CategoriesService } from "./categories.service";
const Swal = require("sweetalert2");
@Injectable({
    providedIn: "root"
})
export class CopyAdvertsService {
    private copiedAdvertsStack = new BehaviorSubject([]);
    private selectedCategoriesStack = new BehaviorSubject([]);
    private selectedCategoriesForFill = new BehaviorSubject([]);
    public stackOfAllAdverts = [];
    public copyMode: boolean = false;
    public showSpinner: boolean = false;
    public portalId: any = null;
    public showFillCheckbox = true;
    public showDefSheckbox = true;
    public has = (item) => this.copiedAdvertsStack.getValue().find((el) => el.prc_ID == item.prc_ID);
    get count() {
        return this.copiedAdvertsStack.getValue().length;
    }
    constructor(
        private router: Router,
        private _location: Location,
        private env: EnvService,
        private authService: AuthService,
        private cats: CategoriesService,
        private mainService: MainService
    ) {
        //    this.setNewPortalId()
        this.portalId = this.authService.getPortal();
        // let savedItems = localStorage.getItem("copiedItems");
        // if(savedItems != null && JSON.parse(localStorage.getItem("copiedItems")).length != 0){
        //     this.copiedAdvertsStack.next(JSON.parse(localStorage.getItem("copiedItems")));
        //     this.selectedCategoriesStack.next(JSON.parse(localStorage.getItem("copiedCategories")))
        // }
    }

    getSelectedAdverts() {
        return this.copiedAdvertsStack.getValue();
    }

    public setNewPortalId(newId = this.authService.getPortal()) {
        this.mainService.getShopIdByName(newId).subscribe((res) => {
            this.portalId = res.cust_id;
        });
    }

    get getNewPortalId() {
        return this.portalId;
    }

    public getSelectedCategoriesList() {
        return this.selectedCategoriesStack.asObservable();
    }

    public getSelectedCategoriesForFill() {
        return this.selectedCategoriesForFill.asObservable();
    }

    private toCalendarFormat(model) {
        const addZero = (el) => (el < 10 ? "0" + el : el);
        const { year, month, day } = model;
        return year + "-" + addZero(month) + "-" + addZero(day);
    }

    public calendarItems = new Set();
    public selectCalendarDate(calendarModel) {
        if (calendarModel) {
            if (this.calendarItems.size < 12) this.calendarItems.add(this.toCalendarFormat(calendarModel));
            this.updateDisabledDays();
        }
    }

    public calendarTimes = new Set();
    public selectCalendarTime(calendarModel) {
        if (calendarModel) {
            const addZero = (el) => (el < 10 ? "0" + el : el);
            if (this.calendarTimes.size < 5) {
                const { hour, minute } = calendarModel;
                this.calendarTimes.add(addZero(hour) + ":" + addZero(minute));
            }
        }
    }

    private updateDisabledDays() {
        this.disabledDaysCalendar = (date: NgbDate) => {
            return this.getCalendarDates().includes(this.toCalendarFormat(date));
        };
    }

    disabledDaysCalendar = (date: NgbDate) => {
        return this.getCalendarDates().includes(this.toCalendarFormat(date));
    };

    public getCalendarDates() {
        return Array.from(this.calendarItems);
    }
    public getCalendarTimes() {
        return Array.from(this.calendarTimes);
    }

    public deleteCalendarItem(item) {
        if (this.calendarItems.has(item)) {
            this.calendarItems.delete(item);
            this.updateDisabledDays();
        }
    }
    public deleteCalendarTime(item) {
        if (this.calendarTimes.has(item)) {
            this.calendarTimes.delete(item);
        }
    }

    public clearCalendarDates() {
        this.calendarItems.clear();
        this.updateDisabledDays();
    }
    public clearCalendarTimes() {
        this.calendarTimes.clear();
    }

    newDateTime(data: { i: number; date: string }) {
        let existStack = this.copiedAdvertsStack.getValue();
        let searchItem = existStack.find((el) => el.prc_ID == data.i);
        if (searchItem) {
            let index = existStack.indexOf(searchItem);
            searchItem.date = data.date;
            existStack.splice(index, 1, searchItem);
            this.copiedAdvertsStack.next(existStack);
            this.saveData();
        }
    }

    deleteAdv(prc_ID: number) {
        let existStack = this.copiedAdvertsStack.getValue();
        let searchItem = existStack.find((el) => el.prc_ID == prc_ID);
        if (searchItem) {
            let index = existStack.indexOf(searchItem);
            existStack.splice(index, 1);
            this.copiedAdvertsStack.next(existStack);
            this.saveData();
        }
    }

    public put(item) {
        let existStack = this.copiedAdvertsStack.getValue();
        let searchItem = existStack.find((el) => el.prc_ID == item.prc_ID);
        if (!searchItem) {
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        } else {
            existStack.splice(existStack.indexOf(searchItem), 1);
        }
        this.copiedAdvertsStack.next(existStack);
        this.saveData();
    }

    public putCategory(item) {
        let existStack = this.selectedCategoriesStack.getValue();
        existStack.includes(item) ? existStack.splice(existStack.indexOf(item), 1) : existStack.push(item);
        this.selectedCategoriesStack.next(existStack);
        this.saveData();
    }

    public putCategoryForFill(item) {
        let existStack = this.selectedCategoriesForFill.getValue();
        existStack.includes(item) ? existStack.splice(existStack.indexOf(item), 1) : existStack.push(item);
        this.selectedCategoriesForFill.next(existStack);
    }

    public clearData() {
        this.copiedAdvertsStack.next([]);
        this.selectedCategoriesStack.next([]);
        this.selectedCategoriesForFill.next([]);
        this.calendarItems = new Set();
        this.calendarTimes = new Set();
    }

    public cancelCopy() {
        this.clearData();
        this.isNotifyActive = false;
        this.copyMode = false;
    }

    public doCopy() {
        if (this.selectedCategoriesStack.getValue().length === 0) {
            Swal.fire({
                icon: "warning",
                title: "Выберите категорию для выбранных обьявлений!",
                showConfirmButton: false,
                timer: 2000
            });
        } else if (this.copiedAdvertsStack.getValue().length === 0) {
            Swal.fire({
                icon: "warning",
                title: "Выберите обьявления для копирования!",
                showConfirmButton: false,
                timer: 2000
            });
        } else {
            this.showSpinner = true;
            this.mainService
                .doMultiCart({
                    prc_IDs: this.copiedAdvertsStack.getValue(),
                    dates: this.isNotifyActive ? ["999"] : this.getCalendarDates(),
                    times: this.isNotifyActive ? ["999"] : this.getCalendarTimes(),
                    iDs: this.selectedCategoriesStack.getValue().map((el) => el.split("_")[0]),
                    appCode: this.portalId,
                    cid: this.mainService.getUserIdFromToken()
                })
                .subscribe(
                    () => {
                        Swal.fire({
                            icon: "success",
                            title: "Обьявления успешно скопированы!",
                            timer: 1000,
                            willClose: () => {
                                this.showSpinner = false;
                                this.setNewPortalId();
                                this.clearData();
                            //    this.cancelCopy();
                                this._location.back();
                                // this.router.navigate(['/']);
                            }
                        });
                    },
                    () => {
                        Swal.fire({
                            icon: "error",
                            title: "Произошла ошибка копирования!",
                            showConfirmButton: false,
                            timer: 2000,
                            willClose: () => {}
                        });
                        this.showSpinner = false;
                    }
                );
        }
    }

    public doCopyCatalog() {
        if (this.selectedCategoriesStack.getValue().length === 0) {
            Swal.fire({
                icon: "warning",
                title: "Выберите категории каталога для копирования",
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {}
            });
        } else {
            // const preparedObject = {
            //     "prc_IDs": this.selectedCategoriesStack.getValue().join(),
            //     "prc_IDs_fill": this.selectedCategoriesForFill.getValue().join(),
            //     "appCode": this.portalId,
            //     "cid": this.mainService.getUserIdFromToken()
            // }
            // console.log(preparedObject);
            const uniqueArray = Array.from(
                new Set([
                    ...this.selectedCategoriesStack.getValue().map((el) => el.split("_")[0]),
                    ...this.selectedCategoriesForFill.getValue()
                ])
            );
            this.showSpinner = true;
            this.mainService
                .doCopyCatalog({
                    Categories: uniqueArray.map((el) => {
                        return {
                            ID: el,
                            IDdst: "0",
                            Mark: this.selectedCategoriesForFill.getValue().includes(el)
                        };
                    }),
                    AppCode: this.portalId,
                    CID: this.mainService.getUserIdFromToken()
                })
                .subscribe(
                    () => {
                        this.clearData();
                        Swal.fire({
                            icon: "success",
                            title: "Категории успешно скопированы!",
                            timer: 1000,
                            willClose: () => {
                                this.showSpinner = false;
                                this.setNewPortalId();
                                //   this.isNotifyActive = false;
                                  this.cancelCopy();
                                this._location.back();
                                // this.router.navigate(['/']);
                            }
                        });
                    },
                    () => {
                        Swal.fire({
                            icon: "error",
                            title: "Произошла ошибка копирования!",
                            showConfirmButton: false,
                            timer: 2000,
                            willClose: () => {}
                        });
                        this.showSpinner = false;
                    }
                );
        }
    }

    public saveData() {
        localStorage.setItem("copiedCategories", JSON.stringify(this.selectedCategoriesStack.getValue()));
        // localStorage.setItem("copiedItems",JSON.stringify(this.copiedAdvertsStack.getValue()));
    }

    isNotifyActive: boolean = false;
    public notify() {
        this.isNotifyActive = !this.isNotifyActive;
    }

    public selectAll() {
        let existStack = [];
        this.stackOfAllAdverts.forEach((item) => {
            existStack.push({
                prc_ID: item.prc_ID,
                title: item.tName,
                author: item.suplier,
                date: new Date().toISOString()
            });
        });
        let newStack = this.copiedAdvertsStack.value;
        const preveiousDataIDS = this.copiedAdvertsStack.value.map((el) => el.prc_ID);
        for (let curr of existStack) {
            if (!preveiousDataIDS.includes(curr.prc_ID)) {
                newStack.push(curr);
            }
        }
        this.copiedAdvertsStack.next(newStack);
        this.saveData();
    }

    public selectAllCats(v) {
        this.cats.getMainCategories().subscribe((r) => {
            if (Array.isArray(r) && r.length != 0) {
                const cats = r.map((e) => e.id);
                this.selectedCategoriesStack.next(v ? cats : []);
            }
        });
    }

    public selectAllCatsFill(v) {
        this.cats.getMainCategories().subscribe((r) => {
            if (Array.isArray(r) && r.length != 0) {
                const cats = r.map((e) => e.id);
                this.selectedCategoriesForFill.next(v ? cats : []);
            }
        });
    }
}
