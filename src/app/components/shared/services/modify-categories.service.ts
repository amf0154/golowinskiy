import { Injectable } from "@angular/core";
import { EnvService } from "src/app/env.service";
const Swal = require('sweetalert2');
import { MainService } from "./main.service";

@Injectable({
    providedIn: 'root'
})
export class ModifyCatalogService {

constructor(private mainService: MainService, private env: EnvService){}

  modifyCat(type: number, categoryObject, level, callback: Function){
    const data = {
      actionType: type,
      ...categoryObject
    }
    if(level == 10){
      data.id = 0
    }
    if(Object.keys(categoryObject).length === 0 ){
      this.modifyCategory(data,false,callback);
    }else{
    Swal.mixin({
        input: 'select',
        confirmButtonText: 'Next &rarr;',
        showCancelButton: true,
        inputOptions: {
        '1': 'Добавить подраздел',
        '2': 'Переименовать',
        '3': 'Удалить раздел',
      },
      }).queue([
        {
          title: 'Управление каталогом',
          text: 'Выберите действие'
        },
      ]).then((result) => {
        if(result.value)
         switch(result.value[0]){
           case "1": {
            this.modifyCategory(data,false, callback);
             break;
           }
           case "2": {
            this.modifyCategory(data,true,callback);
            break;
          }
          case "3": {
            this.deleteCategory(data);
            break;
          }
          default: Swal.fire({
            title: 'Действие неопознанно',
            html: `
              <pre><code>попробуйте еще раз</code></pre>
            `,
            confirmButtonText: 'закрыть'
          })
         }
      })

    }
  }






  modifyCategory(data, isEdit: boolean = false,clb){
    Swal.fire({
      title: isEdit ? 'Имя редактируемой категории' : 'Введите имя категории:',
      input: 'text',
      cancelButtonText: 'Отмена',
      inputValue: isEdit ? data.txt : '',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Добавить',
      showLoaderOnConfirm: true,
      inputValidator: function(value) {
        if(value === '') { 
          return 'Введите имя категории'
        } else {
          if(value.trim().length === 0){   
            return 'Введите корректное название'
          } 
        }
      },
      preConfirm: (name) => {   
        const preparedBody = {
          "Id": data.id,
          "Name": name,
          "ImgName": "betls.png",
          "CustIdMain": this.mainService.getCustId()
        }
        return fetch(`${this.env.apiUrl}/api/Catalog`,{
          method: isEdit ? 'PUT' : 'POST',
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify(preparedBody)
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        clb()
      if(result.isConfirmed){
        Swal.fire({
          icon: 'success',
          title: "категория успешно добавлена"
        })
      //  this.categoryService.fetchCategoriesAll(this.mainService.getCustId(),null,"1");
      }
    });
  }


  deleteCategory(body){
    Swal.queue([{
      title: 'Удаление категории',
      confirmButtonText: 'Удалить',
      text: 'Вы действительно хотите удалить категорию?',
      cancelButtonText: 'Отмена',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: async () => {
        return new Promise((resolve,reject)=>{
          this.mainService.deleteCategory(body).subscribe(res=>{
            resolve(()=>{
              Swal.insertQueueStep({
              icon: 'success',
              title: "Категория успешно удалена"
            });
          });
          }, error=>{
            resolve(Swal.insertQueueStep({
              icon: 'error',
              title: 'Не могу удалить категорию'
            }));
          })
        })
      }
    }])
  }

}