export namespace Enums {
    export enum ManualType {
        MAIN = 'main',
        DETAIL = 'detail',
        PERSONAL = 'personal',
        EDITCOMMODITY = 'commodityedit',
        NEWCOMMODITY = 'commoditynew',
        SUBSCRIBE = 'subscribe',
        COPYLIST = 'copylist',
        COPYCATALOG = 'copycatalog',
        SITESETTINGS = 'sitesettings',
    };

    export enum UserSettings {
        PERSONAL = 1,
        SITE = 2,
        SORT = 3,
        DELETE_REPEATS = 4,
    };

    export enum ModifyAdvertType {
        DISABLE = 0,
        EDIT_TITLE = 1,
        EDIT_AUDIO = 2,
        EDIT_VIDEO = 3,
    };

    export enum FastModify {
        DISABLED = 0,
        EDIT_TITLE = 1,
        EDIT_AUDIO = 2,
    };

    export enum CatalogMode {
        SEARCH = 0,
        REPEAT = 6,
        STUDY = 7,
        DICTIONARY = 10,
    };

    export enum CatalogSelectItemsMode {
        REPEAT = 1,
        COPY_ITEMS = 2,
        MOVE_TO_CATEGORY = 3,
        DOLL = 4
    };

    export enum SiteDepartment {
        REPEAT = 'repeat',
        STUDY = 'study',
        CABINET = 'cabinet',
        MAIN = 'categories'
    };
    
}