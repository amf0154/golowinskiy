import { Enums } from "./enums";

export namespace Models {
    export interface ManualParamsGet {
        cust_ID_Main: string;
        itemname: Enums.ManualType;
    }
    export interface ManualResponse {
        manual: string;
    }
    export interface ManualParamsSave {
        cust_ID_Main: string;
        itemname: Enums.ManualType;
        manual: string;
    }

    export interface DefRespStatus {
        result: boolean;
    }

    export interface ShopInfo {
        addr: string;
        cust_id: string;
        dz: string;
        email: string;
        isEditing: boolean;
        isSearchPageVariant: number;
        manual: string;
        pageKind: boolean;
        phone: string;
        playAudio: boolean;
        shortDescr: string;
        welcome: string;
    }

    export enum PageRoutes {
        CabinetProducts = 'cabinet/categories/:id/products',
        Detail = 'categories/:id/products/:idProduct',
        CabinetDetail = 'cabinet/categories/:id/products/:idProduct',
        Products = 'categories/:id/products',
        CabinetMain = 'cabinet',
        Subscribe = 'subscribe',
        CopyList = 'copy',
        CopyCat = 'copy-catalog',
        Search = 'search',
        Repeat = 'repeat',
        Study = 'study',
        Dictionary = 'dictionary'
    }; 

    export interface MapPoint {
        cust_ID_Main: string;
        geoname: string;
        latitude: string;
        longitude: string;
    }

    export interface MapPointArray {
        locations: MapPoint[]
    }
};