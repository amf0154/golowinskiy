export interface AdditionalImage{
    imageOrder: number; 
    t_image: string;
    audio?: string;
    video?: string | null;
}