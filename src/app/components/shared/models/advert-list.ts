export interface AdvertList {
    appCode: string;
    createdAt: string;
    cust_ID: string;
    gdate: string;
    id: string;
    idCategories: [];
    idcrumbs: string;
    image_Base: string;
    image_Site: string;
    img: string;
    isShowBasket: string;
    nameCategories: [];
    parent_id: string;
    prc_Br: string;
    prc_ID: number;
    suplier: string;
    tName: string;
    txtcrumbs:string;
}

export interface AdvertDetail{
    catalog: string;
    ctlg_Name: string;
    ctlg_No: number;
    tName: string;
    suplier: string;
    tDescription: string;
    id: number;
    sup_ID: number;
    wgt: string;
    code_1C: string;
    qty: any;
    delivery: string;
    phoneclient: any;
    v_isnoprice: any;
    isprice: any;
    gdate: any;
    createdAt: string;
    creater_ID: number;
    youtube: string;
    latitude: string;
    longitude: string;
    addr: string;
    prc_ID: number;
    prc_Br: any;
    t_imageprev: string;
    additionalImages: Array<{imageOrder: number,t_image: string} | any>
    isActive: boolean;
    email: string;
    skype: string;
    whatsapp: string;
    mediaLink: string | null;
}
