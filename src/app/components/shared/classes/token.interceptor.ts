import { Injectable } from "@angular/core"
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpHeaders } from "@angular/common/http"

import { AuthService } from "../services/auth.service"
import { Observable, throwError } from "rxjs"
import { catchError, map } from "rxjs/operators"
import { Router } from "@angular/router"
import { environment } from "src/environments/environment"
import { EnvService } from "src/app/env.service"
import { MatDialog } from "@angular/material"
import { LoginComponent } from "@components/main-page/auth-page/login/login.component"


@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private authService: AuthService, 
                public router: Router,
                private dialog: MatDialog,
                public envService: EnvService        
    ){

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        if(this.authService.isAuthenticated()){
            /*
            req = req.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + this.authService.getToken()
                }
            })
            */
           if(this.authService.isAuthenticated()){
            const headers = new HttpHeaders({
                'Authorization': `Bearer ${this.authService.getToken()}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
              });
            const headersForOtherDomains = new HttpHeaders({
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
            });  
            req = req.url.includes(this.envService.apiUrl) ? req.clone({headers}) : req.clone({headers: headersForOtherDomains});
        }
        }
        return next.handle(req).pipe(
            catchError(
                (error: HttpErrorResponse) => this.handleAuthError(error)
            )
        )
    } 

    private handleAuthError(error: HttpErrorResponse): Observable<any>{
        if(error.status ===  401){
            return this.dialog.open(LoginComponent, {
                width: '0px',
                data: [],
              }).afterClosed()
              .pipe(
                map(response => {
                //   if(!response){
                //     this.router.navigate(['/home']);
                //   } 
                  return response
                })
              );
        }
        return throwError(error) 
    }

}