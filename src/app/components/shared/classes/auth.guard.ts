import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, Router } from "@angular/router"
import { Observable, of } from "rxjs"
import { Injectable } from "@angular/core"

import { AuthService } from "../services/auth.service"
import { LoginComponent } from "@components/main-page/auth-page/login/login.component"
import { map } from "rxjs/operators"
import { MatDialog } from "@angular/material"


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild{
    constructor(
        private auth: AuthService,
        private dialog: MatDialog,
        private router: Router
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
        if(this.auth.isAuthenticated()){
            return of(true)
        }
        else{  
            return this.dialog.open(LoginComponent, {
              width: '0px',
              data: [],
            }).afterClosed()
            .pipe(
              map(response => {
                if(!response){
                  this.router.navigate(['/home']);
                } 
                return response
              })
            );
        }
    }
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
        return this.canActivate(route, state)
    }
}