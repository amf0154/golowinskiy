import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'image-loader',
  templateUrl: './image-loader.component.html',
  styleUrls: ['./image-loader.component.scss']
})
export class ImageLoaderComponent{
  @Input() image:string;

  isLoading:boolean;
  
  constructor() { 
    this.isLoading=true;
  }

  hideLoader(err: any = false){
    this.isLoading=false;
    if(err){
      err.image = 'assets/images/img-not-available.jpeg';
    }
  }

}
