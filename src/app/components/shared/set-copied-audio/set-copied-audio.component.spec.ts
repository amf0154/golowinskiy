import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetCopiedAudioComponent } from './set-copied-audio.component';

describe('SetCopiedAudioComponent', () => {
  let component: SetCopiedAudioComponent;
  let fixture: ComponentFixture<SetCopiedAudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetCopiedAudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetCopiedAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
