import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MoveAdvertsResp } from 'src/app/interfaces/adverts';
import { DictionaryService } from '../services/dictionary.service';
import { MainService } from '../services/main.service';
import { ModifyAdvertsService } from '../services/modify-adverts.service';
import { NotifyService } from '../services/notify.service';
import { SpinnerService } from '../services/spinner.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'set-copied-audio',
  templateUrl: './set-copied-audio.component.html',
  styleUrls: ['./set-copied-audio.component.scss']
})
export class SetCopiedAudioComponent implements OnInit {
  audioForm: FormGroup;
  position = [{ type: 1, name: 'Основное аудио'},{ type: 2, name: 'Доп.картинка'}];
  audioLink = localStorage.getItem('sharedAudioLink');
  selectedPosition: number = 1;
  constructor(
    private dialogRef: MatDialogRef<SetCopiedAudioComponent>,
    public mainService: MainService,
    public dictionary: DictionaryService,
    public spinner: SpinnerService,
    public modifyAdverts: ModifyAdvertsService,
    public notify: NotifyService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb: FormBuilder
  ) {}

  ngOnInit() {
    const hasAddImgs = this.data.images && this.data.images.length !== 0;

    if(!hasAddImgs){
      this.position.length = 1;
    }

    this.audioForm = this.fb.group({
      Prc_ID: this.data.prc_ID,
      Appcode: this.data.appCode,
      MediaFileName: this.audioLink,
      position: [this.position[0].type, [Validators.required]], 
      MediaOrder: hasAddImgs ? [this.data.images[0].imageOrder, [Validators.required]] : null
    });
  }

  cancel(){
    this.dialogRef.close();
  }

  changePosition(){
    this.selectedPosition = this.audioForm.value.position;
  }


  submit(){
    if(!this.audioForm.valid){
      Swal.fire({
        icon: 'error',
        title: "Некорректно заполнена форма",
        showConfirmButton: false,
        timer: 3000,
      });
    }else{
      const preparedData = this.audioForm.getRawValue();
      this.selectedPosition == 1 ? preparedData.MediaOrder = null : null;
      delete preparedData.position;
      this.spinner.display(true);
      this.mainService.setCopiedAudio(preparedData).subscribe(({result}: MoveAdvertsResp)=>{
        if({result}){
          //this.modifyAdverts.fastEdit(2);
            this.dialogRef.close(true);
            this.notify.showNotify(
              this.dictionary.getTranslate("adv_updated",`Обьявление успешно обновлено`),
              '',
              'success'
          );            
        }else{
          this.spinner.display(false);
          Swal.fire({
            icon: 'error',
            title: "Ошибка сохранения",
            showConfirmButton: false,
            timer: 3000,
          });
        }
      },() => this.spinner.display(false));
    }
  }

}
