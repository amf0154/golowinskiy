import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-video-upload',
  templateUrl: './video-upload.component.html',
  styleUrls: ['./video-upload.component.css']
})
export class VideoUploadComponent implements OnInit {

  public length: string = null;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<VideoUploadComponent>,
  ) { }
  ngOnInit(): void {
    this.length = (this.data.size/1024/1024).toFixed(2) + " MB"
  }



  close(): void {
    this.dialogRef.close();
  }

}
