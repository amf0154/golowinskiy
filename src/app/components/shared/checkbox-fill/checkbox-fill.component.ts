import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CopyAdvertsService } from '../services/copyadverts.service';

@Component({
  selector: 'fill-checkbox',
  templateUrl: './checkbox-fill.component.html',
  styleUrls: ['./checkbox-fill.component.css']
})
export class FillCheckboxComponent implements OnInit, OnDestroy {

  constructor(public copyAdvertsService: CopyAdvertsService) { 
    this.copyAdvertsService.getSelectedCategoriesForFill().subscribe(res => {
      this.selectedItems = res;
    });
  }
  @Input() isChecked: boolean;
  @Input() catalogData: {cust_id: string, id: string,listInnerCat};
  @Output() status = new EventEmitter();
  @Input() isMobileVersion: boolean = false;
  private selectedItems = [];
  subscription: Subscription;
  checkbox: FormGroup;
  get checkVal(){
    return this.selectedItems.includes(this.catalogData.id)
  }

  ngOnInit() {

    this.checkbox = new FormGroup({
      value: new FormControl(this.isChecked),
    });
    this.subscription = this.checkbox.valueChanges.subscribe(res=>this.applyChanges(res.value));
  }

  applyChanges(res: boolean){
    this.copyAdvertsService.putCategoryForFill(this.catalogData.id);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
