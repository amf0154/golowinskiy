import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Subscription } from 'rxjs';
import { MainService } from '../shared/services/main.service';
import { CopyAdvertsService } from '../shared/services/copyadverts.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit, OnDestroy {
  private selectedItems = []
  constructor(private authService: AuthService, private mainService: MainService, public copyAdvertsService: CopyAdvertsService) {
    this.subscription2 = this.copyAdvertsService.getSelectedCategoriesList().subscribe(res => {
      this.selectedItems = res;
    });
  }
  @Input() isArrow?: boolean;
  @Input() isChecked: boolean;
  @Input() catalogData: {cust_id: string, id: string,listInnerCat};
  @Input() copy = false;
  @Output() status = new EventEmitter();
  @Input() isMobileVersion: boolean = false;
  @Input() showAllCheckbox: boolean = false;
  subscription: Subscription;
  subscription2: Subscription;
  checkbox: FormGroup;
  showCheckbox: boolean = false;

  get checkVal(){
    return this.isChecked || this.selectedItems.includes(this.catalogData.id)
  }

  ngOnInit() {
    if(!this.showAllCheckbox){
      if(this.catalogData.listInnerCat.length == 0){
        this.showCheckbox = true;
      }
    }else{
      this.showCheckbox = this.showAllCheckbox;
    }
    this.checkbox = new FormGroup({
      value: new FormControl(this.isChecked || this.selectedItems.includes(this.catalogData.id)),
    });
    this.subscription = this.checkbox.valueChanges.subscribe(res=>this.applyChanges(res.value));
   // if(this.isMobileVersion)
    //  window.document.getElementById("cbox").style.height="40px";
  }

  applyChanges(res: boolean){
    const preparedStatement = {
      AppCode: this.catalogData.cust_id,
      CID: this.authService.getUserId(),
      ID: this.catalogData.id,
      dellAll: false,
      Mark: res
    }

    if(!this.copy)
      this.mainService.addSubscription(preparedStatement).subscribe(res=>this.status.emit("true"),err=>this.status.emit("false"));
    if(this.copy)
      this.copyAdvertsService.putCategory(this.catalogData.id+ '_'+ this.catalogData.cust_id);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }
}
