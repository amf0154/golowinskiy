import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCatalogComponent } from './modify-catalog.component';

describe('ModifyCatalogComponent', () => {
  let component: ModifyCatalogComponent;
  let fixture: ComponentFixture<ModifyCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
