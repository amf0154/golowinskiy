import { Component, OnInit } from '@angular/core';
import { Console } from 'console';
import { Observable } from 'rxjs';
import { CategoryItem } from '../categories/categories.component';
import { EnvService } from 'src/app/env.service';
import { AuthService } from '../shared/services/auth.service';
import { CategoriesService } from '../shared/services/categories.service';
import { CommonService } from '../shared/services/common.service';
import { DictionaryService } from '../shared/services/dictionary.service';
import { MainService } from '../shared/services/main.service';
import { StorageService } from '../shared/services/storage.service';

@Component({
  selector: 'app-modify-catalog',
  templateUrl: './modify-catalog.component.html',
  styleUrls: ['./modify-catalog.component.scss']
})
export class ModifyCatalogComponent implements OnInit {

  headers = new Headers({
    'Content-Type': 'application/json; charset=utf8',
    'Authorization': 'Bearer ' + localStorage.getItem('token')
  })

  userId
  cust_id


  showSpinner = true
  idCategory
  itemName = ''
  categories: CategoryItem[] = []

  fio
  userName
  phone
  loadingImage: string = null;
  loadingMini: string = null;
  loadingSpinner: string = null;
  public isAdmin: Observable<boolean>;
  // new images code


  // categories
  initialCategories: CategoryItem[] = []

   advertId: string = "1";
   useRotate: boolean = false;
   testObject: any;
   
  constructor(
    public authService: AuthService,
    public mainService: MainService,
    public storageService: StorageService,
    public categoriesService: CategoriesService,
    public commonStore : CommonService,
    public env: EnvService,
    public local: DictionaryService
  ) { 
    this.loadingMini = this.loadingImage = this.commonStore.loadingLittleRedSpinner;
    this.loadingSpinner = this.commonStore.loadingImageSpinner;

    this.storageService.setCategories([])
    this.isAdmin = this.authService.isAdminObservable()
  }

  ngOnInit() {
    this.fio = localStorage.getItem('fio')
    this.userName = false //localStorage.getItem('userName')
    this.phone = false //localStorage.getItem('phone')
    this.mainService.getShopInfo().subscribe(
      (res) => {
        this.cust_id = res.cust_id;
        this.userId = this.authService.getUserId()
        this.showSpinner = false;
        this.categoriesService.fetchCategoriesAll(this.cust_id,null,this.advertId);
      }, error=>alert(error.error.message)
    )
    this.initialCategories = this.storageService.getCategories();
    this.storageService.breadcrumbFlag = false;
    setTimeout( ()=> window.scrollTo(0,0) ,300); 
  }

  ngAfterContentInit() {
    document.body.style.backgroundColor = "#ffffff";
  }

  refreshCat: boolean = false;
  breadcrumbsClick(i) {
    this.storageService.setCategories(this.categories.slice(0,i))
    this.categories = []
    this.storageService.breadcrumbFlag = true
    this.initialCategories = this.storageService.getCategories();
    this.refreshCat = true;
    setTimeout(()=>this.refreshCat = false);
    this.idCategory = '';
    this.itemName = '';  
  }

  public isCabinet(): boolean {
    return window.location.pathname.includes('cabinet')
  }
  onCategoriesClick(items: CategoryItem[]) {
    this.storageService.setCategories(items)
  }
  categorySelect(event){
    
  }

  categoryAdded(event){
    this.mainService.clearCategoriesHistory();
    this.categoriesService.fetchCategoriesAll(this.cust_id,null,this.advertId);
  }

}
