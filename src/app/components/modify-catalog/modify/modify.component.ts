import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MainService } from '@components/shared/services/main.service';
import { ModifyCatalogComponent } from '../modify-catalog.component';
@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModifyCatalogComponent>,
    private mainService: MainService,
    ) { }

  public submitted: boolean = false;
  public catalogForm = this.formBuilder.group({ Name: ['', [Validators.required, this.emptySpacesValidator]]});
  get f() {
    return this.catalogForm.controls;
  }

  ngOnInit(): void {
    if(this.data.actionType === 2 || this.data.actionType === 3){
      this.catalogForm.controls['Name'].setValue(this.data.txt)
    }
  }

  public emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value.trim().length === 0) {
     return { emptySpaces: 'Введите корректное название' };
    }
     return null;
   }

  public saveButton() {
    this.submitted = true;
    if(!this.catalogForm.valid){
      return false
    }else{
      const preparedBody = {
        "Id": this.data.id,
        ...this.catalogForm.value,
        "ImgName": "betls.png",
        "CustIdMain": this.mainService.getCustId()
      }
      switch(this.data.actionType){
        case 1: {
          this.mainService.addNewCategory(preparedBody).subscribe(() => this.dialogRef.close(true));
          break;
        }
        case 2:{
          this.mainService.updateCategory(preparedBody).subscribe(() => this.dialogRef.close(true));
          break;
        } 
        case 3: {
          this.mainService.deleteCategory(preparedBody).subscribe(() => this.dialogRef.close(true));
          break
        }
      }
      
    }
  }

  public onNoClick(){
    this.dialogRef.close(null);
  }

}