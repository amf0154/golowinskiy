import { Component, ElementRef, OnInit, ViewChild, OnDestroy, HostListener } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Message } from "src/app/components/shared/models/message.model";
import { AuthService } from "@components/shared/services/auth.service";
import { MainService } from "../shared/services/main.service";
import { CategoryItem, CategoriesComponent } from "../categories/categories.component";
import { AdditionalImagesData, AwsImageResponse } from "../shared/interfaces";
import { of, BehaviorSubject, Subscription } from "rxjs";
import { StorageService } from "../shared/services/storage.service";
import { CategoriesService } from "../shared/services/categories.service";
import { CommonService } from "../shared/services/common.service";
import { getBase64Strings } from "exif-rotate-js/lib";
import { DictionaryService } from "../shared/services/dictionary.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { EnvService } from "src/app/env.service";
import { Enums } from "@components/shared/models/enums";
import { Models } from "@components/shared/models/models";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { Title } from "@angular/platform-browser";

const Swal = require("sweetalert2");
export interface ImageDataInterface {
    src: string | any;
    name: string;
    blob?: Blob;
    file: File | any;
    link: string;
}

@Component({
    selector: "app-advertisement-page",
    templateUrl: "./advertisement-page.component.html",
    styleUrls: ["./advertisement-page.component.scss"]
})
export class AdvertisementPageComponent implements OnInit {
    headers = new Headers({
        "Content-Type": "application/json; charset=utf8",
        Authorization: "Bearer " + localStorage.getItem("token")
    });
    additionalImageLimit = 50;
    public apiRoot;
    public userId;
    public cust_id;
    public uploadStatus: boolean = false;
    public form: FormGroup;
    public data_form: any;
    public showRecorder: boolean = false;
    public message: Message;
    public showSpinner = true;
    public isDisabled = true;
    public _imageNotLoaded = new BehaviorSubject<boolean>(false);
    public idCategory;
    public itemName = "";
    public categories: CategoryItem[] = [];
    public showCatalog = false;
    public fio;
    public userName;
    public phone;
    public isCanPromo: boolean = true;
    public prc_id;
    public loadingImage: string = null;
    public loadingMini: string = null;
    public loadingSpinner: string = null;
    public mainImageData: ImageDataInterface = {
        src: "",
        name: "",
        file: null,
        link: null
    };
    public additionalImagesData: ImageDataInterface[] = [];
    public initialCategories: CategoryItem[] = [];
    @ViewChild("submitButton", { static: true }) submitButton: ElementRef;
    @ViewChild("mainImage", { static: true }) mainImg: ElementRef;
    @ViewChild("videoProgess", { static: false }) videoProgess: ElementRef;
    @ViewChild("mainResizer", { static: true }) mainResizer: ElementRef;
    public advertId: string = "1";
    public useRotate: boolean = false;
    public testObject: any;
    public copiedAdvId;
    any = null;
    public copiedAdvert: any = null;
    public isAdmin: boolean = false;
    public prevPage: string = null;
    constructor(
        private router: Router,
        private authService: AuthService,
        private mainService: MainService,
        public storageService: StorageService,
        public categoriesService: CategoriesService,
        public dialog: MatDialog,
        private spinner: SpinnerService,
        public commonStore: CommonService,
        public route: ActivatedRoute,
        public env: EnvService,
        public local: DictionaryService,
        private titleService: Title
    ) {
        this.loadingMini = this.loadingImage = this.commonStore.loadingLittleRedSpinner;
        this.loadingSpinner = this.commonStore.loadingImageSpinner;
        this.apiRoot = this.env.apiUrl;
        titleService.setTitle(local.getTranslate('make_new_adv','Создание нового обьявления'));
        this.storageService.setCategories([]);
        this.route.queryParams.subscribe((queryParam: any) => {
            if (queryParam["copy"]) {
                this.copiedAdvId = queryParam["copy"] ? queryParam["copy"] : null;
                this.prevPage = JSON.parse(queryParam["prev"]);
            } else {
            }
        });
    }

    public copiedImages = [];
    ngOnInit() {
        this.useRotate = false;
        this.createForm();
        this.message = new Message("danger", "");
        this.fio = localStorage.getItem("fio");
        this.userName = false; //localStorage.getItem('userName')
        this.phone = false; //localStorage.getItem('phone')
        this.mainService.getShopInfo().subscribe(
            (res) => {
                if (this.authService.getUserId() == res.cust_id || res.isEditing) {
                    this.isAdmin = this.authService.getUserId() == res.cust_id;
                    this.cust_id = res.cust_id;
                    this.mainService.setCustId(res.cust_id);
                    this.userId = this.authService.getUserId();
                    this.showSpinner = false;
                    this.categoriesService.fetchCategoriesAll(
                        this.cust_id,
                        this.authService.getUserId(),
                        this.advertId
                    );
                } else {
                    this.router.navigate(["/"]);
                }
                if (this.copiedAdvId) {
                    this.mainService
                        .retailPrice(this.copiedAdvId)
                        .subscribe((retail: { retail: number; isChange: boolean }) => {
                            this.mainService
                                .getProduct(this.copiedAdvId, this.authService.getUserId(), res.cust_id)
                                .subscribe((res: any) => {
                                    this.form.setValue({
                                        Article: "",
                                        TName: res.tName,
                                        TDescription: res.tDescription
                                            ? res.tDescription.replace(/<br>/g, "\n")
                                            : res.tDescription,
                                        TCost: retail.retail,
                                        TImageprev: "",
                                        TypeProd: "",
                                        PrcNt: "",
                                        TransformMech: "",
                                        Ctlg_Name: res.ctlg_Name,
                                        Categories: "",
                                        youtube: res.youtube.includes("cloudfront") ? "" : res.youtube, // res.youtube,
                                        audio: ""
                                    });
                                    this.copiedAdvert = res;
                                    this.doCopy();
                                });
                        });
                }
            },
            (error) => {
                this.showSpinner = false;
                Swal.fire({
                    position: "center",
                    icon: "warning",
                    title: "Не могу загрузить информацию по магазину, попробуйте позже.",
                    showConfirmButton: true
                    //  timer: 2000
                });
            }
        );
        this.initialCategories = this.storageService.getCategories();
        this.storageService.breadcrumbFlag = false;
        setTimeout(() => window.scrollTo(0, 0), 300);
    }

    doCopy() {
        this.doCopyOfImages();
        this.doCopyCover();
        this.doCopyAudio();
    }

    @HostListener("window:paste", ["$event"])
    onPaste($event) {
        const dt = $event.clipboardData;
        const file = dt.files[0];
        let obj = {
            target: {
                files: [file]
            }
        };
        if (!this.categorySelected) {
            this.showMessage(
                this.local.getTranslate("category_not_selected", "Вы не выбрали категорию каталога!"),
                "warning",
                false,
                false
            );
        } else {
            if (file && file.type.includes("image")) this.additionalImagesAdd(obj);
        }
    }

    isCopyDone: boolean = false;
    doCopyOfImages() {
        if (this.copiedAdvert.additionalImages.length != 0) {
            this.showSpinner = true;
            const links = this.copiedAdvert.additionalImages.map((el) => "https://" + el.t_image);
            links.forEach((v, i) => {
                this.mainService.getImageByLink(v).subscribe((res) => {
                    const mainImageData = {
                        src: v,
                        name: this.uuidv4() + ".png",
                        file: new File([res], this.uuidv4() + ".png"),
                        link: null
                    };
                    this.additionalImagesData.push(mainImageData);
                    this.uploadImages(mainImageData, this.additionalImagesData.length - 1, true, 2);
                    if (i === links.length - 1) {
                        this.isCopyDone = true;
                        this.showSpinner = false;
                    }
                });
            });
        }
    }

    isCopyCoverDone: boolean = false;
    doCopyCover() {
        if (this.copiedAdvert.t_imageprev != null && this.copiedAdvert.t_imageprev.length > 10) {
            if (this.mainImageData.name) {
                this.mainService.deleteCloudContent(this.mainImageData.link).subscribe((res) => {});
                this.uploadedImageStatuses.delete(this.mainImageData.name);
                document.getElementById(this.mainImageData.name).style.display = "block";
            } else {
                this.mainImg.nativeElement.style.display = "block";
            }
            const link = this.copiedAdvert.t_imageprev;

            this.mainService.getImageByLink("https://" + link).subscribe((res) => {
                this.isCopyCoverDone = true;
                this.mainImageData = {
                    src: "https://" + link,
                    name: this.uuidv4() + ".png",
                    file: new File([res], this.uuidv4() + ".png"),
                    link: null
                };
                this.isDisabled = false;
                this.uploadImages(this.mainImageData, null, true, 1);
            });
        }
    }

    isCopyAudioDone: boolean = false;
    doCopyAudio() {
        if (this.copiedAdvert.mediaLink != null && this.copiedAdvert.mediaLink.length > 10) {
            this.mainService.getImageByLink(this.copiedAdvert.mediaLink).subscribe((res) => {
                this.fileToUpload = new File([res], "audio_" + this.uuidv4() + ".mp3");
            });
        }
    }

    balanser: boolean = false;
    categorySelected: boolean = false;
    categoryNotSelected = () =>
        this.showMessage(
            this.local.getTranslate("category_not_selected", "Вы не выбрали категорию каталога!"),
            "warning",
            false,
            false
        );
    blockInputs(status: boolean = true) {
        const form = window.document.getElementById("form");
        if (status) {
            this.form.disable();
            this.categorySelected = false;
            form.addEventListener("click", this.categoryNotSelected, false);
        } else {
            this.form.enable();
            this.categorySelected = true;
            form.removeEventListener("click", this.categoryNotSelected, false);
        }
    }
    ngAfterContentInit() {
        document.body.style.backgroundColor = "#ffffff";
    }

    showHideRecorder() {
        this.showRecorder = !this.showRecorder;
        if (!this.showRecorder) {
            this.clear();
        }
    }

    private showMessage(
        text: string,
        type: string = "danger",
        redirect: boolean = true,
        showSpinner: boolean = false,
        hideMessage: boolean = true
    ) {
        this.message = new Message(type, text, showSpinner);
        window.setTimeout(() => {
            if (hideMessage) this.message.text = "";
            if (this.message.type == "success" && redirect) {
                this.router.navigate(["/addProduct"]);
            }
        }, 2000);
    }

    removeAdditionalImage(index: number) {
        const link = this.additionalImagesData[index].link;
        this.mainService.deleteCloudContent(link).subscribe((res) => {});
        this.additionalImagesData.splice(index, 1);
    }

    recordFileBlob = null;
    audioRecord($event) {
        this.recordFileBlob = $event;
    }

    Validators(el) {
        this.form.controls["Categories"].setValue(el.txt);
    }

    selectedFile: File = null;
    mainFileSelected(event) {
        this.selectedFile = <File>event.target.files[0];
        if (this.mainImageData.name) {
            this.mainService.deleteCloudContent(this.mainImageData.link).subscribe((res) => {});
            this.uploadedImageStatuses.delete(this.mainImageData.name);
            document.getElementById(this.mainImageData.name).style.display = "block";
        } else {
            this.mainImg.nativeElement.style.display = "block";
        }
        this.mainImageData.src = this.loadingSpinner;
        this.uploadStatus = true;
        getBase64Strings(event.target.files, { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then((file) => {
                    this.uploadStatus = false;
                    this.isDisabled = false;
                    this.mainImageData = { src: res[0], name: this.uuidv4() + ".png", file, link: null };
                    this.uploadImages(this.mainImageData, null, false, 1);
                });
        });
    }

    selectedFiles: File = null;
    additionalImagesAdd(event) {
        if (event.target.files.length < this.additionalImageLimit - this.additionalImagesData.length) {
            this.uploadStatus = true;
            for (let i = 0; i < event.target.files.length; i++) {
                this.additionalImagesData.push({
                    src: this.loadingSpinner,
                    name: null,
                    file: null,
                    link: null
                });
            }
            setTimeout(() => {
                for (let i = 1; i <= event.target.files.length; i++) {
                    getBase64Strings([event.target.files[i - 1]], { maxSize: 2400 }).then((res) => {
                        fetch(res[0])
                            .then((res) => res.blob())
                            .then((file) => {
                                if (event.target.files.length === i) this.uploadStatus = false;
                                const preparedObject = { src: res[0], name: this.uuidv4() + ".png", file, link: null };
                                this.additionalImagesData[this.additionalImagesData.length - i] = preparedObject;
                                this.uploadImages(preparedObject, this.additionalImagesData.length - i, false, 2);
                            });
                    });
                }
            });
        } else {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Максимальное количество доп.картинок не может быть больше 9",
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    additionalImagesChange(event, index, isTurning: boolean = false) {
        this.showSpinner = true;
        if (this.additionalImagesData[index].name) {
            this.mainService.deleteCloudContent(this.additionalImagesData[index].link).subscribe((res) => {});
            this.uploadedImageStatuses.delete(this.additionalImagesData[index].name);
            document.getElementById(this.additionalImagesData[index].name).style.display = "block";
        }
        this.additionalImagesData[index] = {
            src: this.loadingSpinner,
            name: null,
            file: null,
            link: null
        };
        const file = isTurning ? event : <File>event.target.files[0];
        getBase64Strings(event.target.files, { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then((file) => {
                    this.showSpinner = false;
                    const item = this.additionalImagesData[index];
                    item.src = res[0];
                    item.name = this.uuidv4() + ".png";
                    item.file = file;
                    this.uploadImages(item, index, false, 2);
                });
        });
    }

    isFormValid(): boolean {
        return this.form.valid;
    }

    manual() {
        const Swal = require("sweetalert2");
        const body = {
            cust_ID_Main: this.mainService.getCustId(),
            itemname: Enums.ManualType.NEWCOMMODITY
        };
        this.mainService.getManual(body).subscribe(
            (res: Models.ManualResponse) => {
                Swal.fire({
                    title: "<strong>Инструкция</strong>",
                    html: res.manual,
                    customClass: "swal-wide",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Понятно',
                    confirmButtonAriaLabel: "Thumbs up, great!"
                });
            },
            () => this.spinner.display(false),
            () => this.spinner.display(false)
        );
    }

    previousSelectedCategory: string = null;
    createForm(blockInputsEnable: boolean = true) {
        this.form = new FormGroup({
            Article: new FormControl(null),
            TName: new FormControl(null, [Validators.required]),
            TDescription: new FormControl(null),
            TCost: new FormControl(null),
            TImageprev: new FormControl(null),
            TypeProd: new FormControl(null),
            PrcNt: new FormControl(null),
            TransformMech: new FormControl(null),
            Ctlg_Name: new FormControl(null),
            Categories: new FormControl(null, [Validators.required]),
            youtube: new FormControl(null),
            audio: new FormControl(null)
        });
        if (blockInputsEnable) this.blockInputs();
    }

    successAddedProduct(Ctlg_Name, prc_id) {
        if (this.recordFileBlob != null) {
            //  this.mainService.uploadDropbox(this.recordFileBlob,prc_id).then((res)=>{
            this.showSpinner = false;
            this.showMessage(
                this.local.getTranslate("adv_added", "Объявление было успешно размещено"),
                "success",
                false
            );
            this.createForm(false);
            this.form.controls["Categories"].setValue(Ctlg_Name);
            this.isDisabled = true;
            this.mainResizer.nativeElement.display = "none";
            this.mainImageData = {
                src: "",
                name: "",
                blob: null,
                file: null,
                link: null
            };
            this.recordFileBlob = null;
            this.fileToUpload = null;
            this.showRecorder = false;
            this.additionalImagesData = [];

            setTimeout(() => {
                if (this.copiedAdvId != null) this.router.navigate([this.prevPage]);
                else window.scrollTo(0, 0);
            }, 1000);
            //   });
        } else {
            this.showSpinner = false;
            this.showMessage(
                this.local.getTranslate("adv_added", "Объявление было успешно размещено"),
                "success",
                false
            );
            this.createForm(false);
            this.form.controls["Categories"].setValue(Ctlg_Name);
            this.isDisabled = true;
            this.fileToUpload = null;
            this.mainResizer.nativeElement.display = "none";
            this.mainImageData = {
                src: "",
                name: "",
                blob: null,
                file: null,
                link: null
            };
            this.additionalImagesData = [];
            setTimeout(() => {
                if (this.copiedAdvId != null) this.router.navigate([this.prevPage]);
                else window.scrollTo(0, 0);
            }, 1000);
        }
    }

    countAdditionalImgs: number = 0;
    submit() {
        if (this.form.invalid) {
            if (this.form.controls["Categories"].invalid) {
                this.showMessage(
                    this.local.getTranslate("category_not_selected", "Вы не выбрали категорию каталога!"),
                    "warning",
                    false,
                    false
                );
            } else if (this.form.controls["TName"].invalid) {
                this.showMessage(
                    this.local.getTranslate("naim_not_selected", "Вы не ввели наименование!"),
                    "warning",
                    false,
                    false
                );
            }
        } else {
            this.showMessage(
                this.local.getTranslate("being_published", "Идет публикация "),
                "primary",
                false,
                true,
                false
            );
            this.submitButton.nativeElement.disabled = true;
            let intervalChecker = setInterval(() => {
                if (this._imageNotLoaded.value == false) {
                    clearInterval(intervalChecker);
                    this.sendData();
                }
            }, 1000);
        }
    }

    public blobToFile = (theBlob: Blob, fileName: string): File => {
        var b: any = theBlob;
        b.lastModifiedDate = new Date();
        b.name = fileName;
        return <File>theBlob;
    };

    sendData() {
        const formData = this.form.value;
        this.data_form = {
            Catalog: this.cust_id, //nomer catalog
            Id: this.idCategory, // post categories/
            Ctlg_Name: formData.Categories, //input form
            TArticle: formData.Article, //input form
            TName: formData.TName, //input form
            TDescription: formData.TDescription, //input form
            TCost: formData.TCost, //input form
            Appcode: this.cust_id,
            TypeProd: formData.TypeProd, //input form
            PrcNt: formData.PrcNt, //input form
            TransformMech: formData.TransformMech, //input form
            CID: this.authService.getUserId(), // userId for auth,
            video: formData.youtube,
            audio: formData.audio
        };
        if (this.selectedFile || this.isCopyCoverDone) {
            this.data_form["TImageprev"] = this.mainImageData.link;
        }
        if (this.isCanPromo === true) {
            this.Validators(this.data_form.Ctlg_Name);
            if (this.recordFileBlob != null || this.fileToUpload != null) {
                let data = new FormData();
                if (this.fileToUpload != null) {
                    this.fileToUpload = new File([this.fileToUpload], "attached_" + this.uuidv4() + ".mp3");
                }
                let audioRecord;
                if (this.recordFileBlob != null)
                    audioRecord = new File([this.recordFileBlob], "voiceRecord_" + this.uuidv4() + ".mp3");
                data.append("file", this.recordFileBlob != null ? audioRecord : this.fileToUpload);
                this.mainService.uploadFileAws(data).subscribe(
                    (res: any) => {
                        const link = "https://" + res.data;
                        this.data_form.audio = link;
                        this.uploadAdvert(this.data_form);
                    },
                    (err) => {
                        if (err.status == 200) {
                            const link = "https://" + err.error.text;
                            this.data_form.audio = link;
                            this.uploadAdvert(this.data_form);
                        } else {
                            this.uploadAdvert(this.data_form);
                        }
                    }
                );
            } else {
                this.uploadAdvert(this.data_form);
            }
        } else {
            alert("isCanPromo is false");
        }
    }
    uploadedImageStatuses = new Map();
    showStatus(name, loaded, total, xhr) {
        const percent = Math.ceil((loaded / total) * 100);
        const element = document.getElementById(name);
        if (element) (element as any).value = percent;
    }

    showStatusFile(name, loaded, total, xhr) {
        const percent = Math.ceil((loaded / total) * 100);
        const element = document.getElementById(name);
        const viewPercent = document.getElementById("fileProgress");
        if (document.getElementById("cancelUpload"))
            document.getElementById("cancelUpload").onclick = function () {
                if (xhr) xhr.abort();
                xhr = null;
            };
        if (element && viewPercent) {
            element.style.maxWidth = percent + "%";
            viewPercent.innerText = percent + "%";
        }
    }

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(","),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, { type: mime });
    }

    uploadImages(additionalImagesData, index, fromFile: boolean = false, type: number) {
        const name = additionalImagesData.name;
        const file = !fromFile ? this.dataURLtoFile(additionalImagesData.src, additionalImagesData.name) : null;
        const formData = new FormData();
        formData.append("file", !fromFile ? file : additionalImagesData.file);
        this.uploadedImageStatuses.set(name, false);
        this._imageNotLoaded.next(true);
        this.mainService
            .uploadImageXhrCloud(formData, name, fromFile ? this.showStatusFile : this.showStatus)
            .subscribe((res: AwsImageResponse) => {
                if (res.success && index == null) {
                    switch (type) {
                        case 1: {
                            this.mainImageData.link = res.data;
                            break;
                        }
                        case 3: {
                            this.isFileLoaded = true;
                            this.form.controls["youtube"].setValue(res.data);
                            break;
                        }
                    }
                } else if (res.success && index != null) {
                    this.additionalImagesData[index].link = res.data;
                }
                this.uploadedImageStatuses.set(name, true);
                const element = document.getElementById(name);
                const rotateElement = document.getElementById(`rotate_${name}`);
                if (rotateElement) {
                    rotateElement.style.display = "none";
                }
                if (element) {
                    element.style.display = "none";
                }
                this._imageNotLoaded.next(!Array.from(this.uploadedImageStatuses.values()).every((obj) => obj));
            });
    }
    previousItem: {} = null;
    categorySelect(items: CategoryItem[]) {
        this.blockInputs(false);
        if (!this.previousItem) {
            this.previousItem = items[items.length - 1];
        } else {
            if (JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length - 1])) {
                this.previousItem = items[items.length - 1];
            }
        }
        setTimeout(() => {
            this.categories = items;
            let item = items[items.length - 1];
            this.form.controls["Categories"].setValue(item.txt);
            this.idCategory = item.id;
            this.showCatalog = false;
        });
    }

    uploadAdvert(data) {
        this.mainService.addProduct(data, this.headers).subscribe((res: any) => {
            const data: AdditionalImagesData[] = [];
            for (let i = 0; i < this.additionalImagesData.length; i++) {
                data.push({
                    request: {
                        catalog: this.cust_id,
                        id: this.idCategory,
                        prc_ID: res.prc_id,
                        imageOrder: i,
                        tImage: this.additionalImagesData[i].link,
                        appcode: this.cust_id,
                        cid: this.userId
                    }
                });
            }
            this.mainService.additionalImagesArray(data).subscribe(() => {
                this.countAdditionalImgs += 1;
                if (this.countAdditionalImgs == this.additionalImagesData.length) {
                    this.countAdditionalImgs = 0;
                    this.mainService.moveToMainAdditionalPicture(res.prc_id, this.cust_id);
                    this.successAddedProduct(this.data_form.Ctlg_Name, res.prc_id);
                }
            });
            if (this.additionalImagesData.length == 0) {
                this.successAddedProduct(this.data_form.Ctlg_Name, res.prc_id);
            }
        });
    }

    refreshCat: boolean = false;
    breadcrumbsClick(i) {
        this.storageService.setCategories(this.categories.slice(0, i));
        this.categories = [];
        this.storageService.breadcrumbFlag = true;
        this.initialCategories = this.storageService.getCategories();
        this.refreshCat = true;
        setTimeout(() => (this.refreshCat = false));
        this.form.controls["Categories"].setValue("");
        this.idCategory = "";
        this.itemName = "";
    }

    public isCabinet(): boolean {
        return window.location.pathname.includes("cabinet");
    }
    onCategoriesClick(items: CategoryItem[]) {
        this.storageService.setCategories(items);
    }

    fileToUpload: File = null;
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }

    videoPasted(){
        const value = this.form.get('youtube').value;
        if(value && value.length &&['iframe', 'vk.com/video'].every((attr) => value.includes(attr))){
            const link = value.split('src="')[1].split('"')[0];
            if(link && ["oid", "id", "hash"].every((attr) => link.includes(attr))){
                this.form.get('youtube').setValue(link);
            }
        }
    }

    deleteVideo() {
        this.mainService.deleteCloudContent(this.form.controls["youtube"].value).subscribe((res) => {});
        this.uploadedImageStatuses.delete(this.videofile.name);
        this.form.controls["youtube"].setValue("");
        this.videofile = null;
        this.isFileLoaded = false;
    }

    videofile: any = null;
    isFileLoaded: boolean = false;
    uploadVideo(event) {
        const file = <File>event.target.files[0];
        if (Number((event.target.files[0].size / 1024 / 1024).toFixed(0)) > 105) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Размер файла не должен превышать 100мб",
                showConfirmButton: false,
                timer: 2000
            });
            setTimeout(() => {
                event.target.value = "";
            }, 1000);
        } else {
            const filename = this.uuidv4() + ".mp4";
            this.videofile = { src: null, name: filename, file: new File([file], filename), link: null };
            this.uploadImages(this.videofile, null, true, 3);
        }
    }

    clear() {
        this.fileToUpload = null;
        this.audioRecord(null);
        this.showRecorder = false;
    }

    private uuidv4() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    }
}
