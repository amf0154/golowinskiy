import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoryItem} from '../categories.component';
import {StorageService} from '../../shared/services/storage.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';
import { EnvService } from 'src/app/env.service';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { AuthService } from '@components/shared/services/auth.service';
import { SharedService } from '@components/shared/services/shared.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-mobile-categories',
  templateUrl: './mobile-categories.component.html',
  styleUrls: ['./mobile-categories.component.scss']
})
export class MobileCategoriesComponent implements OnInit {
  private selectedCategories: CategoryItem[] = [];
  @Input() showArrow = false;
  @Input() initialCategories: CategoryItem[] = [];
  @Input() categories: CategoryItem[] = [];
  @Output() lastChildAction = new EventEmitter<CategoryItem[]>();
  @Output() clickAction = new EventEmitter<number>();
  @Output() needUpdate = new EventEmitter<number>();
  @Input() subscribeMode: boolean = false;
  @Input() editMode: boolean = false;
  @Input() copy: boolean = false;
  @Input() copyModeCatalog: boolean = false;
  constructor(
    private storageService: StorageService,
    public env: EnvService,
    public local: DictionaryService,
    public ct: CopyAdvertsService,
    public mainService: MainService,
    public authService: AuthService,
    public shared: SharedService
  ) {
    this.storageService.selectedCategories.subscribe(value => {
      this.selectedCategories = value;
    });
  }

  ngOnInit() {
    // load selected categories if redirect over breadcrumbs
    this.selectedCategories = this.initialCategories;
  }

  checkArrow(c){
    return this.showArrow && c.mark == 2;
  }

  click(level: number, item: CategoryItem) {
    let res = (this.subscribeMode && !this.copyModeCatalog || (this.copyModeCatalog && this.ct.getNewPortalId == this.mainService.getCustId())) && (item.listInnerCat.length == 0)
    if(!res)
    if(!this.copyModeCatalog){
      this.clickAction.emit(level);
      const oldCategory = this.selectedCategories[level]
      if (oldCategory !== undefined) {
        this.selectedCategories.splice(level, 10);
      } else {
        this.selectedCategories.splice(level, 10, item);
      }
      if (item.listInnerCat.length === 0) {
          this.lastChildAction.emit(this.selectedCategories);
      }
      // share categories
      this.storageService.selectedCategories.next(this.selectedCategories);

      setTimeout( ()=> window.scrollTo(0,0) , 500);       
    }
  }

  firstLoad: boolean  = false;
  isShowItem(level: number, item: CategoryItem): boolean {  
    const selected = this.selectedCategories[level];
    const res = selected === undefined || this.isEqual(selected, item);
  //  if(level !==0){
      return res
  /*  }else{
      return true  
    } */
  }

  isShowSubitems(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item);
  }

  isSelected(level: number, item: CategoryItem): boolean {
    return true //this.isEqual(this.selectedCategories[level], item);
  }

  isEqual(item1?: CategoryItem, item2?: CategoryItem): boolean {
    return (item1 && item1.id) === (item2 && item2.id);
  }

  subscribeResult(result: string){
    if(Boolean(result) === false){
      alert('subscription error')
    }
  }


  modifyCat(type: number, categoryObject, level?){
    const data = {
      actionType: type,
      ...categoryObject
    }
    if(level == 10){
      data.id = 0
    }
    if(Object.keys(categoryObject).length === 0 ){
      this.addCategory(data);
    }else{
    Swal.mixin({
        input: 'select',
        confirmButtonText: 'Далее &rarr;',
        cancelButtonText: 'Отмена',
        showCancelButton: true,
        inputOptions: this.getVariants(level),
      }).queue([
        {
          title: 'Управление каталогом',
          text: 'Выберите действие'
        },
      ]).then((result) => {
        if(result.value)
         switch(result.value[0]){
           case "1": {
            this.addCategory(data);
             break;
           }
           case "2": {
            this.addCategory(data,true);
            break;
          }
          case "3": {
            this.deleteCategory(data);
            break;
          }
          default: Swal.fire({
            title: 'Действие неопознанно',
            html: `
              <pre><code>попробуйте еще раз</code></pre>
            `,
            confirmButtonText: 'закрыть'
          })
         }
      })

    }
    event.stopPropagation();
  }

  getVariants(level){
      return level <4 ? {
        '1': 'Добавить подраздел',
        '2': 'Переименовать',
        '3': 'Удалить раздел',
      } : {
        '2': 'Переименовать',
        '3': 'Удалить раздел',
      }
    
  }






  addCategory(data, isEdit: boolean = false){
    Swal.fire({
      title: isEdit ? 'Имя редактируемой категории' : 'Введите имя категории:',
      input: 'text',
      cancelButtonText: 'Отмена',
      inputValue: isEdit ? data.txt : '',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: isEdit ? 'Сохранить' : 'Добавить',
      showLoaderOnConfirm: true,
      inputValidator: function(value) {
        if(value === '') { 
          return 'Введите имя категории'
        } else {
          if(value.trim().length === 0){   
            return 'Введите корректное название'
          } 
        }
      },
      preConfirm: (name) => {   
        const preparedBody = {
          "Id": data.id,
          "Name": name,
          "ImgName": "betls.png",
          "CustIdMain": this.mainService.getCustId()
        }
        return fetch(`${this.env.apiUrl}/api/Catalog`,{
          method: isEdit ? 'PUT' : 'POST',
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify(preparedBody)
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if(result.isConfirmed){
        this.sucessfullyModifyed(isEdit ? 2 : 3);
        this.needUpdate.emit(1);
      }
    });
  }


  deleteCategory(body){
    Swal.queue([{
      title: 'Удаление категории',
      confirmButtonText: 'Удалить',
      text: 'Вы действительно хотите удалить категорию?',
      cancelButtonText: 'Отмена',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return this.mainService.deleteCategoryAsync(body)  
        // return fetch(`${this.env.apiUrl}/api/Catalog`,{
        //   method:'DELETE',
        //   headers: {
        //     'Content-Type': 'application/json;charset=utf-8',
        //     'Authorization': `Bearer ${localStorage.getItem('token')}`
        //   },
        //   body: JSON.stringify({
        //     "id": body.id,
        //     "custIdMain": body.cust_id
        //   })
        // })
          .then(response => {
            if(response === "Раздел не пустой"){

              Swal.queue([{
                title: 'Категория не пустая!',
                confirmButtonText: 'Удалить',
                icon: 'warning',
                text: 'Вы действительно хотите удалить категорию?',
                cancelButtonText: 'Отмена',
                showCancelButton: false,
                showDenyButton: true,
                denyButtonText: `Отмена`,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                  return this.mainService.deleteNotEmptyCategoryAsync(body)  
                    .then(response => {
                        let timerInterval;
                        this.needUpdate.emit(1);
                        Swal.fire({
                          icon: 'success',
                          title: "Категория успешно удалена",
                          showConfirmButton: false,
                          timer: 2000,
                          willClose: () => {
                            clearInterval(timerInterval);
                          }
                        });
                    })
                    .catch(error => {
                      let timerInterval;
                      Swal.fire({
                        icon: 'error',
                        title: "Категория не удалена",
                        showConfirmButton: false,
                        timer: 2000,
                        willClose: () => {
                          clearInterval(timerInterval);
                        }
                      });
                    })
                }
              }])


              // let timerInterval;
              // Swal.fire({
              //   icon: 'error',
              //   title: response + "!",
              //   showConfirmButton: false,
              //   timer: 2000,
              //   willClose: () => {
              //     clearInterval(timerInterval);
              //   }
              // });


            }else{
              let timerInterval;
              this.needUpdate.emit(1);
              Swal.fire({
                icon: 'success',
                title: "Категория успешно удалена",
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  clearInterval(timerInterval);
                }
              });
            }
          })
          .catch(error => {
            let timerInterval;
            Swal.fire({
              icon: 'error',
              title: "Категория не удалена",
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                clearInterval(timerInterval);
              }
            });
          })
      }
    }])
  }

  public sucessfullyModifyed(id){
    let timerInterval
    Swal.fire({
      icon: 'success',
      title: this.getTitle(id),
      showConfirmButton: false,
      timer: 2000,
      willClose: () => {
        clearInterval(timerInterval);
      }
    });
  }
  public notSuccessModifyed(){
    let timerInterval
    Swal.fire({
      icon: 'error',
      title: 'Не могу удалить категорию',
      showConfirmButton: false,
      timer: 2000,
      willClose: () => {
        clearInterval(timerInterval);
      }
    });
    return false;
  }

  getTitle(id){
      switch(Number(id)){
        case 1: return "Категория успешно удалена";
        case 2: return "Категория успешно переименована";
        case 3: return "Категория успешно добавлена";
        default: return "Неизвестное действие";
      };
  }

  public editCat() {
    window.open(this.env.adminUrl + '/categories', '_blank');
  }


}
