import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MainService} from '../shared/services/main.service'
import {StorageService} from '../shared/services/storage.service';
import { CategoriesService } from '../shared/services/categories.service';
import { Observable } from 'rxjs';
import { DictionaryService } from '../shared/services/dictionary.service';
import { MatDialog } from "@angular/material/dialog";
import { EnvService } from 'src/app/env.service';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { AuthService } from '@components/shared/services/auth.service';
import { SharedService } from '@components/shared/services/shared.service';
const Swal = require('sweetalert2');

export interface CategoryItem {
  cust_id: number
  id: number
  isshow: "1"
  listInnerCat: CategoryItem[]
  parent_id: any
  picture: string
  txt: string
}

interface SelectedItem {
  [key: string]: CategoryItem
}

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  loadingImage = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
  isCategoriesLoaded: Observable<boolean>;
  _categories: CategoryItem[] = []
  @Input() set categories(categories: CategoryItem[]) {
    this._categories = categories;
    this.recalculate = true;
  }
  selected: SelectedItem = {}
  selectedCategories: CategoryItem[] = []
  recalculate = true;
  loaded: boolean = false;
  checked: boolean = true;
  @Input() showCatalog = true;
  @Input() showArrow = false;
  @Input() initialCategories: CategoryItem[] = []
  @Output() lastChildAction = new EventEmitter<CategoryItem[]>()
  @Input() subscribeMode: boolean = false;
  @Input() copy;
  @Input() copyModeCatalog: boolean = false;
  @Input() editMode: boolean = false;
  @Output() needUpdate = new EventEmitter<number>();
  //public selectedCats = [];
  constructor(
              private storageService: StorageService,
              private categoryService: CategoriesService,
              public mainService: MainService,
              public authService: AuthService,
              public dialog: MatDialog,
              public local: DictionaryService,
              public ct: CopyAdvertsService,
              public env : EnvService,
              public cp: CopyAdvertsService,
              public shared: SharedService
            ){
      //  cp.getSelectedCategoriesList().subscribe(res=>this.selectedCats = res);
      storageService.selectedCategories.subscribe(value => {
        for (let i = 0; i < value.length; i++) {
          this.selected['lavel' + (i + 1)] = value[i]
        }
      });
      this.isCategoriesLoaded = this.categoryService.isCategoriesLoaded();
      this.categoryService.isCategoriesLoaded().subscribe(res=>this.loaded = !res);
  }


  checkArrow(c){
    return this.showArrow && c.mark == 2;
  }

  onRedraw() {
    if (this.recalculate) {
      this.recalculateHeight()
    }
  }

  test(i:HTMLInputElement){
    
  }

  subscribeResult(result: string){
    if(Boolean(result) === false){
      alert('subscription error')
    }
  }
  ngOnInit() {
    this.selectedCategories = this.initialCategories //.slice(0, -1);
  }

  calculateWidth(id){
    if(document.getElementById("catArea") != null)
    switch(id){
      case 0: {
        document.getElementById("catArea").style.width="auto";
        break;
      }
      case 1: {
        document.getElementById("catArea").style.width="auto";
        break;
      }
      case 2: {
        document.getElementById("catArea").style.width="1280px";
        break;
      }
      case 3: {
        document.getElementById("catArea").style.width="1530px";
        break;
      }
    }
  }

  modifyCat(type: number, categoryObject, level?){
    if(level !== null && level !== undefined && level != 10)
    this.select(level,categoryObject);
    const data = {
      actionType: type,
      ...categoryObject
    }
    if(level == 10){
      data.id = 0
    }
    switch(type.toString()){
      case "1": {
      this.addCategory(data);
        break;
      }
      case "2": {
      this.addCategory(data,true);
      break;
      }
      case "3": {
        this.deleteCategory(data);
        break;
      }
      default: Swal.fire({
        title: 'Действие неопознанно',
        html: `
        <pre><code>попробуйте еще раз</code></pre>
      `,
      confirmButtonText: 'закрыть'
    })
    }
  }

  addCategory(data, isEdit: boolean = false){
    Swal.fire({
      title: isEdit ? 'Имя редактируемой категории' : 'Введите имя категории:',
      input: 'text',
      cancelButtonText: 'Отмена',
      inputValue: isEdit ? data.txt : '',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: isEdit ? 'Сохранить' : 'Добавить',
      showLoaderOnConfirm: true,
      inputValidator: function(value) {
        if(value === '') { 
          return 'Введите имя категории'
        } else {
          if(value.trim().length === 0){   
            return 'Введите корректное название'
          } 
        }
      },
      preConfirm: (name) => {   
        const preparedBody = {
          "Id": data.id,
          "Name": name,
          "ImgName": "betls.png",
          "CustIdMain": this.mainService.getCustId()
        }
        return fetch(`${this.env.apiUrl}/api/Catalog`,{
          method: isEdit ? 'PUT' : 'POST',
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify(preparedBody)
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if(result.isConfirmed){
        this.needUpdate.emit(1);
        Swal.fire({
          icon: 'success',
          title: isEdit ? 'Категория успешно переименована' : 'категория успешно добавлена'
        })
      //  this.categoryService.fetchCategoriesAll(this.mainService.getCustId(),null,"1");
      }
    });
  }


  deleteCategory(body){
    Swal.queue([{
      title: 'Удаление категории',
      confirmButtonText: 'Удалить',
      text: 'Вы действительно хотите удалить категорию?',
      cancelButtonText: 'Отмена',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: async () => {
        return new Promise((resolve,reject)=>{
          this.mainService.deleteCategory(body).subscribe(res=>{
            resolve(()=>{
              Swal.insertQueueStep({
              icon: 'success',
              title: "Категория успешно удалена"
            });
          });
          }, error=>{
            resolve(Swal.insertQueueStep({
              icon: 'error',
              title: 'Не могу удалить категорию'
            }));
          })
        })
      }
    }])
  }

  recalculateHeight() {
    let menu = document.getElementsByClassName('left-menu')[0] as HTMLElement
    let menus = menu.querySelectorAll('ul')
    let height = 0;
    for(let i = 0; i < menus.length; i++) {
      let client = menus[i].clientTop + menus[i].clientHeight;
      if (client > height) {
        height = client
      }
    }
  //  menu.style.minHeight = `${height + 40}px`
    this.recalculate = false;
  }
   previous_level: number = null;
  select(level: number, item: CategoryItem, event?) {
   // this.calculateWidth(level);
    if(event)
    event.stopPropagation()
    if (item.listInnerCat.length === 0) {
      if(this.previous_level !== level){
        if(item && Reflect.has(item,"parent_id") && item.parent_id === "0"){
          this.selectedCategories.length = 0;
        }
        this.selectedCategories.push(item);
        if(!this.editMode)
        this.lastChildAction.emit(this.selectedCategories);
        this.previous_level = level;
      }else{
        this.selectedCategories[this.selectedCategories.length -1] = item;
        if(!this.editMode)
        this.lastChildAction.emit(this.selectedCategories);
      }
    } else {
      if (this.isEqual(this.selectedCategories[level], item)) {
        this.previous_level = level;
        this.selectedCategories.length = 0;
      } else {
        this.previous_level = level;
        this.selectedCategories.splice(level, this.selectedCategories.length, item)
      }
      this.storageService.selectedCategories.next(this.selectedCategories)
      this.recalculate = true
    }
  }

  isSelected(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item)
  }

  isShowSubitems(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item)
  }

  isEqual(item1?: CategoryItem, item2?: CategoryItem): boolean {
    return (item1 && item1.id) === (item2 && item2.id)
  }

  limiter(text: string){
    return text ? text.trim() : text; // ? text.length > 22 ? text.substring(0, 19)+ '...' : text : "";
  }

  editCat(){
    window.open(this.env.adminUrl + '/categories', '_blank');
  }
}
