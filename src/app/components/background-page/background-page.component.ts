import {Component, ElementRef, OnInit, Sanitizer} from '@angular/core';
import {AuthService} from '@components/shared/services/auth.service';
import {MainService} from '../shared/services/main.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import {StorageService} from '../shared/services/storage.service';
import {CategoriesService} from '../shared/services/categories.service';
import { CommonService } from '../shared/services/common.service';
import { EnvService } from 'src/app/env.service';
import { DictionaryService } from '../shared/services/dictionary.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, Validators } from '@angular/forms';
import { StateService } from '@components/shared/services/state.service';
import { ShopInfo } from '@components/shared/interfaces';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-background-page',
  templateUrl: './background-page.component.html',
  styleUrls: ['./background-page.component.scss']
})
export class BackgroundPageComponent implements OnInit {

  fio
  userName
  phone
  cust_id;
  constructor(
    private authService: AuthService,
    private mainService: MainService,
    private stateService: StateService,
    public storageService: StorageService,
    public categoriesService: CategoriesService,
    public commonStore : CommonService,
    private sanitizer: DomSanitizer,
    public env: EnvService,
    public fb: FormBuilder,
    public local: DictionaryService
  ) { 
  }
  public  buildBase64ImageLink = (imageLink: string) => {
    return  "data:image/png;base64," + imageLink;
  };
  public getOrientationWidth(orientation: string){
    switch(orientation){
      case 'H': {
        return '300px'
      };
      case 'V': {
        return '140px';
      };
      default: {
        return '200px';
      }
    }
  }
  public isUploading: boolean = false;
  public selectedBackground: boolean = false;
  public currentProportion = "H";
  public limitOfUploadingImages = 5;
  public isLimitReached: boolean = false;
  public orientations  = [
    { name:'горизонтальная (для дектоп.версии)', value:'H' },  
    { name:'вертикальная (для моб.версии)', value:'V' }
  ];
  public places  = [
    { name:'личный кабинет', value:'L' },  
    { name:'главная страница', value:'G' }
  ];
  public bgConfiguration = this.fb.group({
    orientation: [this.orientations[0].value, Validators.required],
    place: [this.places[0].value, Validators.required],
  });
  public backgrounds = [];
  ngOnInit() {
    this.fio = localStorage.getItem('fio')
    this.stateService.getShopInfo().then(
      (res: ShopInfo) => {
        this.cust_id = res.cust_id;
        this.uploadBackgrounds();
      }
    )
  }
  public isUpdating: boolean = false;
  private uploadBackgrounds(){
    this.isUpdating = true;
    this.mainService.downloadBackgroundMain(this.cust_id,"H").subscribe(
      (res: any)=>{
        this.backgrounds = res;
        if(res.length === this.limitOfUploadingImages){
          this.isLimitReached = true;
        }else{
          this.isLimitReached = false;
        }
      },()=>{},()=> this.isUpdating = false
    )
  }

  public getOrientationProportion(orientation: string){
    switch(orientation){
      case 'H': {
        this.currentProportion = "H";
        break;
      };
      case 'V': {
        this.currentProportion = "V";
        break;
      };
      default: {
        this.currentProportion = "H";
      }
    }
  }

  public deleteSelectedBackground(){
    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.clearImgStack();
  }

  public sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  public imageChangedEvent: any = '';
  public croppedImage: any = null;
  //public file;
  public showImageParams: boolean = false;
  public clearImgStack;
  fileChangeEvent(event: any): void {
   // this.file = event.target.files[0]
      this.imageChangedEvent = event;
      this.clearImgStack = () => {
        event.target.value = ''
      }
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }

  imageLoaded() {
      this.showImageParams = true; 
  }

  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  uploadImage(){
  this.isUploading = true;
  const preparedData = new FormData();
  preparedData.append("Orientation",this.currentProportion);
  preparedData.append("AppCode",this.cust_id);
  preparedData.append("FileName","bg");
  preparedData.append("Image",this.croppedImage.split("data:image/png;base64,")[1]);
    this.mainService.uploadBackground(preparedData).subscribe(()=>{
    this.deleteSelectedBackground();
      this.sucessfullyAdded();
    },()=>{},()=> this.isUploading = false);
  }

  public changeOrientation(): void {
    this.getOrientationProportion(this.bgConfiguration.controls['orientation'].value)
  }

  public changePlace(): void {
    console.log(this.bgConfiguration.controls['place'].value)
  }


  delete(name: string){
    Swal.queue([{
      title: 'Удаление изображения',
      confirmButtonText: 'Удалить',
      text: 'Вы действительно хотите удалить изображение?',
      cancelButtonText: 'Отмена',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: async () => {
        return new Promise((resolve,reject)=>{
          this.mainService.deleteBackgrounds({
            "appCode": this.cust_id,
            "fileName": name
          }).subscribe(res=>{
            this.uploadBackgrounds();
            resolve(()=>{
              Swal.insertQueueStep({
              icon: 'success',
              title: "Изображение успешно удалено"
            });
          });
          }, error=>{
            resolve(Swal.insertQueueStep({
              icon: 'error',
              title: 'Не могу удалить изображение'
            }));
          })
        })
      }
    }])
  }

  public sucessfullyAdded(){
    let timerInterval
    Swal.fire({
      icon: 'success',
      title: "Изображение успешно загруженно!",
      timer: 2000,
     // timerProgressBar: true,
      // willOpen: () => {
      //   Swal.showLoading();
      // },
      willClose: () => {
        clearInterval(timerInterval);
        this.uploadBackgrounds();
      }
    });
  }

}
