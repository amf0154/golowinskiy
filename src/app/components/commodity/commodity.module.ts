import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommodityRoutingModule } from './commodity-routing.module';
import { CommodityComponent } from './commodity.component';
import { NewCommodityComponent } from './new-commodity/new-commodity.component';
import { ImageUploaderComponent } from './shared/image-uploader/image-uploader.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SelectCatalogComponent } from './shared/select-catalog/select-catalog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AudioRecordingService } from './shared/voice-recorder/audio-recording.service';
import { SharedModule } from './../../shared.module';
import { BaseCommodityComponent } from './shared/base-commodity/base-commodity.component';
import { EditCommodityComponent } from './edit-commodity/edit-commodity.component';
import { MapModule } from '@components/map/map.module';

@NgModule({
  declarations: [
    CommodityComponent, 
    NewCommodityComponent, 
    ImageUploaderComponent, 
    SelectCatalogComponent, 
    BaseCommodityComponent,
    EditCommodityComponent,
  ],
  imports: [
    CommonModule,
    CommodityRoutingModule,
    DragDropModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    FormsModule,
    SharedModule,
    MapModule
  ],
 // entryComponents: [AttachAudioToImgComponent],
  providers: [
    AudioRecordingService
  ]
})
export class CommodityModule { }
