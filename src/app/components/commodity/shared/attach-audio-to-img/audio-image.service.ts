import { Injectable } from '@angular/core';
import { AdvertDetail } from '@components/shared/models/advert-list';
import { AuthService } from '@components/shared/services/auth.service';
import { MainService } from '@components/shared/services/main.service';

@Injectable({
  providedIn: 'root'
})
export class AudioImageService {

  constructor(
    public authService: AuthService,
    public mainService: MainService) { }

  initialAudioState = new Map();
  initialVideoState = new Map();
  advert: AdvertDetail = null;
  deleteStack = [];

  setArticle(advert){
    this.advert = advert;
    this.setInitialMediaState(advert.additionalImages);
  }  
  
  setInitialMediaState(state){
    state.filter((a) => (a.audio)).forEach((res) => {
      this.initialAudioState.set(this.removeProtocol(res.audio),res.imageOrder);
    });
    state.filter((a) => (a.video)).forEach((res) => {
      this.initialVideoState.set(res.imageOrder,this.removeProtocol(res.video));
    });
  }
  
  resetData(){
    this.advert = null;
    this.initialAudioState.clear();
    this.initialVideoState.clear();
  }

  removeProtocol(url){
    return url && url.startsWith('https://') ? url.split('https://')[1] : url;
  }

  // check deleted initial images/audios and build body for delete additional audios before update with new links too audio;
  additAudioRemover(){
    const deletingAudiosImageBody = this.deleteStack
    .reduce((delAddImgs,curr) => {
      if(this.initialAudioState.has(this.removeProtocol(curr.link))){
        delAddImgs.push({
          "prc_ID": this.advert.prc_ID,
          "mediaOrder": this.initialAudioState.get(this.removeProtocol(curr.link)),
          "mediaLink": this.removeProtocol(curr.link),
          "appcode": this.mainService.getCustId(),
          "cid": this.authService.getUserId(),
        });
        this.deleteStack.splice(this.deleteStack.indexOf(curr),1);
      }
      return delAddImgs;
    },[]);
    return {
      beforeUpd: deletingAudiosImageBody,
      deleteStack: this.deleteStack
    }
  }
}
