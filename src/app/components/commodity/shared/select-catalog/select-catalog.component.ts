import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CategoryItem } from '@components/categories/categories.component';
import { AuthService } from '@components/shared/services/auth.service';
import { CategoriesService } from '@components/shared/services/categories.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';

@Component({
  selector: 'select-catalog',
  templateUrl: './select-catalog.component.html',
  styleUrls: ['./select-catalog.component.scss']
})
export class SelectCatalogComponent implements OnInit {

  @Input() selectedCatName = null;
  public selectedItem = null;
  public initialCategories: CategoryItem[] = [];
  @Output() selectedCategoryId = new EventEmitter();
  @Input() isOpened: boolean = false;
  constructor(
    public categoriesService: CategoriesService,
    private authService: AuthService,
    private mainService: MainService,
    public dictionary: DictionaryService
  ) { 
    this.categoriesService.fetchCategoriesAll(this.mainService.getCustId(),this.authService.getUserId(),"1");
  }

  public categories = [];
  ngOnInit(): void {
    if(this.selectedCatName) {
      this.selectedItem = {
        txt: this.selectedCatName
      }
    }
    this.categoriesService.mainCategories$.subscribe((res)=>{
      this.categories = res;
    });
    // setTimeout(()=>{
    //   if(this.isOpened && window.innerWidth > 700) {
    //     document.getElementById('menu-select-category').click();
    //   }
    // }, 1000);
  }

  reset(){
    this.selectedItem  = null;
    this.selectedCategoryId.emit(null)
  }

  select(items: any){
    this.selectedItem = items[items.length - 1];
    this.selectedCategoryId.emit(this.selectedItem);
  }

}
