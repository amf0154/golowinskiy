import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { HttpEventType, HttpResponse } from "@angular/common/http";
import { ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnInit, Output } from "@angular/core";
import { ImageUploaderService } from "./image-uploader.service";
import { getBase64Strings } from "exif-rotate-js/lib";
import { MatDialog } from "@angular/material/dialog";
import { AttachAudioToImgComponent } from "../attach-audio-to-img/attach-audio-to-img.component";
import { AudioImageService } from "../attach-audio-to-img/audio-image.service";
import { SharedService } from "@components/shared/services/shared.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { MainService } from "@components/shared/services/main.service";
import { EditAdditImgComponent } from "@components/main-page/edit-addit-img/edit-addit-img.component";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { FormArray, FormBuilder } from "@angular/forms";
import { NotifyService } from "@components/shared/services/notify.service";
import { EnvService } from "src/app/env.service";
const Swal = require("sweetalert2");

@Component({
    selector: "image-uploader",
    templateUrl: "./image-uploader.component.html",
    styleUrls: ["./image-uploader.component.scss"]
})
export class ImageUploaderComponent implements OnInit {
    @Output() isUploadsDone = new EventEmitter();
    @Output() deleteStack = new EventEmitter();
    @Input() audioAttach?: boolean = true;
    @Input() isEditMode?: boolean = false;
    @Input() advertInfo?: any = null;
    @Input("onPaste") $onPaste?: boolean = false;
    @Input() fileInfos = [];
    @Output() updatedAdditImages = new EventEmitter<
        Array<{
            audio: null;
            imageOrder: number;
            index: number;
            link: string;
            t_image: string;
        }>
    >();
    @Input() singleMode? = false;
    selectedFiles: FileList;
    progressInfos = [];
    public progressStatus = new Set();
    message = "";
    indexMainImg: number = 0;

    public imgForm = this.fb.group({
        images: new FormArray([])
    });

    get images(): FormArray {
        return this.imgForm.controls.images as FormArray;
    }

    constructor(
        private uploadService: ImageUploaderService,
        private shared: SharedService,
        public dictionary: DictionaryService,
        private audioImageState: AudioImageService,
        private mainService: MainService,
        private spinner: SpinnerService,
        private readonly fb: FormBuilder,
        private cd: ChangeDetectorRef,
        public toastr: NotifyService,
        public env: EnvService,
        private dialog: MatDialog
    ) {}

    ngOnInit(): void {
        if (this.isEditMode && !this.singleMode) {
            this.fileInfos.forEach((el) => {
                this.images.push(
                    this.fb.group({
                        id: [el.imageOrder],
                        checked: [false]
                    })
                );
            });
        }
    }

    selectUnselectAll(select = true) {
        this.images.controls.map((s) => {
            s.get("checked").setValue(select);
        });
    }

    makeAdvert(): void {
        const selectedImages = this.imgForm.get("images").value;
        const checked = selectedImages.filter((el) => el.checked);
        if (!checked || (checked && !checked.length)) {
            this.toastr.showNotify(this.dictionary.getTranslate("choose-add-images", "Выберите доп. картинки!"));
            return;
        }
        const preparedData = {
            imageOrderStr: checked.map(({ id }) => id),
            prc_ID: this.advertInfo.info.prc_ID,
            appCode: this.advertInfo.form.Appcode
        };
        this.mainService.createImageAdvert(preparedData).subscribe(()=>{
          this.toastr.showNotify(this.dictionary.getTranslate("img-adverts-created", "Обьявление создано!"));
          this.selectUnselectAll(false);
        })
    }

    selectFiles(event, i = null): void {
        this.update(i);
        this.selectedFiles = event.target.files;
        if (this.selectedFiles.length) this.uploadFiles();
    }

    updateAdditImage(event, i = null): void {
        this.selectedFiles = event.target.files;
        const { link } = this.fileInfos[i];
        if (this.selectedFiles.length && link) {
            this.deleteStack.emit(link);
            this.updateImage(i, this.selectedFiles);
        }
    }

    @HostListener("window:paste", ["$event"])
    onPaste($event) {
        if (this.$onPaste) {
            const dt = $event.clipboardData;
            const file = dt.files[0];
            let obj = {
                target: {
                    files: [file]
                }
            };
            if (file && file.type.includes("image")) this.selectFiles(obj);
        }
    }

    uploadFiles(): void {
        this.message = "";

        for (let i = 0; i < this.selectedFiles.length; i++) {
            this.upload(i, this.selectedFiles[i]);
        }
    }

    attachAudio(index: number, audio: string) {
        this.dialog
            .open(AttachAudioToImgComponent, {
                width: "300px",
                data: {
                    link: audio,
                    isEdit: !!this.isEditMode
                }
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.fileInfos[index].audio = result["link"];
                } else {
                    this.fileInfos[index].audio = null;
                }
            });
    }

    attachVideo(index: number, video: string) {
        this.dialog
            .open(EditAdditImgComponent, {
                width: "300px",
                data: {
                    img: {
                        video: video
                    },
                    linkOnly: true
                }
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    const link = result["youtubeLink"] && result["youtubeLink"].length ? result["youtubeLink"] : null;
                    this.fileInfos[index].video = link;
                }
            });
    }

    update(i) {
        if (i !== null) {
            const { link } = this.fileInfos[i];
            if (link) {
                this.deleteStack.emit(link);
                this.fileInfos.splice(i, 1);
            }
        }
    }

    upload(idx, file): void {
        this.isUploadsDone.emit(false);
        getBase64Strings([file], { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then(($file) => {
                    //  this.progressInfos[idx] = { value: 0, fileName: null };
                    const imgName = this.shared.uuidv4() + ".png";
                    this.progressStatus.add(imgName);
                    this.uploadService.upload($file, imgName).subscribe(
                        (event) => {
                            if (event.type === HttpEventType.UploadProgress) {
                                //  this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
                            } else if (event instanceof HttpResponse) {
                                const { data, success } = event.body;
                                if (success) {
                                    if (this.isEditMode) {
                                        const indexes = this.fileInfos.map((i) => i.imageOrder);
                                        const imgObj = {
                                            link: data,
                                            imageOrder: indexes.length ? Math.max(...indexes) + 1 : 0
                                        };
                                        this.images.push(
                                            this.fb.group({
                                                id: [imgObj.imageOrder],
                                                checked: [false]
                                            })
                                        );
                                        this.fileInfos.push(imgObj);
                                    } else {
                                        this.fileInfos.push({
                                            link: data
                                        });
                                    }
                                    this.progressStatus.has(imgName) && this.progressStatus.delete(imgName);
                                    //  this.progressInfos.splice(idx,1);
                                    this.isUploadsDone.emit(!this.progressStatus.size);
                                }
                            }
                        },
                        (err) => {
                            //  this.progressInfos[idx].value = 0;
                            this.message = "Could not upload the file:" + file.name;
                        }
                    );
                });
        });
    }

    updateImage(i, file): void {
        this.spinner.display(true);
        getBase64Strings(file, { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then(($file) => {
                    const imgName = this.shared.uuidv4() + ".png";
                    this.uploadService.upload($file, imgName).subscribe(
                        (event) => {
                            if (event.type === HttpEventType.UploadProgress) {
                                //  this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
                            } else if (event instanceof HttpResponse) {
                                const { data, success } = event.body;
                                if (success) {
                                    // const indexes = this.fileInfos.map((i)=> i.imageOrder);
                                    // data
                                    this.fileInfos[i].link = data;
                                    this.fileInfos[i].t_image = data;
                                }
                            }
                            this.spinner.display(false);
                        },
                        () => this.spinner.display(false)
                    );
                });
        });
    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.fileInfos, event.previousIndex, event.currentIndex);
    }

    getMainImg() {
        if (this.indexMainImg !== null) return this.fileInfos[this.indexMainImg];
    }

    delete(i: number) {
        const { link, imageOrder } = this.fileInfos[i];
        if (link) {
            if (!this.isEditMode) {
                this.mainService.deleteCloudContent(link).subscribe();
            } else {
                this.deleteStack.emit(this.fileInfos[i]);
                if (this.fileInfos[i].audio) {
                    this.audioImageState.deleteStack.push({ link: this.fileInfos[i].audio });
                }

                //   this.images.push(
                //     this.fb.control({
                //         id: [imgObj.imageOrder],
                //         checked: [false]
                //     })
                // );

                const imgAt = this.images.value.findIndex(({ id }) => id === imageOrder);
                if (Number.isInteger(imgAt) && imgAt !== -1) {
                    this.images.removeAt(imgAt);
                }
            }
            this.fileInfos.splice(i, 1);
        }
    }

    deleteVideo(i: number) {
        this.fileInfos[i].video = null;
    }
}
