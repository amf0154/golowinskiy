import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '@components/shared/services/auth.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';
import { SharedService } from '@components/shared/services/shared.service';
import { Observable } from 'rxjs';
import { EnvService } from 'src/app/env.service';

@Injectable({
  providedIn: 'root'
})
export class ImageUploaderService {

  private imgUrl = this.env.apiUrl + '/api/FileS3/UploadImage?shopId='+this.mainService.getCustId();
  private filesUrl = this.env.apiUrl + '/api/FileS3/UploadFile?shopId='+this.mainService.getCustId();

  constructor(
    private http: HttpClient,
    private shared: SharedService,
    private env: EnvService,
    public dictionary: DictionaryService,
    private mainService: MainService,
    private authService: AuthService
    ) { }

  upload(file: File| Blob,filename: string = this.shared.uuidv4(),type = 1): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', new File([file],filename));
    const req = new HttpRequest('POST', `${type === 1 ? this.imgUrl : this.filesUrl}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }


}
