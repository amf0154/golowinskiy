import { Component, Input, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { ChoosePointComponent } from "@components/map/choose-point/choose-point.component";
import { ShopInfo } from "@components/shared/interfaces";
import { Enums } from "@components/shared/models/enums";
import { Models } from "@components/shared/models/models";
import { AuthService } from "@components/shared/services/auth.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { MainService } from "@components/shared/services/main.service";
import { NotifyService } from "@components/shared/services/notify.service";
import { SharedService } from "@components/shared/services/shared.service";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { EnvService } from "src/app/env.service";
import { CommodityService } from "../../commodity.service";
import { AudioImageService } from "../attach-audio-to-img/audio-image.service";
@Component({
    selector: "base-commodity",
    templateUrl: "./base-commodity.component.html",
    styleUrls: ["./base-commodity.component.scss"]
})
export class BaseCommodityComponent implements OnInit {
    constructor(
        public mainService: MainService,
        public commodity: CommodityService,
        public authService: AuthService,
        public environment: EnvService,
        public shared: SharedService,
        public toastr: NotifyService,
        public audioImageService: AudioImageService,
        public dialog: MatDialog,
        public route: ActivatedRoute,
        public router: Router,
        public spinner: SpinnerService,
        public fb: FormBuilder,
        public dictionary: DictionaryService,
        private titleService: Title
    ) {
        
    }

    videoPasted(){
        const value = this.advForm.get('video').value;
        if(value && value.length &&['iframe', 'vk.com/video'].every((attr) => value.includes(attr))){
            const link = value.split('src="')[1].split('"')[0];
            if(link && ["oid", "id", "hash"].every((attr) => link.includes(attr))){
                this.advForm.get('video').setValue(link);
            }
        }
    }

    @Input() data?: any = null;
    @Input() canEditPrice: boolean = false;
    @Output() isEditFinished = new EventEmitter();
    public selectedCategoryId = null;
    public images = []; // additional images;
    public baseImage = []; // main image;
    public attachedAudio? = []; // audioRecord;
    private allImagesUploaded: boolean = true; // is additional images uploaded;
    private baseImageUploaded: boolean = true; // is base image uploaded;
    private audioUploaded: boolean = true; // is audio uploaded;
    public selectedCategory: any = null; // is category selected;
    public submitted: boolean = false; // is form submitted;
    public isEditMode: boolean = false;
    public showMapButton;
    public advForm: FormGroup = this.fb.group({
        Appcode: [this.mainService.getCustId()],
        Catalog: [this.mainService.getCustId()],
        CID: [this.authService.getUserId(), [Validators.required]],
        Ctlg_Name: ["", [Validators.required]],
        Id: [null, [Validators.required]],
        PrcNt: [null],
        TArticle: [null],
        TCost: [null],
        TDescription: [""],
        TName: [""],
        TransformMech: [null],
        TypeProd: [null],
        audio: [null],
        video: [null],
        TImageprev: [null],
        geoname: [],
        latitude: [],
        longitude: []
    });

    get f() {
        return this.advForm.controls;
    }

    ngOnInit(): void {
        this.checkIfEditMode();
        this.mainService.getShopInfo().subscribe((res: ShopInfo) => {
            this.showMapButton = res.isMapShow;
        });
    }

    checkIfEditMode() {
        if (this.data) {
            this.isEditMode = true;
            this.titleService.setTitle(this.data.isCopy ? this.dictionary.getTranslate('copy_adv','Копирование обьявления') : this.dictionary.getTranslate('edit_adv','Редактирование обьявления'));
            this.advForm.setValue(this.data.form);
            if(this.isEditMode && !this.canEditPrice){
                this.advForm.get('TCost').disable()
            }
            if (this.data.info) {
                if (this.data.info.t_imageprev) {
                    this.baseImage = [{ link: this.data.info.t_imageprev }];
                }
                if (this.data.info.additionalImages != 0) {
                    this.images = this.data.info.additionalImages.map((e) => ({ ...e, link: e.t_image }));
                }
                if (this.data.info.mediaLink && this.data.info.mediaLink.trim().length) {
                    const protocol = "https://";
                    const mediaLink = this.data.info.mediaLink;
                    const link = mediaLink.startsWith(protocol) ? mediaLink.split(protocol)[1] : mediaLink;
                    this.attachedAudio = [{ link: link }];
                }
            }
        }else{
            this.titleService.setTitle(this.dictionary.getTranslate('make_new_adv','Создание нового обьявления'));
        }
    }

    instruction(){
        const Swal = require('sweetalert2');
        const body = {
            cust_ID_Main: this.mainService.getCustId(),
            itemname: this.isEditMode ? Enums.ManualType.EDITCOMMODITY : Enums.ManualType.NEWCOMMODITY
          };
          this.mainService.getManual(body)
          .subscribe((res: Models.ManualResponse)=>{
              Swal.fire({
                title: '<strong>Инструкция</strong>',
                html: res.manual,
                customClass: 'swal-wide',
                showCloseButton: true,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText:
                  '<i class="fa fa-thumbs-up"></i> Понятно',
                confirmButtonAriaLabel: 'Thumbs up, great!',
              });
            }, 
            () => this.spinner.display(false), 
            () => this.spinner.display(false)
          );   
    }

    main() {
        if (this.data) {
            this.isEditFinished.emit(2);
        }
    }

    setMainImage() {
        const mainImage = this.baseImage[0];
        this.advForm.controls["TImageprev"].setValue(mainImage ? mainImage.link : "");
    }

    setAudio() {
        if (this.attachedAudio.length) {
            this.advForm.controls["audio"].setValue("https://" + this.attachedAudio[0].link);
        } else {
            this.advForm.controls["audio"].setValue("");
        }
    }

    $isAllImgsUploaded(status: boolean) {
        this.allImagesUploaded = status;
    }

    $isBaseUploaded(status: boolean) {
        this.baseImageUploaded = status;
    }

    private audioStackForDelting = [];
    $audioDeleteStack($event) {
        this.audioStackForDelting.push($event);
    }

    $isAudioUploaded(status: boolean) {
        this.audioUploaded = status;
    }

    $selectedCategoryId(cat) {
        this.selectedCategory = cat;
        this.setSelectedCategory();
    }

    // FOR EDIT MODE (DELETE ADDITIONAL IMAGES AFTER SAVING CHANGES)
    private imgStackForDelting = []; // delete array of these images only after press Save button (edit commodity)
    $imgDeleteStack(image) {
        this.imgStackForDelting.push(image);
    }

    private setSelectedCategory() {
        this.advForm.controls["Ctlg_Name"].setValue(this.selectedCategory ? this.selectedCategory.txt : null);
        this.advForm.controls["Id"].setValue(this.selectedCategory ? this.selectedCategory.id : null);
    }

    isInvalid(ctrName: string) {
        const isInvalid = this.advForm.controls[ctrName].invalid;
        return {
            "is-invalid": this.submitted && isInvalid,
            "is-valid": this.submitted && !isInvalid
        };
    }

    private showError(ms: string){
        this.toastr.showNotify(
            ms,
            "",
            "error",
            2500,
            "center",
            "top"
        );
    }

    countAdditionalImgs: number = 0;
    submit() {
        this.submitted = true;
        this.setAudio();
        this.setMainImage();
        if (this.advForm.invalid) {
            if (this.advForm.controls["Id"].invalid) {
                this.showError(this.dictionary.getTranslate("cat_not_selected", "Вы не выбрали категорию каталога"));
            } else if (this.advForm.controls["TImageprev"].invalid) {
                this.showError(
                    this.dictionary.getTranslate("no_adv_img", "Вы не загрузили основную картинку!")
                );
            } else if (this.advForm.controls["TName"].invalid) {
                this.showError(this.dictionary.getTranslate("no_adv_title", "Вы не ввели наименование!"));
            }
        } else {
            this.spinner.display(true);
            let intervalChecker = setInterval(() => {
                if (this.allImagesUploaded && this.baseImageUploaded && this.audioUploaded) {
                    clearInterval(intervalChecker);
                    if (this.data && !this.data.isCopy) {
                        this.saveEditting();
                    } else {
                        this.uploadAdvert();
                    }
                }
            }, 1000);
        }
    }

    uploadAdvert() {
        const catId = this.data && this.data.isCopy ? this.advForm.get('Id').value : this.selectedCategory.id
        this.commodity.createCommodity(this.advForm.value, this.images, catId).subscribe(
            (prc_id) => {
                this.resetData();

                window.scrollTo({
                    top: 0,
                    behavior: "smooth"
                });
                if(this.data && this.data.isCopy) {
                    this.isEditFinished.emit(true);
                }
                // setTimeout(()=>{
                //   if(prc_id)
                //   this.router.navigate(['/commodity/edit'], { queryParams: { id: prc_id } });
                // },500);
            },
            () => this.spinner.display(false)
        );
    }

    private resetData() {
        this.spinner.display(false);
        if(this.data && this.data.isCopy) {
            this.toastr.showNotify(this.dictionary.getTranslate("adv_copied", "Обьявление успешно скопировано!"));
        }else {
            this.toastr.showNotify(this.dictionary.getTranslate("adv_created", "Обьявление успешно добавлено!"));
        }
        this.images = [];
        this.baseImage = [];
        this.attachedAudio = [];
        this.submitted = false;
        this.imgStackForDelting.length = 0;
        this.advForm.reset();
        this.setSelectedCategory();
        this.advForm.controls["Appcode"].setValue(this.mainService.getCustId());
        this.advForm.controls["Catalog"].setValue(this.mainService.getCustId());
        this.advForm.controls["CID"].setValue(this.authService.getUserId());
    }

    getAdditImgsForUpd() {
        let qIndexes = this.images.map((i) => i.imageOrder);
        const getMinAndRemoveIndex = () => {
            const min = Math.min(...qIndexes);
            qIndexes.splice(qIndexes.indexOf(min), 1);
            return min;
        };
        const additImgs = this.images.map((img, index) => ({
            Catalog: this.authService.getUserId(),
            Id: this.data.form.Id,
            Prc_ID: this.data.info.prc_ID,
            ImageOrder: getMinAndRemoveIndex(),
            TImage: img.link,
            Appcode: this.mainService.getCustId(),
            audio: img.audio,
            video: img.video,
            CID: this.authService.getUserId()
        }));

        return additImgs; //.map((img)=>({...img,video: this.audioImageService.initialVideoState.get(img.ImageOrder)}))
    }

    // FOR EDIT MODE ONLY (link to requestBody for deleting)
    convDelImagesToView() {
        // const getOrderId = (link: string) => {
        //     const image = this.data.info.additionalImages.find((t) => t.t_image == link);
        //     return image ? image.imageOrder.toString() : null;
        // };
        return this.imgStackForDelting.map(({link, imageOrder}) => ({
            link,
            ImageOrder: imageOrder,
            Prc_ID: this.data.info.prc_ID,
            appCode: this.mainService.getCustId(),
            cid: this.authService.getUserId(),
            cust_ID: this.authService.getUserId()
        }));
    }

    saveEditting() {
        const images = this.getAdditImgsForUpd();
        const prcId = this.data.info.prc_ID;
        this.setAudio();
        this.setMainImage();
        this.commodity
            .updateCommodity(this.advForm.getRawValue(), images, this.convDelImagesToView(), this.audioStackForDelting, prcId)
            .subscribe((resp: any) => {
                this.submitted = false;
                this.toastr.showNotify(this.dictionary.getTranslate("adv_updated", "Обьявление успешно обновлено!"));
                this.isEditFinished.emit(true);
                this.commodity.updateAudioImages(images, prcId);
                this.spinner.display(false);
            });
    }

    public choosePosition(): void {
        this.dialog
            .open(ChoosePointComponent, {
                width: "70%",
                data: this.advForm.getRawValue()
            })
            .afterClosed()
            .subscribe((coords: any) => {
                if (coords && coords.length) {
                    const [lat, long] = coords;
                    this.advForm.get("latitude").setValue(lat);
                    this.advForm.get("longitude").setValue(long);
                }
            });
    }
}

