import { Component, EventEmitter, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@components/shared/services/auth.service';
import { DictionaryService } from '@components/shared/services/dictionary.service';
import { MainService } from '@components/shared/services/main.service';
import { SpinnerService } from '@components/shared/services/spinner.service';
import { of } from 'rxjs';
import { CommodityService } from '../commodity.service';
import { AudioImageService } from '../shared/attach-audio-to-img/audio-image.service';

@Component({
  selector: 'app-edit-commodity',
  templateUrl: './edit-commodity.component.html',
  styleUrls: ['./edit-commodity.component.scss']
})
export class EditCommodityComponent implements OnInit, OnDestroy{

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public route: ActivatedRoute,
    private audioImageState: AudioImageService,
    private dialogRef: MatDialogRef<EditCommodityComponent>,
    public spinner: SpinnerService,
    public commodity: CommodityService,
    public mainService: MainService,
    public dictionary: DictionaryService,
    @Inject(MAT_DIALOG_DATA) public params: any,
  ) {
    
  }
  public advId = this.route.snapshot.queryParams['id'];
  public canEditPrice = false;
  public data: any = null; // advertData
  public isLoaded = false;
  ngOnInit(): void {
    this.spinner.display(true);
    this.mainService.retailPrice(this.advId || this.params.advert.prc_ID).subscribe((retail: {retail: number,isChange:boolean})=>{
      this.canEditPrice = retail.isChange;
      const productReq = this.params.advert ? of(this.params.advert) : this.commodity.getProduct(this.advId, this.authService.getUserId(), this.mainService.getCustId());
      productReq.subscribe( (res: any) => {
        if(Number(res.id)){
          this.audioImageState.setArticle(res);
          this.data = {
            form: {
              Appcode: this.mainService.getCustId(),
              Catalog: this.mainService.getCustId(),
              Id: res.id,
              CID: this.authService.getUserId(),
              TArticle: res.ctlg_No,
              TName: res.tName,
              TDescription: res.tDescription ? res.tDescription.replace(/<br>/g,'\n') : res.tDescription,
              TCost: retail.retail, //res.prc_Br,
              TImageprev: '',
              TypeProd: '',
              PrcNt: '',
              TransformMech: '',
              Ctlg_Name: res.ctlg_Name,
              video: res.youtube,
              audio: res.mediaLink,
              geoname: res.geoname,
              latitude: res.latitude,
              longitude: res.longitude,
            },
            info: res,
            isCopy: !!this.params.isCopy
          }; 
        }
        this.isLoaded = true
        this.spinner.display(false);
      });
    });
  }

  public $isEditFinished(status: boolean){
    this.dialogRef.close(status);
  }

  onNoClick(){
    setTimeout(()=> this.dialogRef.close(false));
  }

  ngOnDestroy(): void {
    this.audioImageState.resetData();
  }

}
