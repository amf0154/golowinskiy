import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Enums } from "@components/shared/models/enums";
import { Models } from "@components/shared/models/models";

@Component({
    selector: "app-mode",
    templateUrl: "./catalog-mode.component.html"
})
export class CatalogModeComponent {
    public mode: Enums.CatalogMode;
    constructor(private router: Router) {
        switch (router.url.replace("/", "")) {
            case Models.PageRoutes.Dictionary:
                this.mode = Enums.CatalogMode.DICTIONARY;
                break;
            case Models.PageRoutes.Study:
                this.mode = Enums.CatalogMode.STUDY;
                break;
            case Models.PageRoutes.Repeat:
                this.mode = Enums.CatalogMode.REPEAT;
                break;
            case Models.PageRoutes.Search:
                this.mode = Enums.CatalogMode.SEARCH;
                break;
            default:
                this.router.navigate(["/"]);
        }
    }
}
