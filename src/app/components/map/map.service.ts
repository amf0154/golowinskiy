import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Models } from "@components/shared/models/models";
import { EnvService } from "src/app/env.service";

@Injectable({
    providedIn: "root"
})
export class MapService {
    constructor(
        private http: HttpClient,
        private environment: EnvService,
        private env: EnvService
    ) {}

    public getCoordinates(catId : string | null = null) {
        const byCatId = catId ? '&id='+ catId : '';
        return this.http.get(`${this.environment.apiUrl}/api/Geolocation/Get?cust_ID_Main=${this.env.shopInfo.cust_id + byCatId}`);
    }

    public addCoordinate(params: Models.MapPoint | Models.MapPoint[]) {
        const body: Models.MapPointArray = {
            locations: Array.isArray(params) ? params : [params]
        };
        return this.http.post(`${this.environment.apiUrl}/api/Geolocation/Set`, body);
    }

    public deleteCoordinate(params: Models.MapPoint | Models.MapPoint[]) {
        const body: Models.MapPointArray = {
            locations: Array.isArray(params) ? params : [params]
        };
        return this.http.post(`${this.environment.apiUrl}/api/Geolocation/Delete`, body);
    }
}
