import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import Map from "ol/Map";
import View from "ol/View";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { fromLonLat, transform } from "ol/proj.js";
import { Vector } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source.js";
import { Fill, Stroke, Icon, Style, Text } from "ol/style.js";
import { MatDialog } from "@angular/material/dialog";
import Overlay from "ol/Overlay";
import TileLayer from "ol/layer/Tile";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { MapService } from "./map.service";
import { NotifyService } from "@components/shared/services/notify.service";
import { ActivatedRoute } from "@angular/router";
import { of } from "rxjs";

@Component({
    selector: "app-map",
    templateUrl: "./map.component.html",
    styleUrls: ["./map.component.scss"]
})
export class MapComponent implements OnInit {
    constructor(
        public dictionary: DictionaryService,
        private route: ActivatedRoute,
        private readonly notify: NotifyService,
        private readonly mapService: MapService
    ) {
        this.route.queryParams.subscribe((queryParam: any) => {
            if (queryParam["id"]) {
                this.catId = queryParam["id"];
            } else if (queryParam["latitude"] && queryParam["longitude"]) {
                this.singlePoint = queryParam;
            }
        });
    }

    public map: any;
    public markerSource = new VectorSource();
    public singlePoint = null;
    public points = [];
    public catId = null;
    @ViewChild("overlayElement", { static: false }) overlayElement: ElementRef;

    ngOnInit() {
        this.initilizeMap();
        // this.getPosition().then((location) => {
        //     this.addMyPosition(location);
        // });
    }

    public onMapReady(event: any): void {
        this.map = event.target;
    }

    private addMyPosition(location: []) {
        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "../assets/images/person.png",
                scale: 0.1
            })
        });
        const iconFeature = new Feature({
            geometry: new Point(transform(location, "EPSG:4326", "EPSG:3857")),
            name: "me",
            population: 4000,
            rainfall: 500
        });
        iconFeature.setStyle(iconStyle);

        this.markerSource.addFeature(iconFeature);
    }

    public setHint(name): any {
        return {
            balloonContentHeader: name,
            // balloonContentBody:
            //     "Выставочное пространство, расположенное в бывшем здании Георгиевской электрической станции XIX века. Открытие состоялось в 1995 году по инициативе Юрия Лужкова. По состоянию на 2018 год здание входит в состав Московского выставочного объединения «Манеж», в помещениях проходят выставки русского искусства",
            hintContent: name
        };
    }

    public getZoom() {
        return this.points.length === 1 ? 18 : window.innerWidth < 600 ? 11 : 11;
    }

    initilizeMap() {
        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "../assets/images/point2.png", // https://www.shareicon.net/download/2016/06/10/586233_signal_512x512.png
                scale: 0.05
            })
        });
        const labelStyle = new Style({
            text: new Text({
                font: "16px Calibri,sans-serif",
                overflow: true,
                fill: new Fill({
                    color: "#000"
                }),
                stroke: new Stroke({
                    color: "#2E4",
                    width: 3
                }),
                offsetY: -12
            })
        });
        const style = [iconStyle, labelStyle];

        const request = this.singlePoint ? of([this.singlePoint]) : this.mapService.getCoordinates(this.catId);
        request.subscribe((r: any) => {
            if (r.length) {
                console.log(r);
                this.points = r.map((el) => ({
                    ...el,
                    hint: {
                        balloonContentHeader: el.geoname,
                        iconContent: el.geoname,
                        // balloonContentBody:
                        //     "Выставочное пространство, расположенное в бывшем здании Георгиевской электрической станции XIX века. Открытие состоялось в 1995 году по инициативе Юрия Лужкова. По состоянию на 2018 год здание входит в состав Московского выставочного объединения «Манеж», в помещениях проходят выставки русского искусства",
                        hintContent: el.geoname,
                        openEmptyBalloon: true,
                        balloonPanelMaxMapArea: 0,
                    },
                    point: [el.latitude, el.longitude]
                }));
                r.forEach((point) => {
                    this.addMarker(point.longitude, point.latitude, point.geoname);
                });
            }

            const container = document.getElementById("popup");
            const content = document.getElementById("popup-content");
            const closer = document.getElementById("popup-closer");

            const overlay = new Overlay({
                element: container,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            } as any);

            this.map = new Map({
                target: "map",
                layers: [
                    new TileLayer({
                        source: new OSM()
                    }),

                    new Vector({
                        source: this.markerSource,
                        // new VectorSource({
                        //   features: generatePoint(r),
                        // }),
                        style: function (feature: any) {
                            labelStyle.getText().setText(feature.get("name"));
                            return style;
                        }
                    })
                ],
                overlays: [overlay],
                view: new View({
                    center: fromLonLat(r.length ? [r[0].longitude, r[0].latitude] : [37.618423, 55.751244]),
                    zoom: this.points.length === 1 ? 13 : window.innerWidth < 600 ? 6 : 8
                })
            });

            // this.map.on("singleclick", function (evt: any) {
            //     console.log(evt);
            //     const coordinate = evt.coordinate;
            //     const hdms = toStringHDMS(toLonLat(coordinate));
            //     this.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
            //         var coordinate = evt.coordinate;
            //         if (layer) {
            //             //  content.innerHTML = '<p>Current coordinates are :</p><code>' + hdms +
            //             ("</code>");
            //             content.innerHTML = `<b>${feature.get("name")}</b>`;
            //             overlay.setPosition(coordinate);
            //         }
            //     });
            // });

            // closer.onclick = function () {
            //     overlay.setPosition(undefined);
            //     closer.blur();
            //     return false;
            // };
        });
    }

    private resetMap(): void {
        this.mapService.getCoordinates().subscribe((r: any) => {
            if (r.length) {
                this.points = r;
                this.markerSource.clear();
                r.forEach((point) => {
                    this.addMarker(point.longitude, point.latitude, point.geoname);
                });
            }
        });
    }

    setCenter(long: any, lat: any) {
        var view = this.map.getView();
        view.setCenter(fromLonLat([long, lat]));
        view.setZoom(13);
    }

    private addMarker(lon, lat, name): void {
        const iconFeature = new Feature({
            geometry: new Point(transform([lon, lat], "EPSG:4326", "EPSG:3857")),
            name: name,
            population: 4000,
            rainfall: 500
        });
        this.markerSource.addFeature(iconFeature);
    }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                (resp) => {
                    resolve([resp.coords.longitude, resp.coords.latitude]);
                },
                (err) => {
                    this.notify.showNotify("Не удалось получить ваши координаты");
                    reject(err);
                }
            );
        });
    }
}
