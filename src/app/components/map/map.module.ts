import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularYandexMapsModule } from 'angular8-yandex-maps';
import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';
import { ChoosePointComponent } from './choose-point/choose-point.component';
import { MatButtonModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared.module';

const mapConfig: any = {
  apikey: 'b6545677-da4c-4d90-8e08-a7cd2bf9bd98',
  lang: 'ru_RU',
};


@NgModule({
  declarations: [MapComponent, ChoosePointComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AngularYandexMapsModule.forRoot(mapConfig),
  ],
  entryComponents: [ChoosePointComponent, MapComponent],
  exports: [ChoosePointComponent]
})
export class MapModule { }
