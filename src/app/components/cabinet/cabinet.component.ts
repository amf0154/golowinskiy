import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from '../shared/services/storage.service';

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.scss']
})
export class CabinetComponent implements OnInit , OnDestroy{

  constructor(private storage: StorageService) { }

  ngOnDestroy(): void {
    this.storage.fromCabinetAllItems = true;
  }

  ngOnInit() {

  }

  

}
