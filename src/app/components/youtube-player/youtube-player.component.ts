import { Component, OnInit, Input, HostListener, Output, EventEmitter, ViewChild, OnDestroy } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { SharedService } from "@components/shared/services/shared.service";
import { Observable, Subscription } from "rxjs";

export enum PlayerType {
    DEFAULT = "default",
    YOUTUBE = "youtube",
    VKONTAKTE = "vk",
    BROKEN = "broken"
}

@Component({
    selector: "app-youtube-player",
    templateUrl: "./youtube-player.component.html",
    styleUrls: ["./youtube-player.component.scss"]
})
export class YoutubePlayerComponent implements OnDestroy, OnInit {
    constructor(public readonly sanitizer: DomSanitizer, public shared: SharedService) {}
    public videoLink: any = null;
    @ViewChild("uplayer", { static: false }) public uplayer;
    @Input() inputAction?: Observable<number>;
    public playerType = PlayerType.DEFAULT;
    private $actionSubscriber: Subscription;
    public width: number = window.innerWidth > 1078 ? window.innerWidth * 0.6 : 300;
    public height: number = window.innerWidth > 1078 ? window.innerWidth * 0.4 : 180;
    public YT: any;
    public isReady: boolean = true;
    public video: any;
    public player: any;
    public reframed: Boolean = false;
    public vkLinkParams = [];
    public isRestricted = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    @Output() outputActions = new EventEmitter<{}>(); // 1 - video finished. 2 - repeat On. 3 - repeat Off
    @Input() autoPlay: boolean = false;
    @Input() img: string = null;
    @Input() set link(link: string) {
        if (link.includes("youtu.be") || link.includes("youtube.com")) {
            this.playerType = PlayerType.YOUTUBE;
            this.videoLink = link.includes("youtu.be") ? link.split("youtu.be/")[1] : link.split("v=")[1];
        } else if (link.includes("vk.com")) {
            if (["oid", "id", "hash"].every((attr) => link.includes(attr))) {
                this.videoLink = `https://vk.com/video_ext.php?oid=${this.shared.getParameterByName('oid',link)}&id=${this.shared.getParameterByName('id',link)}&hash=${this.shared.getParameterByName('hash',link)}&hd=2&autoplay=${this.autoPlay ? 1 : 0}`
                this.playerType = PlayerType.VKONTAKTE;
            } else {
                this.playerType = PlayerType.BROKEN;
            }
        } else {
            this.playerType = PlayerType.DEFAULT;
            this.videoLink = link;
        }
        this.isReady = false;
        setTimeout(() => {
            this.isReady = true;
            this.init();
        });
    }
    @HostListener("window:resize", ["$event"]) onResize(event) {
        this.width = window.innerWidth * 0.6;
        this.height = window.innerWidth * 0.4;
    }
    ngOnInit(): void {
        this.actionListener();
    }
    onReady() {
        if (this.autoPlay) {
            this.outputActions.emit({
                key: 2,
                link: this.videoLink
            });
            setTimeout(() => {
                this.uplayer.playVideo();
            }, 500);
        }
    }

    public sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }

    init() {
        if (window["YT"]) {
            window["YT"] = null;
        }
        if (this.playerType == PlayerType.YOUTUBE) {
            const tag = document.createElement("script");
            tag.src = "https://www.youtube.com/iframe_api";
            document.body.appendChild(tag);
            var firstScriptTag = document.getElementsByTagName("script")[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            // window["onYouTubeIframeAPIReady"] = () => this.startVideo();
        }
    }

    /* 4. It will be called when the Video Player is ready */
    onPlayerReady(event) {
        if (this.isRestricted) {
            event.target.mute();
            //  event.target.playVideo();
        } else {
            if (this.autoPlay) event.target.playVideo();
        }
    }

    /* 5. API will call this function when Player State changes like PLAYING, PAUSED, ENDED */
    onPlayerStateChange(event) {
        switch (event.data) {
            // case window["YT"].PlayerState.PLAYING:
            //     if (this.cleanTime() == 0) {
            //         console.log("started " + this.cleanTime());
            //     } else {
            //         console.log("playing " + this.cleanTime());
            //     }
            //     break;
            // case window["YT"].PlayerState.PAUSED:
            //     if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
            //         console.log("paused" + " @ " + this.cleanTime());
            //     }
            //     break;
            case window["YT"].PlayerState.ENDED:
                this.outputActions.emit({
                    key: 1,
                    link: this.videoLink
                });
                break;
        }
    }

    private actionListener() {
        if (this.inputAction instanceof Observable)
            this.$actionSubscriber = this.inputAction.subscribe((actionType) => {
                // 1 - just repeat
                switch (actionType) {
                    case 1: {
                        this.uplayer.playVideo();
                        break;
                    }
                    default:
                        null;
                }
            });
    }

    public ngOnDestroy() {
        if (this.$actionSubscriber) this.$actionSubscriber.unsubscribe();
    }
}
