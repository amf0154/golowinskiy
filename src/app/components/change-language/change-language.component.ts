import { Component, OnInit } from '@angular/core';
import { DictionaryService } from '../shared/services/dictionary.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.css']
})
export class ChangeLanguageComponent implements OnInit {
  public languageForm: FormGroup;
  constructor(
    public locale: DictionaryService,
    private fb: FormBuilder,
  ) { 
    const {value} = this.locale.selectedLocalize
    const $locale = localStorage.getItem("locale");
    this.languageForm = this.fb.group({
      code: [value ? value : $locale, [Validators.required]],
    });
    this.languageForm.valueChanges.subscribe(res=>{
      this.locale.changeLanguage(res.code);
    })
  }

  ngOnInit() {}

}
