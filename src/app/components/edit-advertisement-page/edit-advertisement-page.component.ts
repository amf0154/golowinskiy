import { Component, OnInit, ViewChild, ElementRef, HostListener, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Message } from "@components/shared/models/message.model";
import { MainService } from "../shared/services/main.service";
import { BehaviorSubject, of } from "rxjs";
import { CommonService } from "../shared/services/common.service";
import { EnvService } from "src/app/env.service";
import { getBase64Strings } from "exif-rotate-js/lib";
import { DictionaryService } from "../shared/services/dictionary.service";
import { AuthService } from "../shared/services/auth.service";
import { AwsImageResponse } from "../shared/interfaces";
import { StorageService } from "../shared/services/storage.service";
import { CategoriesService } from "../shared/services/categories.service";
import { CategoryItem } from "../categories/categories.component";
import { AdvertDetail } from "@components/shared/models/advert-list";
import { Enums } from "@components/shared/models/enums";
import { Models } from "@components/shared/models/models";
import { SpinnerService } from "@components/shared/services/spinner.service";
const Swal = require("sweetalert2");

export interface ImageDataInterface {
    src: string;
    name: string;
    blob?: Blob;
    file: File;
}

@Component({
    selector: "edit-advert",
    templateUrl: "./edit-advertisement-page.component.html",
    styleUrls: ["./edit-advertisement-page.component.scss"]
})
export class EditAdvertisementPageComponent implements OnInit {
    public _imageNotLoaded = new BehaviorSubject<boolean>(false);
    @Input() advert?: AdvertDetail;
    @Input() prcId?: number | string;
    @Input() catId?: number | string;
    @Output("close") closer = new EventEmitter<boolean>();
    additionalImageLimit = 50;
    AppCode: any;
    urlsImages = [];
    imageIndexList = [];
    imageIndex: any;
    data_form: any = {
        Catalog: null, //nomer catalog
        Id: null, // post categories/
        Ctlg_Name: null, //Ctlg_Name
        TArticle: null, //Article
        TName: null, //input form
        TDescription: null, //input form
        TCost: null, //input form
        TImageprev: null, // input form
        Appcode: null, //post Gallery/
        TypeProd: null, //input form
        PrcNt: null, //input form
        TransformMech: null, //input form
        CID: null, // userId for auth
        video: null,
        audio: null
    };
    showPhone: boolean = false;
    loadingImage = "";
    form: FormGroup;
    message: Message;
    loadingSpinner = "";
    showModal: boolean = true;
    additionalImagesArray: any;
    user: any;

    Gallery = [];

    appCode;
    idCategorie;
    Ctlg_Name;
    article;
    prc_ID;
    element: AdvertDetail = null;
    elGallery = [];

    is_edit: boolean;
    initialCategories: CategoryItem[] = [];
    srcImg: any = { src: null, name: null, link: null };
    srcAddImg = "";

    TName;
    TDescription;
    TCost;
    youtube;
    id;
    dataForm;
    srcImgName;

    showSpinner: boolean = false;
    uploadImg: boolean = true;
    categories: CategoryItem[] = [];
    selectedFiles: File = null;
    files: any;
    urls = [];
    formDataImages: any;
    imageName: string;
    filesImg = [];
    apiRoot: string = "";
    urlsAdd = [];
    dataAddImg;
    isCanPromo: boolean = true;
    cust_id: string = "";
    fio: string = "";
    userName: string | boolean = null;
    phone: string | boolean = null;
    progress: string = "";
    useRotate: boolean = false;
    showForm: boolean = false;
    accessDenied: boolean = false;
    doCopy: boolean = false;
    audio: string = null;
    @ViewChild("mainResizer", { static: false }) mainResizer: ElementRef;
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private mainService: MainService,
        private spinner: SpinnerService,
        public storageService: StorageService,
        public categoriesService: CategoriesService,
        public commonStore: CommonService,
        private env: EnvService,
        private authService: AuthService,
        public local: DictionaryService
    ) {
        this.is_edit = true;
        this.loadingSpinner = this.commonStore.loadingImageSpinner;
        this.loadingImage = this.commonStore.loadingLittleRedSpinner;
        this.apiRoot = this.env.apiUrl;
    }
    isRetailDisabled: boolean = false;

    ngOnInit() {
        this.checkQuery();
        this.fio = localStorage.getItem("fio");
        this.userName = false; //localStorage.getItem('userName')
        this.phone = false; //localStorage.getItem('phone')
        // this.id = this.route.snapshot.params['id'];
        // this.prc_ID = this.route.snapshot.params['idProduct'];
        this.form = this.fb.group({
            TArticle: "",
            TName: "",
            TDescription: "",
            TCost: "",
            TImageprev: "",
            TypeProd: "",
            PrcNt: "",
            TransformMech: "",
            Ctlg_Name: "",
            youtube: "",
            audio: ""
        });
        this.showSpinner = true;
        this.mainService.getShopInfo().subscribe(
            (res) => {
                this.AppCode = res.cust_id;
                this.categoriesService.fetchCategoriesAll(res.cust_id);
                if (this.authService.getUserId() == res.cust_id || res.isEditing) {
                    this.mainService
                        .retailPrice(this.prc_ID)
                        .subscribe((retail: { retail: number; isChange: boolean }) => {
                            this.isRetailDisabled = !retail.isChange;
                            const getAdvert = this.advert
                                ? of(this.advert)
                                : this.mainService.getProduct(this.prc_ID, this.authService.getUserId(), res.cust_id);
                            getAdvert.subscribe((res: AdvertDetail) => {
                                this.accessDenied = res.creater_ID
                                    ? !(this.authService.getUserId() == res.creater_ID)
                                    : false;
                                this.showForm = res.creater_ID ? this.authService.getUserId() == res.creater_ID : true;
                                this.form.setValue({
                                    TArticle: "",
                                    TName: res.tName,
                                    TDescription: res.tDescription
                                        ? res.tDescription.replace(/<br>/g, "\n")
                                        : res.tDescription,
                                    TCost: retail.retail, //res.prc_Br,
                                    TImageprev: "",
                                    TypeProd: "",
                                    PrcNt: "",
                                    TransformMech: "",
                                    Ctlg_Name: res.ctlg_Name,
                                    youtube: res.youtube,
                                    audio: res.mediaLink
                                });
                                this.isFileLoaded = this.videofile = res.youtube.includes("cloudfront");
                                this.showSpinner = false;
                                this.srcImg = {
                                    src: `https://${res.t_imageprev}`,
                                    name: res.t_imageprev,
                                    link: res.t_imageprev
                                };
                                this.srcImgName = res.t_imageprev;
                                this.element = res;
                                this.Ctlg_Name = res.ctlg_Name;
                                this.article = res.ctlg_No;
                                this.idCategorie = res.id;
                                this.TName = res.tName;
                                this.TDescription = res.tDescription;
                                this.TCost = res.prc_Br;
                                this.youtube = res.youtube;
                                this.audio = res.mediaLink && res.mediaLink.length > 10 ? res.mediaLink : null;
                                this.additionalImagesArray = res.additionalImages;
                                if (res.additionalImages && res.additionalImages.length != 0) {
                                    for (let i in res.additionalImages) {
                                        this.urls[i] = {
                                            src: `https://${res.additionalImages[i].t_image}`,
                                            name: res.additionalImages[i].t_image,
                                            link: res.additionalImages[i].t_image,
                                            index: res.additionalImages[i].imageOrder
                                        };
                                        this.urlsImages.push(res.additionalImages[i]);
                                    }
                                }
                                setTimeout(() => {
                                    res.additionalImages.forEach((element) => {
                                        if (document.getElementById(element.t_image))
                                            document.getElementById(element.t_image).style.display = "none";
                                    });
                                });
                            });
                        });
                } else {
                    this.router.navigate(["/"]);
                }
            },
            (error) => alert(error.error.message)
        );
        this.message = new Message("danger", "");
        if (localStorage.getItem("phone")) {
            this.showPhone = true;
        } else {
            this.showPhone = false;
        }
    }

    manual() {
        const Swal = require("sweetalert2");
        const body = {
            cust_ID_Main: this.mainService.getCustId(),
            itemname: Enums.ManualType.EDITCOMMODITY
        };
        this.mainService.getManual(body).subscribe(
            (res: Models.ManualResponse) => {
                Swal.fire({
                    title: "<strong>Инструкция</strong>",
                    html: res.manual,
                    customClass: "swal-wide",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Понятно',
                    confirmButtonAriaLabel: "Thumbs up, great!"
                });
            },
            () => this.spinner.display(false),
            () => this.spinner.display(false)
        );
    }

    checkQuery() {
        if (this.advert) {
            this.id = this.catId;
            this.prc_ID = this.prcId;
        } else
            this.route.queryParams.subscribe((queryParam: any) => {
                if (queryParam["category"] && queryParam["adv"]) {
                    this.id = queryParam["category"];
                    this.prc_ID = queryParam["adv"];
                    this.doCopy = queryParam["copy"] ? true : false;
                } else {
                    this.router.navigate(["/"]);
                }
            });
    }

    @HostListener("window:paste", ["$event"])
    onPaste($event) {
        const dt = $event.clipboardData;
        const file = dt.files[0];
        let obj = {
            target: {
                files: [file]
            }
        };
        if (file && file.type.includes("image")) this.addNewAdditionalImgs(obj);
    }

    ngAfterContentInit() {
        document.body.style.backgroundColor = "#ffffff";
    }
    showRecorder: boolean = false;
    showHideRecorder() {
        this.showRecorder = !this.showRecorder;
    }

    recordFileBlob = null;
    audioRecord($event) {
        this.recordFileBlob = $event;
    }

    fileToUpload: File = null;
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }

    clear() {
        this.fileToUpload = null;
    }

    private uuidv4() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    }

    deleteAudio() {
        if (this.audio != null && this.audio.includes("cloudfront")) {
            let body = {
                title: this.audio.replace(/^https?:\/\//, ""),
                isCloud: true
            };
            this.deleteAudioRecord(body);
        } else {
            this.data_form.audio = null;
            this.audio = null;
            this.recordFileBlob = null;
        }
    }

    private showMessage(
        text: string,
        type: string = "danger",
        redirect: boolean = true,
        showSpinner: boolean = false,
        hideMessage: boolean = true
    ) {
        this.message = new Message(type, text, showSpinner);
        window.setTimeout(() => {
            if (hideMessage) this.message.text = "";
            if (this.message.type == "success" && redirect) {
                if (this.advert) this.closer.emit(true);
                else this.router.navigate(["/cabinet/categories", this.id, "products", this.prc_ID]);
            }
        }, 2000);
    }

    onFileSelected(event, isTurning: boolean = false) {
        const selectedFile = isTurning ? event : <File>event.target.files[0];
        document.getElementById(this.srcImg.name).style.display = "block";
        if (this.srcImg.name) {
            this.mainService.deleteCloudContent(this.srcImg.name).subscribe((res) => {});
            this.uploadedImageStatuses.delete(this.srcImg.name);
        }
        this.srcImg = {
            src: this.loadingSpinner,
            name: this.uuidv4(),
            link: null
        };
        getBase64Strings(event.target.files, { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then((file) => {
                    this.srcImg = {
                        src: res[0],
                        name: this.uuidv4() + ".png",
                        link: null
                    };
                    this.srcImgName = this.srcImg.name;
                    this.srcImg = { src: res[0], name: this.srcImgName, file, link: null };
                    this.uploadImages(this.srcImg, null);
                    this.uploadImg = false;
                });
        });
    }

    loadFile(files: File, callback: (src: string, name: string) => any) {
        const file = files;
        const reader = new FileReader();
        reader.onload = () => callback(reader.result.toString(), file.name);
        reader.readAsDataURL(file);
    }

    arrayOfAdditionalImages = new Array<FormData>();
    sizeCounter: number = 0;
    deletingLink;
    string = null;
    onFilesMultipleSelected(event, i?, isTurning: boolean = false) {
        const previousImgName = Number.isInteger(i) ? this.urls[i].name : "";
        this.selectedFiles = <File>event.target.files[0];
        const name = this.uuidv4() + ".png";
        this.sizeCounter += event.target.files[0].size;
        this.commonStore.addImagesStack.next(this.sizeCounter);
        const obj = { src: this.loadingSpinner, name: null };
        if (Number.isInteger(i)) {
            this.deletingLink = this.urls[i].link;
            const index = this.urls[i].index;
            this.urls[i] = obj;
            this.urls[i].index = index;
        } else {
            this.deletingLink = null;
            this.urls.push(obj);
        }
        getBase64Strings(event.target.files, { maxSize: 2400 }).then((res) => {
            fetch(res[0])
                .then((res) => res.blob())
                .then((file) => {
                    const obj = {
                        src: res[0],
                        name,
                        file
                    };
                    if (Number.isInteger(i)) {
                        // upd current image
                        this.urls[i].src = obj.src;
                        this.urls[i].name = obj.name;
                    } else {
                        // new image
                        this.urls[this.urls.length - 1] = obj;
                    }
                    this.imageIndex = Number.isInteger(i) ? this.getImageOrderId(previousImgName) : this.urls.length;
                    this.imageIndexList.push(this.imageIndex);
                    this.filesImg.push(name);
                    this.srcImgName = this.srcImg.name;
                    this.uploadImages(obj, i);
                    this.uploadImg = false;
                });
        });
    }

    addNewAdditionalImgs(event) {
        this.deletingLink = null;
        if (event.target.files.length < this.additionalImageLimit + 1 - this.urls.length) {
            setTimeout(() => {
                for (let i = 1; i <= event.target.files.length; i++) {
                    getBase64Strings([event.target.files[i - 1]], { maxSize: 2400 }).then((res) => {
                        fetch(res[0])
                            .then((res) => res.blob())
                            .then((file) => {
                                const name = this.uuidv4() + ".png";
                                const obj = {
                                    src: res[0],
                                    name,
                                    file
                                };
                                this.urls.push(obj);
                                const lockedIndexes = this.urls.map((el) => el.index).filter((el) => el != undefined);
                                let freeIndexes = Array.from(Array(this.additionalImageLimit).keys()).filter(
                                    (el) => !lockedIndexes.includes(el)
                                );
                                this.imageIndex = this.urls.length;
                                let indexForUploadingImg;
                                if (lockedIndexes.includes(this.imageIndex - 1)) {
                                    indexForUploadingImg = Math.max.apply(Math, lockedIndexes) + 1;
                                    this.urls[this.urls.length - 1].index = freeIndexes[0];
                                    this.urls[this.urls.length - 1].index = indexForUploadingImg;
                                } else if (!lockedIndexes.includes(this.imageIndex - 1) && this.urls.length - 1 < 10) {
                                    this.urls[this.urls.length - 1].index = this.urls.length - 1;
                                    indexForUploadingImg = this.urls.length - 1;
                                }
                                this.imageIndexList.push(indexForUploadingImg);
                                this.filesImg.push(name);
                                this.srcImgName = this.srcImg.name;
                                this.uploadImages(obj, "multi", false, this.urls.length - 1);
                                this.uploadImg = false;
                            });
                    });
                }
            });
        } else {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Максимальное количество доп.картинок не может быть больше 10",
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    uploadedImageStatuses = new Map();
    showStatus(name, loaded, total) {
        const percent = Math.ceil((loaded / total) * 100);
        const element = document.getElementById(name);
        element ? ((element as any).value = percent) : "";
    }

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(","),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, { type: mime });
    }

    uploadImages(additionalImagesData, index, fromFile: boolean = false, multiIndex = 1) {
        const rotateButton = document.getElementById(`rotate_${additionalImagesData.name}`);
        const deleteButton = document.getElementById(`delete_${additionalImagesData.name}`);
        const formData = new FormData();
        const file = !fromFile ? this.dataURLtoFile(additionalImagesData.src, additionalImagesData.name) : null;
        formData.append("file", !fromFile ? file : additionalImagesData.file);
        this.uploadedImageStatuses.set(additionalImagesData.name, false);
        this._imageNotLoaded.next(true);
        this.mainService
            .uploadImageXhrCloud(formData, name, fromFile ? this.showStatusFile : this.showStatus)
            .subscribe(
                (res: AwsImageResponse) => {
                    switch (index) {
                        case null: {
                            this.srcImg.link = res.data;
                            break;
                        }
                        case "new": {
                            this.urls[this.urls.length - 1].link = res.data;
                            break;
                        }
                        case "multi": {
                            this.urls[multiIndex].link = res.data;
                            break;
                        }
                        case index: {
                            this.mainService.deleteCloudContent(this.deletingLink).subscribe((res) => {});
                            this.urls[index].link = res.data;
                        }
                    }
                    if (index == null) {
                        if (!fromFile) {
                            this.srcImg.link = res.data;
                        } else {
                            this.isFileLoaded = true;
                            this.form.controls["youtube"].setValue(res.data);
                        }
                    }
                    this.uploadedImageStatuses.set(additionalImagesData.name, true);
                    const progressBar = document.getElementById(additionalImagesData.name);
                    if (rotateButton) rotateButton.style.display = "block";
                    if (deleteButton) deleteButton.style.display = "block";
                    if (progressBar) progressBar.style.display = "none";
                    this._imageNotLoaded.next(!Array.from(this.uploadedImageStatuses.values()).every((obj) => obj));
                },
                (err) => {
                    Swal.fire({
                        position: "center",
                        icon: "warning",
                        title: "Неудачнаая загрузка. Попробуйте еще раз",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            );
    }

    deleteImages(url) {
        const stackIndexOdFeletingElement = this.imageIndexList.indexOf(
            this.imageIndexList.find((el) => el == url.index)
        );
        const imgNumber = this.getImageOrderId(url.name);
        const index = this.urls.indexOf(url);
        if (Number.isInteger(imgNumber)) {
            const preparedObj = {
                cust_ID: Number(this.AppCode),
                Prc_ID: this.prc_ID,
                ImageOrder: imgNumber.toString(),
                appCode: this.AppCode,
                cid: this.authService.getUserId()
            };
            this.mainService.deleteCloudContent(url.link).subscribe((res) => {});
            this.mainService.deleteAdditionalImg(preparedObj);
            if (index !== -1) this.urls.splice(index, 1);

            if (stackIndexOdFeletingElement != -1) {
                this.imageIndexList.splice(stackIndexOdFeletingElement, 1);
            }
        } else {
            if (index !== -1) this.urls.splice(index, 1);
            if (stackIndexOdFeletingElement != -1) {
                this.imageIndexList.splice(stackIndexOdFeletingElement, 1);
                this.mainService.deleteCloudContent(url.link).subscribe((res) => {});
            }
        }
    }

    getImageOrderId(name) {
        const res = this.urlsImages.filter((img) => img.t_image == name);
        return res ? (res.length !== 0 ? res[0].imageOrder : null) : null;
    }

    onSubmit() {
        this.showMessage("Идет редактирование ", "primary", false, true, false);
        let intervalChecker = setInterval(() => {
            if (this._imageNotLoaded.value == false) {
                this.sendData();
                clearInterval(intervalChecker);
            }
        }, 1000);
    }

    sendData(showAlertResult = true) {
        if (!this.accessDenied) {
            const formData = this.form.value;
            this.cust_id = this.AppCode;
            if (!showAlertResult) {
                this.audio = null;
            }
            this.data_form = {
                Catalog: this.cust_id, //nomer catalog
                Id: this.idCategorie, // post categories/
                Ctlg_Name: this.Ctlg_Name, //Ctlg_Name
                TArticle: this.article, //Article
                TName: formData.TName, //input form
                TDescription: formData.TDescription, //input form
                TCost: formData.TCost, //input form
                TImageprev: this.srcImg.link, // input form
                Appcode: this.cust_id, //post Gallery/
                TypeProd: formData.TypeProd, //input form
                PrcNt: formData.PrcNt, //input form
                TransformMech: formData.TransformMech, //input form
                CID: this.authService.getUserId(), // userId for auth
                video: formData.youtube,
                audio: this.audio == null ? "" : this.audio
            };
            if (this.isCanPromo) {
                if (this.recordFileBlob != null || this.fileToUpload != null) {
                    let data = new FormData();
                    if (this.fileToUpload != null) {
                        const newFileName = this.uuidv4();
                        this.fileToUpload = new File([this.fileToUpload], "attached_" + newFileName + ".mp3", {
                            type: this.fileToUpload.type
                        });
                    }
                    let audioRecord;
                    if (this.recordFileBlob != null)
                        audioRecord = new File([this.recordFileBlob], "voiceRecord_" + this.uuidv4() + ".mp3");
                    data.append("file", this.recordFileBlob != null ? audioRecord : this.fileToUpload);
                    this.mainService.uploadFileAws(data).subscribe(
                        (res: any) => {
                            const link = "https://" + res.data;
                            this.data_form.audio = link;
                            this.saveData(showAlertResult);
                        },
                        (err) => {
                            this.saveData(showAlertResult);
                        }
                    );
                } else {
                    this.saveData(showAlertResult);
                }
            } else this.showMessage("Объявление не было отредактировано", "danger");
        } else {
            this.showMessage("Ошибка доступа: Объявление не было отредактировано", "danger");
        }
    }

    saveData(showAlert: boolean) {
        this.mainService.editProduct(this.data_form).subscribe(
            (res: { result: boolean }) => {
                if (res.result == true) {
                    if (this.filesImg.length != 0) {
                        for (let i in this.imageIndexList) {
                            this.dataAddImg = {
                                Catalog: this.cust_id,
                                Id: this.idCategorie,
                                Prc_ID: this.prc_ID,
                                ImageOrder: this.imageIndexList[i],
                                TImage: this.urls.find((el) => el.index == this.imageIndexList[i]).link,
                                Appcode: this.cust_id,
                                CID: this.authService.getUserId()
                            };
                            this.mainService
                                .editAdditionalImg(this.dataAddImg)
                                .subscribe(() => (showAlert ? this.advertUpdated() : null));
                        }
                    } else showAlert ? this.advertUpdated() : null;
                } else this.showMessage("Объявление не было отредактировано", "danger");
            },
            (error) => this.showMessage(error, "danger")
        );
    }

    advertUpdated() {
        this.showMessage("Объявление было успешно отредактировано", "success");
        this.mainService.moveToMainAdditionalPicture(this.prc_ID, this.cust_id);
    }

    setControlButton(buttonVariable: string, name: string, status: boolean) {
        const rotateButton = document.getElementById(`${buttonVariable}_${name}`);
        if (rotateButton) rotateButton.style.display = status ? "block" : "none";
    }

    deleteAudioRecord(body) {
        Swal.queue([
            {
                title: "Удаление аудиозаписи",
                confirmButtonText: "Удалить",
                text: "Вы действительно хотите удалить аудиозапись?",
                cancelButtonText: "Отмена",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return this.mainService.deleteCloudContent(body.title).subscribe((response: any) => {
                        let timerInterval;
                        Swal.fire({
                            icon: "success",
                            title: "Аудиозапись успешно удалена",
                            showConfirmButton: false,
                            timer: 2000,
                            willClose: () => {
                                this.data_form.audio = null;
                                this.audio = null;
                                this.recordFileBlob = null;
                                clearInterval(timerInterval);
                                //  this.sendData(false);
                                // ссылка на эедпоинт удаления ссылки аудио с обьявления.
                                //  this.audio = null
                                this.mainService.deleteMediaLink(this.prc_ID);
                            }
                        });
                    });
                }
            }
        ]);
    }

    previousItem: {} = null;
    idCategory: any = null;
    showCatalog: boolean = true;
    categorySelect(items: CategoryItem[]) {
        if (!this.previousItem) {
            this.previousItem = items[items.length - 1];
        } else {
            if (JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length - 1])) {
                this.previousItem = items[items.length - 1];
                //   this.breadcrumbsClick();
            }
        }
        setTimeout(() => {
            this.categories = items;
            let item = items[items.length - 1];
            this.idCategory = item.id;
            this.showCatalog = false;
        });
    }

    refreshCat: boolean = false;
    breadcrumbsClick(i) {
        this.storageService.setCategories(this.categories.slice(0, i));
        this.categories = [];
        this.storageService.breadcrumbFlag = true;
        this.initialCategories = this.storageService.getCategories();
        this.refreshCat = true;
        setTimeout(() => (this.refreshCat = false));
        //  this.form.controls['Categories'].setValue('');
        this.idCategory = "";
    }

    // upload video

    deleteVideo(val: any) {
        if (val == 1) {
            this.uploadedImageStatuses.delete(this.videofile.name);
            this.form.controls["youtube"].setValue("");
            this.videofile = null;
            this.isFileLoaded = false;
        } else {
            this.mainService.deleteCloudContent(this.form.controls["youtube"].value).subscribe((res) => {});
            this.uploadedImageStatuses.delete(this.videofile.name);
            this.form.controls["youtube"].setValue("");
            this.videofile = null;
            this.isFileLoaded = false;
        }
    }

    videofile: any = null;
    isFileLoaded: boolean = false;
    uploadVideo(event) {
        const file = <File>event.target.files[0];
        if (Number((event.target.files[0].size / 1024 / 1024).toFixed(0)) > 105) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Размер файла не должен превышать 100мб",
                showConfirmButton: false,
                timer: 2000
            });
            setTimeout(() => {
                event.target.value = "";
            }, 1000);
        } else {
            const filename = this.uuidv4() + ".mp4";
            this.videofile = { src: null, name: filename, file: new File([file], filename), link: null };
            this.uploadImages(this.videofile, null, true);
        }

        // event.target.value = "";
        if (this.videofile) {
            // this.mainService.deleteCloudContent(this.mainImageData.link).subscribe(res=>{});
            // this.uploadedImageStatuses.delete(this.mainImageData.name);
            // document.getElementById(this.mainImageData.name).style.display = "block";
        } else {
            // this.videoProgess.nativeElement.style.display = "block";
        }
    }

    showStatusFile(name, loaded, total, xhr) {
        const percent = Math.ceil((loaded / total) * 100);
        const element = document.getElementById(name);
        const viewPercent = document.getElementById("fileProgress");
        document.getElementById("cancelUpload").onclick = function () {
            if (xhr) xhr.abort();
            xhr = null;
        };
        if (element) element.style.maxWidth = percent + "%";
        viewPercent.innerText = percent + "%";
    }
}
