import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from "@angular/core";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { NotifyService } from "@components/shared/services/notify.service";
import { SharedService } from "@components/shared/services/shared.service";
import { Howl, Howler } from "howler";
import { Observable, Subscription } from "rxjs";
import { AudioService } from "./recorder.service";
Howler.autoUnlock = true;
Howler.html5PoolSize = 100;
@Component({
    selector: "audio-player",
    templateUrl: "./recorder.component.html",
    styleUrls: ["./recorder.component.scss"]
})
export class RecorderComponent implements OnInit, OnDestroy {
    @Output() getRecord = new EventEmitter<Blob>();
    @Input() set file(file: any) {
        this._file = file;
        // this.currentAudio.stop();
        if (this.currentAudio instanceof Howl) {
            this.currentAudio.stop();
            this.initPlayer();
        }
    }
    @Input() autoPlay: any = false;
    @Input() hasDownload: any = false;
    @Input() copyLink: any = true;
    @Input() short?: any = false;
    @Input() inputAction?: Observable<number>;
    @Output() outputAction = new EventEmitter<number>(); // 1 - audio finished. 2 - repeat On. 3 - repeat Off
    @Input() color: string = "#F0F1F3";
    public vocal_playing: boolean = false;
    public _file: any = null;
    public currentAudio: any = null;
    public isAudioDownloading: boolean = false;
    public isRepeat: boolean = true;
    public errorLoading: boolean = false;
    private timeout;
    constructor(
        public audio: AudioService,
        public notify: NotifyService,
        public shared: SharedService,
        public http: HttpClient,
        public locale: DictionaryService
    ) {
        this.errorLoading = false;
    }

    private prepareProtocolLink(link) {
        if (link instanceof Blob || link instanceof File) {
            return URL.createObjectURL(link);
        }
        return (link.startsWith("https://") ? link : "https://" + link) + '?crossorigin=anonimous&cacheblock=true';
    }

    download() {
        const headers = new HttpHeaders().set("Access-Control-Allow-Origin", "*").set("crossorigin","anonimous")
        this.isAudioDownloading = true;
        this.http
            .get(this.prepareProtocolLink(this._file), {
                responseType: "blob",
                headers
            })
            .subscribe(
                (res: any) => {
                    var a = document.createElement("a");
                    a.href = URL.createObjectURL(res);
                    a.download =
                        this._file instanceof Blob || this._file instanceof File
                            ? "record.mp3"
                            : this._file.split(".").at(-2);
                    a.click();
                    this.isAudioDownloading = false;
                },
                (err) => (this.isAudioDownloading = false)
            );
    }

    copy() {
        let selBox = document.createElement("textarea");
        selBox.style.position = "fixed";
        selBox.style.left = "0";
        selBox.style.top = "0";
        selBox.style.opacity = "0";
        selBox.value = this.prepareProtocolLink(this._file);
        localStorage.setItem("sharedAudioLink", String(this.prepareProtocolLink(this._file)));
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand("copy");
        document.body.removeChild(selBox);
        this.notify.showNotify(this.locale.getTranslate("link_copied", "Ссылка на аудио скопирована в буфер"), "");
    }

    ngOnInit() {
        this.initPlayer();
        this.actionListener();
    }

    private initPlayer() {
        this.vocal_playing = this.autoPlay ? true : false;
        this.isRepeat = this.autoPlay;
        this.currentAudio = new Howl({
            src: [this.prepareProtocolLink(this._file)],
            ext: ["ogg"],
            autoplay: this.autoPlay,
            loop: false,
            preload: true,
            html5: true,
            onloaderror: () => {
                this.errorLoading = this._file instanceof Blob || this._file instanceof File ? false : true;
                this.outputAction.emit(1);
            },
            onend: () => {
                if (this.inputAction) {
                    this.outputAction.emit(1);
                } else {
                    if (this.isRepeat && this.autoPlay) {
                        setTimeout(() => this.repeat(), 300);
                    } else {
                        this.vocal_playing = false;
                    }
                }
            }
        });

        this.currentAudio.once("loaderror", () => {
            Howler.unload();
        });

        this.currentAudio.once("playerror", () => {
            Howler.unload();
        });
    }

    private $actionSubscriber: Subscription;
    private actionListener() {
        if (this.inputAction instanceof Observable)
            this.$actionSubscriber = this.inputAction.subscribe((actionType) => {
                // 1 - just repeat
                switch (actionType) {
                    case 1: {
                        this.repeat(1);
                        break;
                    }
                    // stop playing without repeat
                    case 2: {
                        this.vocal_playing = false;
                        break;
                    }
                    case 3: {
                        this.stopAudio();
                        break;
                    }
                    default:
                        null;
                }
            });
    }

    public playAudio() {
        if (this.short && this.audio.stopAudio) {
            this.audio.stopAudio();
            this.audio.stopAudio = null;
        }

        this.currentAudio.play();
        this.vocal_playing = true;

        if (this.short) {
            this.audio.stopAudio = () => this.stopAudio();
        }
    }

    public stopAudio() {
        this.currentAudio.stop();
        this.vocal_playing = false;
    }

    public pauseAudio() {
        this.currentAudio.pause();
        clearTimeout(this.timeout);
        this.vocal_playing = false;
    }

    public switchRepeat() {
        this.isRepeat = !this.isRepeat;
        if (!this.isRepeat) {
            clearTimeout(this.timeout);
        }
        this.outputAction.emit(this.isRepeat ? 2 : 3);
    }

    public repeat(ms: number = 1) {
        this.timeout = setTimeout(() => this.playAudio(), ms * 1000);
    }

    public ngOnDestroy() {
        this.isRepeat = false;
        if (this.$actionSubscriber) this.$actionSubscriber.unsubscribe();
        this.stopAudio();
        clearTimeout(this.timeout);
    }
}
