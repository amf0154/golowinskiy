import { RecorderService } from "./recorder.service";

export const RecorderServiceFactory = () => {  
  // Create env
  const recorder = new RecorderService();

  // Read environment variables from browser window
  const browserWindow = window || {};
  const browserWindowEnv = browserWindow['__recorder'] || {};

  // Assign environment variables from browser window to env
  // In the current implementation, properties from env.js overwrite defaults from the EnvService.
  // If needed, a deep merge can be performed here to merge properties instead of overwriting them.
  for (const key in browserWindowEnv) {
    if (browserWindowEnv.hasOwnProperty(key)) {
      recorder[key] = window['__recorder'][key];
    }
  }

  return recorder;
};

export const RecorderServiceProvider = {  
  provide: RecorderService,
  useFactory: RecorderServiceFactory,
  deps: [],
};