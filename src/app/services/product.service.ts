import { Injectable } from '@angular/core';
import { AdvertDetail } from '@components/shared/models/advert-list';
import { CopyAdvertsService } from '@components/shared/services/copyadverts.service';
import { DollService } from '@components/shared/services/doll.service';
import { MoveAdvertsService } from '@components/shared/services/moveadverts.service';
import { RepeatService } from '@components/shared/services/repeat.service';
import { GalleryItem } from '../interfaces/adverts';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private repeatService: RepeatService,
    private copyAdverts: CopyAdvertsService,
    private moveAdverts: MoveAdvertsService,
    private dollService: DollService
  ) { }

  gallery: Array<GalleryItem> = [];
  shuffleApplied = false;
  initialGalleryState = [];

  setGallery(data){
    this.shuffleApplied = false;
    this.gallery = data;
    this.initialGalleryState = Object.assign([], data);
    this.updShared();
  }

  updShared(){
    this.repeatService.stackOfAllAdverts = Object.assign([],this.gallery);
    this.copyAdverts.stackOfAllAdverts = Object.assign([],this.gallery);
    this.moveAdverts.stackOfAllAdverts = Object.assign([],this.gallery);
    this.dollService.stackOfAllAdverts = Object.assign([],this.gallery);
  }

  deleteItems(index: number,count: number){
    this.gallery.splice(index,count);
    this.updShared();
  }

  clear(){
    this.gallery.length = 0;
    this.updShared();
  }

  shuffle(): void {
    this.shuffleApplied = true;
    const shuffle = (array) => {
      for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
      }
      return array;
    }
    this.gallery = Object.assign([], shuffle(this.gallery));
    this.updShared();
  }

  cancelShuffle(): void {
    this.shuffleApplied = false;
    this.gallery = Object.assign([], this.initialGalleryState);
    this.updShared();
  }

  updItem(product: AdvertDetail | any){
    const productIndex = this.gallery.findIndex((p: GalleryItem)=> p.prc_ID == product.prc_ID);
    if(productIndex != -1){
      const modifyProduct = Object.assign({},this.gallery[productIndex]);
      const properties = Object.keys(product);
      for (const prop of properties) {
      //  if (modifyProduct.hasOwnProperty(prop) && modifyProduct[prop] !== product[prop])
          modifyProduct[prop] = product[prop];
      }
      if(properties.includes('t_imageprev'))
        modifyProduct.image_Base = product.t_imageprev;
      this.gallery[productIndex] = modifyProduct;
      this.updShared();
    }
  }
}
