import {
  Compiler,
  ComponentFactory,
  Injectable,
  Injector,
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LazyLoaderService {
  constructor(private compiler: Compiler, private injector: Injector) {}
  componentFactories: ComponentFactory<any>[];

  loadModule(path: any) {
    return path()
      .then(elementModuleOrFactory => {
          try {
            const moduleFactories = this.compiler.compileModuleAndAllComponentsSync(elementModuleOrFactory);
            console.log('moduleFactories',moduleFactories)
            return moduleFactories.componentFactories;
          } catch (err) {
            throw err;
          }
        
    }
  )};
}