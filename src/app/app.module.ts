import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from "@angular/platform-browser";
import { APP_INITIALIZER, NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxPaginationModule } from "ngx-pagination";
import { TokenInterceptor } from "@components/shared/classes/token.interceptor";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MainPageComponent } from "@components/main-page/main-page.component";
import { AdvertisementPageComponent } from "@components/advertisement-page/advertisement-page.component";
import { EditAdvertisementPageComponent } from "@components/edit-advertisement-page/edit-advertisement-page.component";
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA, MAT_DIALOG_DEFAULT_OPTIONS } from "@angular/material/dialog";
import { HeaderComponent } from "@components/main-page/header/header.component";
import { CabinetUserComponent } from "@components/main-page/header/cabinet-user/cabinet-user.component";
import { DropdownDirective } from "@components/shared/directives/dropdown.directive";
import { FooterComponent } from "@components/main-page/footer/footer.component";
import { AuthPageComponent } from "@components/main-page/auth-page/auth-page.component";
import { LoginComponent } from "@components/main-page/auth-page/login/login.component";
import { RegistrationComponent } from "@components/main-page/auth-page/registration/registration.component";
import { RecoveryComponent } from "@components/main-page/auth-page/recovery/recovery.component";
import { ProductsPageComponent } from "@components/main-page/products-page/products-page.component";
import { DetailPageComponent } from "@components/main-page/detail-page/detail-page.component";
import { OrderComponent } from "@components/main-page/order/order.component";
import { EnvServiceProvider } from "./env.service.provider";
import { YoutubePlayerComponent } from "@components/youtube-player/youtube-player.component";
import { YouTubePlayerModule } from "@angular/youtube-player";
import { RecorderComponent } from "@components/audio-player/recorder.component";
import { AudioRecordingService } from "@components/shared/services/audio-recording.service";
import { CabinetComponent } from "@components/cabinet/cabinet.component";
import { FullDescriptionComponent } from "@components/main-page/detail-page/full-description/full-description.component";
import { BackgroundPageComponent } from "@components/background-page/background-page.component";
import { ModifyCatalogComponent } from "@components/modify-catalog/modify-catalog.component";
import { ModifyComponent } from "@components/modify-catalog/modify/modify.component";
import { VoiceRecorderComponent } from "@components/voice-recorder/voice-recorder.component";
import { VideoUploadComponent } from "@components/shared/video-upload/video-upload.component";
import { MatTooltipModule } from "@angular/material/tooltip";
import * as Hammer from "hammerjs";
import { WindowRefService } from "@components/shared/services/window-ref.service";
import { RecorderServiceProvider } from "./recorder.service.provider";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { FastEditAdvComponent } from "./components/main-page/fast-edit-adv/fast-edit-adv.component";
import { EditAudioComponent } from "./components/shared/edit-audio/edit-audio.component";
import { EnvService } from "./env.service";
import { map } from "rxjs/operators";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from "@angular/material";
import { AdditionalImagesComponent } from "./components/shared/additional-images/additional-images.component";
import { ImageLoaderComponent } from "./components/shared/image-loader/image-loader.component";
import { AudioControllerComponent } from "./components/shared/audio-controller/audio-controller.component";
import { VoiceRecorderTwoComponent } from "./components/shared/voice-recorder-two/voice-recorder-two.component";
import { AdminLoginComponent } from "@components/admin-login/admin-login.component";
import { CatalogSelectActionComponent } from "./components/shared/catalog-select-action/catalog-select-action.component";
import { SetCopiedAudioComponent } from "./components/shared/set-copied-audio/set-copied-audio.component";
import { SharedModule } from "./shared.module";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { EditPersonalComponent } from "@components/main-page/auth-page/edit-personal/edit-personal.component";
import { CommodityModule } from "@components/commodity/commodity.module";
import { EditAdditImgComponent } from "./components/main-page/edit-addit-img/edit-addit-img.component";
import { SubscribePageComponent } from "@components/subscribe-page/subscribe-page.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DateTimePickerComponent } from "@components/date-time-picker/date-time-picker.component";
import { SafePipe } from "./pipes/safeurl.pipe";
import { ContentTypeComponent } from "./components/shared/content-type/content-type.component";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { PermissionItem } from "@components/shared/interfaces";
import { CatalogModeComponent } from "@components/catalog-mode/catalog-mode.component";

export class MyHammerConfig extends HammerGestureConfig {
    buildHammer(element: HTMLElement) {
        const mc = new Hammer(element, {
            touchAction: "pan-y"
        });

        return mc;
    }
}

@NgModule({
    declarations: [
        AppComponent,
        MainPageComponent,
        AdvertisementPageComponent,
        EditAdvertisementPageComponent,
        HeaderComponent,
        CabinetUserComponent,
        DropdownDirective,
        FooterComponent,
        AuthPageComponent,
        LoginComponent,
        RegistrationComponent,
        RecoveryComponent,
        ProductsPageComponent,
        DetailPageComponent,
        OrderComponent,
        YoutubePlayerComponent,
        RecorderComponent,
        EditPersonalComponent,
        CabinetComponent,
        FullDescriptionComponent,
        BackgroundPageComponent,
        ModifyCatalogComponent,
        ModifyComponent,
        VoiceRecorderComponent,
        //  SearchResultsComponent,
        VideoUploadComponent,
        //  RepeatResultsComponent,
        FastEditAdvComponent,
        EditAudioComponent,
        AdditionalImagesComponent,
        ImageLoaderComponent,
        AudioControllerComponent,
        VoiceRecorderTwoComponent,
        AdminLoginComponent,
        CatalogSelectActionComponent,
        SetCopiedAudioComponent,
        EditAdditImgComponent,
        SubscribePageComponent,
        DateTimePickerComponent,
        SafePipe,
        ContentTypeComponent,
        // StudyComponent,
        CatalogModeComponent
    ],
    imports: [
        MatTooltipModule,
        YouTubePlayerModule,
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NgxPaginationModule,
        MatSnackBarModule,
        SharedModule,
        NgbModule,
        AngularFontAwesomeModule,
        CommodityModule,
        MatSlideToggleModule
    ],
    exports: [ModifyComponent, SafePipe, ContentTypeComponent],
    entryComponents: [
        ModifyComponent,
        VideoUploadComponent,
        FastEditAdvComponent,
        VoiceRecorderComponent,
        VoiceRecorderTwoComponent,
        AdminLoginComponent,
        SetCopiedAudioComponent,
        EditPersonalComponent,
        EditAdditImgComponent
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: initShopData,
            multi: true,
            deps: [HttpClient, EnvService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initLocalization,
            multi: true,
            deps: [DictionaryService, EnvService, HttpClient]
        },
        EnvServiceProvider,
        RecorderServiceProvider,
        WindowRefService,
        AudioRecordingService,
        {
            provide: HTTP_INTERCEPTORS,
            multi: true,
            useClass: TokenInterceptor
        },
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: MyHammerConfig
        },
        {
            provide: MAT_DIALOG_DATA,
            useValue: {}
        },
        {
            provide: MatDialogRef,
            useValue: {}
        },
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS,
            useValue: {
                hasBackdrop: true,
                direction: "ltr"
            }
        },
        { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}

export function initShopData(http: HttpClient, env: EnvService) {
    function getDataBySlug(slug) {
        return Promise.all([
            http.get<any>(`${env.apiUrl}/api/shopinfo/${slug}`).toPromise(),
            http.get<any>(`${env.apiUrl}/api/shopdetails/${slug}`).toPromise(),
            http.get<any>(`${env.apiUrl}/api/shopdetails/${slug}`).toPromise()
        ])
            .then(([shopInfo, shopDetails]) => {
                env.shopInfo = shopInfo;
                env.shopDetails = shopDetails;
                const { dz } = shopInfo;
                localStorage.setItem("defaultLocale", dz);
                if (!localStorage.getItem("locale")) localStorage.setItem("locale", dz ? dz : "RU");
                return shopInfo;
            })
            .then((shopInfo) => {
                return http
                    .get<any>(`${env.apiUrl}/api/Localization/GetUi?cust_ID_Main=${shopInfo.cust_id}`)
                    .toPromise();
            })
            .then((permission: Array<PermissionItem>) => {
                env.permission.next(permission);
                return permission;
            });
    }

    return () =>
        import("punycode")
            .then((punycode) => {
                const shopName = env.shopName;
                if (shopName && shopName.trim().length !== 0) {
                    return shopName;
                } else {
                    const host = punycode.toUnicode(location.hostname.split(".")[0]);
                    if (host) return env.exceptDomains.includes(host) ? env.defaultShopId : host;
                }
            })
            .then(getDataBySlug);
}

export function initLocalization(dictionary: DictionaryService, env: EnvService, http: HttpClient) {
    return () => {
        const localeToTableView = (data) => {
            const aliaces: Array<any> = Array.from(
                data.reduce((alias, el: any) => {
                    alias.add(el.alias);
                    return alias;
                }, new Set())
            );
            return aliaces.reduce((locale, alias: any) => {
                const eachAliaseData = data.filter((el: any) => el.alias == alias);
                const aliasInfo = eachAliaseData.reduce((curr, el: any) => {
                    curr["ALIAS"] = el.alias;
                    curr[el.dz.toUpperCase()] = el.txt;
                    return curr;
                }, {});
                locale.push(aliasInfo);
                return locale;
            }, []);
        };

        const filterLangColumnsOnly = (attr) => !["ALIAS", "PageName"].includes(attr);
        const getLanguages = (db) => {
            return db.reduce((languages, el) => {
                const keys = Object.keys(el).filter(filterLangColumnsOnly);
                keys.forEach((v) => {
                    if (!languages.includes(v)) languages.push(v);
                });
                return languages;
            }, []);
        };

        return new Promise((resolve) => {
            http.get(`${env.apiUrl}/api/Localization/Get?library=1`)
                .pipe(map((el) => localeToTableView(el)))
                .toPromise()
                .then((data) => {
                    dictionary.generateDictionary(getLanguages(data));
                    const languageDB = data.map((item: any) => {
                        dictionary.languages.forEach((locale) => {
                            dictionary.fillDictionary(item, locale);
                        });
                    });
                    Promise.all(languageDB).then(() => {
                        dictionary.baseLoaded.next(true);
                        resolve(true);
                    });
                });
        });
    };
}
