import { Component, HostListener, OnInit } from "@angular/core";
import { AuthService } from "@components/shared/services/auth.service";
import { MainService } from "@components/shared/services/main.service";
import { LocalizeService } from "@components/shared/services/localize.service";
import { DictionaryService } from "@components/shared/services/dictionary.service";
import { StateService } from "@components/shared/services/state.service";
import { ShopInfo } from "@components/shared/interfaces";
import { EnvService } from "./env.service";
import { SpinnerService } from "@components/shared/services/spinner.service";
import { debounce } from "rxjs/operators";
import { timer } from "rxjs";
import { SharedService } from "@components/shared/services/shared.service";
import { NavigationEnd, Router } from "@angular/router";
import { Enums } from "@components/shared/models/enums";

@Component({
    selector: "app-root",
    template: `
        <span *ngIf="showLoader" class="loading"></span>
        <router-outlet></router-outlet>
    `
})
export class AppComponent implements OnInit {
    showLoader: boolean = false;
    constructor(
        private authService: AuthService,
        private state: StateService,
        public spinnerService: SpinnerService,
        private mainService: MainService,
        private localize: LocalizeService,
        private dictionary: DictionaryService,
        private router: Router,
        private env: EnvService,
        public shared: SharedService
    ) {
        this.state.setShopInfo(this.env.shopInfo);
        this.mainService.loadBackgrounds(this.env.shopInfo as ShopInfo);
        this.mainService.getFonPictures();
        router.events.subscribe((val) => {
            if (val && val instanceof NavigationEnd) {
                if (val.url && val.url.includes("categories/")) {
                    const [catId] = val.url.split("/").filter(Number);
                    this.state.currentCatId.next(catId ? catId : null);
                } else {
                    this.state.currentCatId.next(null);
                }
            }
        });
        if (!localStorage.getItem("locale")) this.dictionary.changeLanguage((this.env.shopInfo as ShopInfo).dz);
        if (!localStorage.getItem("audioRepeats")) { localStorage.setItem("audioRepeats", "5")};

        if (!localStorage.getItem("freezeSettings")) {
            const freezeSettings = {
                [Enums.SiteDepartment.REPEAT]: false,
                [Enums.SiteDepartment.STUDY]: false,
                [Enums.SiteDepartment.MAIN]: true,
                [Enums.SiteDepartment.CABINET]: false
            };
            localStorage.setItem("freezeSettings", JSON.stringify(freezeSettings));
        }
    }

    @HostListener("copy", ["$event"]) blockCopy(e: KeyboardEvent) {
        this.authService.userInfo.subscribe(() => {
            if (!this.shared.isAdmin) {
                e.preventDefault();
            }
        });
    }

    @HostListener("cut", ["$event"]) blockCut(e: KeyboardEvent) {
        this.authService.userInfo.subscribe(() => {
            if (!this.shared.isAdmin) {
                e.preventDefault();
            }
        });
    }

    @HostListener("contextmenu", ["$event"])
    onRightClick(e) {
        this.authService.userInfo.subscribe(() => {
            if (!this.shared.isAdmin) {
                e.preventDefault();
            }
        });
    }

    ngOnInit(): void {
        this.spinnerService.status.pipe(debounce(() => timer(300))).subscribe((val: boolean) => {
            this.showLoader = val;
        });
        const token = localStorage.getItem("token");
        if (token !== null) {
            this.authService.setToken(token);
        }
    }
}
