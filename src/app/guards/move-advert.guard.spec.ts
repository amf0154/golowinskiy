import { TestBed, async, inject } from '@angular/core/testing';

import { MoveAdvertGuard } from './move-advert.guard';

describe('MoveAdvertGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoveAdvertGuard]
    });
  });

  it('should ...', inject([MoveAdvertGuard], (guard: MoveAdvertGuard) => {
    expect(guard).toBeTruthy();
  }));
});
