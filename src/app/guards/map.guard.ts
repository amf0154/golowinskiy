import { Injectable } from "@angular/core";
import { CanActivate, CanActivateChild, Router } from "@angular/router";
import { MainService } from "@components/shared/services/main.service";
import { SharedService } from "@components/shared/services/shared.service";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class MapAccessGuard implements CanActivate, CanActivateChild {
    constructor(private router: Router, private main: MainService, private shared: SharedService) {}

    canActivate(): Observable<boolean> {
        return new Observable<boolean>((obs) => {
            this.main.getShopInfo().subscribe(({ isMapShow }) => {
                if (isMapShow || this.shared.isAdmin) {
                    obs.next(true);
                } else {
                    this.router.navigate(["/"]);
                    obs.next(false);
                }
            });
        });
    }

    canActivateChild(): Observable<boolean> {
        return this.canActivate();
    }
}
