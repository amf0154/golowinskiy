import { Injectable } from '@angular/core';
import { CanActivate,CanActivateChild, Router } from '@angular/router';
import { AuthService } from '@components/shared/services/auth.service';
import { SharedService } from '@components/shared/services/shared.service';
import { Observable } from 'rxjs';
import { EnvService } from '../env.service';
import { UserInfo } from '../interfaces/user-info';

@Injectable({
  providedIn: 'root'
})
export class MoveAdvertGuard implements CanActivate, CanActivateChild {
  constructor(
    private auth: AuthService,
    private router: Router,
    private env: EnvService,
    private shared: SharedService){
}

canActivate(): Observable<boolean>{
  return new Observable<boolean>(obs => {
    this.auth.getUserInfo()
    .subscribe((u:UserInfo)=>{
      // this.env.shopDetails.isMoveAllow &&
      if((u.isMoveAdvert || this.shared.isAdmin)){
        obs.next(true);
      }else {
        this.router.navigate(['/']);
        obs.next(false);
      }
    });
  });
}

canActivateChild(): Observable<boolean>{
  return this.canActivate();
}

  
}
