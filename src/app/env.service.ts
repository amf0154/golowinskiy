import { Injectable } from '@angular/core';
import { PermissionItem, ShopDetails, ShopInfo } from '@components/shared/interfaces';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserInfo } from './interfaces/user-info';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = '';
  public shopName = '';
  public adminUrl = "";
  public exceptDomains = [];
  public defaultShopId = "";
  public advertUrl = "";
  public advertImg = "";
  public shopInfo: ShopInfo = null;
  public userInfo: UserInfo = {} as UserInfo;
  public permission = new BehaviorSubject<Array<PermissionItem>>([]);
  public shopDetails: ShopDetails = null;
  public noImgDesktop = "";
  public noImgMobile = "";
  constructor() {}

  public checkPermission(alias: string){
    return this.permission.pipe(map((el)=>{
      const al =  el.find((a)=> a.txt.includes(alias));
      if(al) {
        return al.isShow;
      }
      if(alias ==='select_repeat' && localStorage.getItem("isShowMenuRepeat") === "1") {
        return true;
      }
      return false;
    }));
  }

}
