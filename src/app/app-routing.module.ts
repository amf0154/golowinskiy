import { NgModule } from "@angular/core"
import { PreloadAllModules, RouterModule, Routes } from "@angular/router"
import { AuthGuard } from "@components/shared/classes/auth.guard"
import { MainPageComponent } from "@components/main-page/main-page.component"
import { AdvertisementPageComponent } from "@components/advertisement-page/advertisement-page.component"
import { AuthPageComponent } from "@components/main-page/auth-page/auth-page.component"
import { LoginComponent } from "@components/main-page/auth-page/login/login.component"
import { RegistrationComponent } from "@components/main-page/auth-page/registration/registration.component"
import { RecoveryComponent } from "@components/main-page/auth-page/recovery/recovery.component"
import { ProductsPageComponent } from "@components/main-page/products-page/products-page.component"
import { DetailPageComponent } from "@components/main-page/detail-page/detail-page.component"
import { OrderComponent } from "@components/main-page/order/order.component"
import { CabinetComponent } from "@components/cabinet/cabinet.component"
import { ModifyCatalogComponent } from "@components/modify-catalog/modify-catalog.component"
import { CatalogSelectActionComponent } from "@components/shared/catalog-select-action/catalog-select-action.component"
import { MoveAdvertGuard } from "./guards/move-advert.guard"
import { SubscribePageComponent } from "@components/subscribe-page/subscribe-page.component"
import { Models } from "@components/shared/models/models"
import { MapAccessGuard } from "./guards/map.guard"
import { CatalogModeComponent } from "@components/catalog-mode/catalog-mode.component"

const routes: Routes = [
    {path: '', component: MainPageComponent, children: [
        { path: 'auth', component: AuthPageComponent, children: [
            { path: 'login', component: LoginComponent },
            { path: 'registration', component: RegistrationComponent },
            { path: 'recovery', component: RecoveryComponent },
        ]},
        {path: 'order', component: OrderComponent}
    ]},
    { path: Models.PageRoutes.Products, component: ProductsPageComponent, children: [
        { path: 'auth', component: AuthPageComponent, children: [
            { path: 'login', component: LoginComponent },
            { path: 'registration', component: RegistrationComponent },
            { path: 'recovery', component: RecoveryComponent },
        ]},
        {path: 'order', component: OrderComponent},
    ]},
    { path: Models.PageRoutes.Detail, component: DetailPageComponent },
    { path: Models.PageRoutes.Search, component: CatalogModeComponent},
    { path: Models.PageRoutes.Repeat, component: CatalogModeComponent, canActivate: [AuthGuard]},
    { path: Models.PageRoutes.Study, component: CatalogModeComponent, canActivate: [AuthGuard]},
    { path: Models.PageRoutes.Dictionary, component: CatalogModeComponent, canActivate: [AuthGuard]},
    { path: 'catcabinet', component: CabinetComponent, canActivate: [AuthGuard] },
    { path: Models.PageRoutes.CabinetMain, component: MainPageComponent, canActivate: [AuthGuard] },
    { path: 'catalog/move', component: CatalogSelectActionComponent, canActivate: [AuthGuard, MoveAdvertGuard] },
    { path: 'catalog/doll', component: CatalogSelectActionComponent, canActivate: [AuthGuard] },
    { path: 'modifycatalog', component: ModifyCatalogComponent, canActivate: [AuthGuard] },
    { path: Models.PageRoutes.CabinetProducts, component: ProductsPageComponent, canActivate: [AuthGuard]},
    { path: Models.PageRoutes.CabinetDetail, component: DetailPageComponent, canActivate: [AuthGuard]},
    { path: 'addProduct', component: AdvertisementPageComponent, canActivate: [AuthGuard] },
    { path: 'commodity', canActivate: [AuthGuard], loadChildren: () => import('./components/commodity/commodity.module').then((m) => m.CommodityModule) },
    { path: 'map', canActivate: [MapAccessGuard], loadChildren: () => import('./components/map/map.module').then(m => m.MapModule) },
    { path: Models.PageRoutes.Subscribe, component: SubscribePageComponent, canActivate: [AuthGuard] },
    { path: Models.PageRoutes.CopyList, component: SubscribePageComponent, canActivate: [AuthGuard] },
    { path: Models.PageRoutes.CopyCat, component: SubscribePageComponent, canActivate: [AuthGuard] },
    { path: '**', redirectTo: '/' },
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes,{ preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
