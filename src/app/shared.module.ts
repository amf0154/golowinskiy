import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileCategoriesComponent } from './components/categories/mobile-categories/mobile-categories.component';
import { CheckboxComponent } from '@components/checkbox/checkbox.component';
import { FillCheckboxComponent } from '@components/shared/checkbox-fill/checkbox-fill.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VoiceRecorderCommodityComponent } from '@components/commodity/shared/voice-recorder/voice-recorder.component';
import { CategoriesComponent } from '@components/categories/categories.component';
import { BreadcrumbsComponent } from '@components/breadcrumbs/breadcrumbs.component';
import { AttachAudioToImgComponent } from '@components/commodity/shared/attach-audio-to-img/attach-audio-to-img.component';
import { AudioPlayerComponent } from '@components/commodity/shared/audio-player/audio-player.component';
import { AudioUploaderComponent } from '@components/commodity/shared/audio-uploader/audio-uploader.component';
import { NavbarComponent } from '@components/shared/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { SearchBarComponent } from '@components/search-bar/search-bar.component';
import { MenuHeaderComponent } from '@components/shared/menu-header/menu-header.component';
import { ChangeLanguageComponent } from '@components/change-language/change-language.component';
import { PopupMenuComponent } from '@components/shared/menu-header/popup-menu/popup-menu.component';

@NgModule({
  declarations: [
    MobileCategoriesComponent,
    CategoriesComponent,
    BreadcrumbsComponent,
    CheckboxComponent,
    FillCheckboxComponent,
    VoiceRecorderCommodityComponent,
    AttachAudioToImgComponent,
    AudioPlayerComponent,
    AudioUploaderComponent,
    NavbarComponent,
    SearchBarComponent,
    MenuHeaderComponent,
    ChangeLanguageComponent,
    PopupMenuComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    MobileCategoriesComponent,
    BreadcrumbsComponent,
    CategoriesComponent,
    CheckboxComponent,
    FillCheckboxComponent,
    AttachAudioToImgComponent,
    AudioPlayerComponent,
    AudioUploaderComponent,
    NavbarComponent,
    SearchBarComponent,
    MenuHeaderComponent,
    ChangeLanguageComponent,
    PopupMenuComponent
    
  ],
  entryComponents: [VoiceRecorderCommodityComponent,AttachAudioToImgComponent, PopupMenuComponent]
})
export class SharedModule {}
